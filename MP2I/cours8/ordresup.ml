let rec somme f n =
  if n = 0 then
    0
  else
    (f n) + somme f (n-1)

let () = Printf.printf "%d\n" (somme (fun x -> x*x) 5)

let derivee f dx =
  let fp x = (f (x +. dx) -. f x) /. dx in
  fp

let f x = x**2.0
let fp = derivee f 0.000001

let () = Printf.printf "%f\n" (fp 3.0)

(* Calculer la somme des carres des éléments d'une liste *)

let rec somme_carres_aux l res = match l with
  | [] -> res
  | x::xs -> (somme_carres_aux xs (x*x+res))

let somme_carres l = somme_carres_aux l 0

let carre x = x*x
let somme_carres2 l = List.fold_left (+) 0 (List.map carre l)

let affiche_liste l = List.iter (fun x -> Printf.printf "%d " x) l
let affiche_liste2 = List.iter (fun x -> Printf.printf "%d " x)
let affiche_liste3 = List.iter (Printf.printf "%d ")
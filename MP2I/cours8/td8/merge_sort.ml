let rec split l =
  match l with
  | [] -> ([], [])
  | [x] -> ([x], [])
  | x :: y :: xs ->
    let (l1, l2) = split xs in
    (x :: l1, y :: l2)

let rec merge l1 l2 =
  match (l1, l2) with
  | ([], l) -> l
  | (l, []) -> l
  | (x :: xs, y :: ys) ->
    if x < y then x :: (merge xs (y :: ys))
    else y :: (merge (x :: xs) ys)

let rec merge_sort l =
  match l with
  | [] -> []
  | [x] -> [x]
  | _ ->
    let (l1, l2) = split l in
    merge (merge_sort l1) (merge_sort l2)

let () = assert (merge_sort [1; 2; 3; 4; 5] = [1; 2; 3; 4; 5])
let () = assert (merge_sort [5; 4; 3; 2; 1] = [1; 2; 3; 4; 5])
let () = assert (merge_sort [1; 3; 5; 2; 4] = [1; 2; 3; 4; 5])
let () = print_endline "OK"

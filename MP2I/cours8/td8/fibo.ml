let rec fibo n =
  if n = 0 then 0
  else if n = 1 then 1
  else fibo (n - 1) + fibo (n - 2)

let fibo_smart n =
  let rec fibo_aux n a b =
    if n = 0 then a
    else fibo_aux (n - 1) b (a + b)
  in
  fibo_aux n 0 1

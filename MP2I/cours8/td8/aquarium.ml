type sexe = Male | Femelle
type race =
  (*Carnivores*)
  | Merou
  | Thon
  | PoissonClown
  (*Herbivores*)
  | Sole
  | Bar
  | Carpe

type poisson = {
  nom: string;
  sexe: sexe;
  race: race;
  mutable pv: int;
  mutable age: int
}

type algue = {
  mutable pv : int
}

type vivant =
  | Poisson of poisson
  | Algue of algue

let pv v = match v with
  | Poisson p -> p.pv
  | Algue a -> a.pv

let est_carnivore p = match p.race with
| Merou | Thon | PoissonClown -> true
| Sole | Bar | Carpe -> false

let poisson nom race sexe = { nom = nom; sexe = sexe; race = race; pv = 10; age = 0 }
let algue () = { pv = 10 }

type aquarium = {
  mutable algues: algue list;
  mutable poissons: poisson list
}

let string_of_sexe s = match s with
  | Male -> "mâle"
  | Femelle -> "femelle"

let string_of_poisson p = Printf.sprintf "%s(%s, %d pv)" p.nom (string_of_sexe p.sexe) p.pv
let string_of_algue (a: algue) = Printf.sprintf "Algue(%d pv)" a.pv

let affiche_aquarium aq =
  print_string "Algues : ";
  List.iter (Printf.printf "%s; ") (List.map string_of_algue aq.algues);
  print_newline();
  print_string "Poissons : ";
  List.iter (Printf.printf "%s; ") (List.map string_of_poisson aq.poissons);
  print_newline()

let liste_indices n = List.init n (fun i -> i)
let rec indices_vrais_aux l i = match l with
  | [] -> []
  | t::q -> 
      let res = indices_vrais_aux q (i+1) in
      if t then
        i::res
      else
        res

let rec indices_faux_aux t i res =
  if i = Array.length t then
    res
  else (
    if t.(i) then
      indices_faux_aux t (i+1) res
    else
      indices_faux_aux t (i+1) (i::res)
  )

let indices_faux t = indices_faux_aux t 0 []


let supprimer_morts aq morts =
  let rec loop i l = match l with
  | [] -> []
  | t::q ->
      let res = loop (i+1) q in
      if morts.(i) then res else t::res
  in aq.poissons <- loop 0 aq.poissons

let list_not_empty l = match l with
  | [] -> false
  | _ -> true

let marquer_poisson_mort (p: poisson) i morts =
  if p.pv <= 0 then (
    morts.(i) <- true;
    Printf.printf "%s est mort...\n" p.nom
  )

let faire_manger_carnivore morts i poissons =
  let indices_non_manges = indices_faux morts in
  let indices_mangeables = Array.of_list indices_non_manges in
  let nb_mangeables = (Array.length (indices_mangeables)) in
  if nb_mangeables > 0 then (
    let cible_idx = indices_mangeables.(Random.int nb_mangeables) in
    let cible = poissons.(cible_idx) in
    let p = poissons.(i) in
    if (cible_idx <> i && cible.race <> p.race) then (
      Printf.printf "%s mange %s\n" poissons.(i).nom cible.nom;
      cible.pv <- cible.pv - 4;
      p.pv <- p.pv + 5;
      marquer_poisson_mort cible cible_idx morts;
    )
  )

let faire_manger_herbivore aq p = match aq.algues with
  | [] -> ()
  | t::q -> (
    Printf.printf "%s mange une algue\n" p.nom;
    p.pv <- p.pv + 3;
    t.pv <- t.pv - 2;
    if t.pv <= 0 then aq.algues <- q
  )

let faire_grandir_algues aq =
  List.iter (fun algue -> algue.pv <- algue.pv+1) aq.algues

let faire_vieillir_poisson morts i p =
  p.age <- p.age+1;
  if p.age > 20 then (
    morts.(i) <- true;
    Printf.printf "%s est mort de vieillesse...\n" p.nom
  )

let faire_vieillir_poissons aq morts =
  List.iteri (faire_vieillir_poisson morts) aq.poissons

let donner_faim_poissons aq morts =
  List.iteri (fun i (p: poisson) -> p.pv <- p.pv-1; marquer_poisson_mort p i morts) aq.poissons

let a_faim (p: poisson) = p.pv <= 5

let tour aq =
  let poissons = Array.of_list aq.poissons in
  let nb_poissons = Array.length poissons in
  let morts = Array.make nb_poissons false in
  faire_grandir_algues aq;
  faire_vieillir_poissons aq morts;
  donner_faim_poissons aq morts;
  let rec loop i =
    if i <> nb_poissons then (
      if (not (morts.(i))) then (
        if a_faim poissons.(i) then (
          if est_carnivore poissons.(i) then (
            faire_manger_carnivore morts i poissons
          )
          else (
            faire_manger_herbivore aq poissons.(i)
          )
        )
      );
      loop (i+1)
    )
  in loop 0;
  supprimer_morts aq morts

let () =
  Random.self_init();
  let p1 = poisson "Gabriel" Merou Male in
  let p2 = poisson "Paloma" Thon Femelle in
  let p3 = poisson "Pierre" Carpe Male in
  let p4 = poisson "Paul" Carpe Male in
  let aq = {
    algues = List.init 1 (fun _ -> algue ());
    poissons = [p1; p2; p3]
    (* poissons = [p3; p4] *)
  } in
  affiche_aquarium aq;

  for i = 1 to 30 do
    Printf.printf "======= TOUR %d =========\n" i;
    tour aq;
    Printf.printf "==== AQUARIUM ====\n";
    affiche_aquarium aq
  done

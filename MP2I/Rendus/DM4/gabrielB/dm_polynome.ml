
type monome = { coeff: int; degre: int
}
type polynome = monome list


let rec print_polynome p = 
  match p with 
  |[] -> print_endline(string_of_int(0))
  |x::q-> 
    if x.degre = 0  then (
      print_int(x.coeff);
      print_endline("")

    )
    else
          let () = print_int(x.coeff)in 
          let () = print_string("*X") in  
          let () = print_string("^") in  
          let () = print_int(x.degre) in 
          let () = print_string(" + ") in  
          print_polynome q

(*question 1 *)

let p = [  {coeff = 1; degre = 2}; {coeff = -2; degre = 1}; {coeff = 1; degre = 0} ]


(*question 2 *)

let rec degre_decroissant p  d =
  match p with 
  |[]-> true
  |x::q-> if x.degre > d  then ( 
    false 
     )
          else degre_decroissant q x.degre


let rec coeff_est_non_nul p d =
  if(d = 0) then (
    false
  )
  else 
    match p with 
    |[]-> true 
    |x::p -> coeff_est_non_nul p x.coeff



let est_polynome p = 
  match p with 
  |[]-> true 
  |x::q-> if ((degre_decroissant q x.degre )= true && (coeff_est_non_nul q  x.coeff) = true )then (
      true 
    )    else 
      false 

(*let () = if est_polynome(p) = true then print_endline(" marche ") *)




(*question 3 *)

(*les degrées sont décroissant on prend donc le premier degrée*)

let degre p = 
  match p with 
  |[]-> -1 
  |x::p -> x.degre


(*question 4 *)


let rec puissance_ter x n resultat = 
  
  match n with 
  |0-> 1 
  |1-> resultat 
  |_-> puissance_ter x (n-1) (resultat*x)

let puissance x n =
  puissance_ter x n x 


let rec evaluation_terminal p x resultat = 
  match p with  
  |[]-> resultat 
  |t::q-> 
    evaluation_terminal q x (resultat + (t.coeff*(puissance x t.degre)))

let evaluation p x = 
  evaluation_terminal p x 0
(*
let () = print_int(evaluation p 10) (* 100 -20 +1 = 81*) *)
  

  
(*question 5 *)



let rec composition_ter p n l= 
  match p with 
  |[]-> List.rev l
  |x::q-> composition_ter q n ({degre = (x.degre*n);coeff = x.coeff }::l)

let composition p n = 
  composition_ter p n []
(*
let () = print_polynome p
let () = print_polynome (composition p 7)
*)
(*question 6 *)
let max a b = 
  if a < b then b
  else a 


let rec somme_terminal p q n l= 
  match p ,q  with 
  |[] , [] -> List.rev l 
  |x::poly ,[] ->somme_terminal  poly [] 0 (x::l)
  |[] , x::poly ->somme_terminal [] poly 0 (x::l)
  |x::poly , t::poly_2 ->
    if (x.degre = n && t.degre <n )then (

      somme_terminal poly (t::poly_2) (n-1) (x::l) 
    )
    else(
      if( x.degre < n && t.degre  = n )then (
       
        somme_terminal (x::poly) poly_2 (n-1) (t::l)
      )
      else(
        if x.degre = n && t.degre = n then(
          
          if (x.coeff + t.coeff ) = 0 then 
            somme_terminal poly poly_2 (n-1) l 
          else(
            somme_terminal poly poly_2 (n-1) ({degre = n; coeff = (x.coeff + t.coeff) }::l)
        ))
        else (
          somme_terminal (x::poly) (t::poly_2) (n-1) (l) (*le cas ou ni p ni q n'ont des monomes de degrés n*)
        )))


let somme p q = 
  somme_terminal p q (max(degre p) (degre q)) []

(*
let q = [{coeff = -5; degre = 5};{coeff = -1; degre = 2}; {coeff = 4; degre = 1}; {coeff = 3  ; degre = 0} ; ]


let () = print_polynome(p)
let () = print_polynome (q)
let() = print_polynome( somme p q )
let () = print_polynome (somme p [])

*)

let rec produit_monome_polynome  monome q resultat = 
  match q with 
  |[]-> resultat
  |x::p ->
    produit_monome_polynome monome p (somme resultat [{degre = x.degre + monome.degre; coeff = monome.coeff*x.coeff}])

let rec produit_terminal p q resultat  = 
  match p with 
  |[]-> resultat
  |x::poly-> produit_terminal poly q (somme resultat (produit_monome_polynome x q []))


let produit p q =
  produit_terminal p q [] 

(*
let () = print_polynome (produit p [])
let q = [{degre = 4; coeff = 5}; {degre = 3; coeff = 2} ;{degre = 1; coeff = -2} ;{degre = 0; coeff = 6} ]
let () = print_polynome q
let () = print_polynome p 
let () = print_polynome (produit p q )
*)

let rec derivation_term p r = 
  match p with 
  |[]-> List.rev r
  |x::q-> 
    if x.degre = 0 then (     
     derivation_term [] r
    )
    else (

      derivation_term q ({degre = x.degre-1 ;coeff =  x.coeff*x.degre }::r) 
    )

let derivation p =
  derivation_term p []
(*
let() = print_polynome (composition p 7 )
let () = print_polynome (derivation (composition p 7 ))
*)
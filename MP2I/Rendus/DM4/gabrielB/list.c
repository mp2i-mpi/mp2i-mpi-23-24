#include "list.h"

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

typedef struct Node {
    int value;
    struct Node *next;
    struct Node *prev;
} node;

typedef struct List {
    node *first;
    node *last;
} list;

void print_list(const list* l) {
    printf("[ ");
    // Une utilisation astucieuse de la boucle for
    // afin d'itérer sur ce genre de structures :
    for (node* n = l->first; n != NULL; n = n->next) {
        printf("%d -> ", n->value);
    }
    // On aurait aussi pu faire :
    // node* n = l->first;
    // while (n != NULL) {
    //     printf("%d -> ", n->value);
    //     n = n->next;
    // }
    printf("NULL ]\n");
}

bool list_equals(const list* l, const int* values, int size) {
    node *n = l->first;
    for (int i = 0; i < size; ++i) {
        if (n == NULL || n->value != values[i]) {
            return false;
        }
        n = n->next;
    }
    return true;
}

list* list_create() {
    list* l = malloc(sizeof(list));
    l->first = NULL;
    l->last = NULL;

}
bool list_is_empty(const list* l) {
    if (l->first == NULL && l->last == NULL){
        return true;
    }
    else{
        return false;
    }

}

void list_push_front(list* l, int value) {
   node* new_node = malloc(sizeof(node));
   new_node->next = l->first;
   new_node->prev = NULL;
   new_node->value = value;
   l->first->prev = new_node;
   l->first=new_node;
   
}

void list_push_back(list* l, int value) {
   node* new_node = malloc(sizeof(node));
   new_node->prev = l->last;
   new_node->next = NULL;
   new_node->value = value;
   l->last->next = new_node;
   l->first=new_node;
   
}

int list_pop_front(list* l) {
    free(l->first->next);
    free(l->first);
}

int list_pop_back(list* l) {
    free(l->last->prev);
    free(l->last);
    
    
}

int list_size(const list* l) {
    if(l->first == NULL){
        return 0;
    }
    else{
        node *n= l->first;

        for (int i = 1;n= NULL; i++ ){
            n = n->next;
            if(n->next = NULL){
                return i;
            }

        }
        
        }
}


int list_get(const list* l, int index) {
    //je supose la liste non vide //
        node *n= l->first;
        for (int i = 0;i < index  ; i++ ){
            n = n->next;
                                        }
        return n->value;
}

void list_destroy(list* l) {
    if(l->first == NULL){
    free(l);             }
    else {
        for (node* n = l->first; n != NULL; n = n->next){
            free(n->prev);

                                                        }
        free(l);
        }
}


void list_clear(list* l) {
     for (node* n = l->first; n != NULL; n = n->next){
        n->value = 0;
     }
    
}

void list_insert(list* l, int value, int index) {
    node *n = l->first;
    for(int i=0;i<index;i++){
        n->next;
    }
    node *new_node = malloc(sizeof(node));
    new_node->value = value;
    new_node->next = n;
    new_node->prev = n->prev;
    n->prev->next= new_node;
    n->next->prev = new_node;
}

void list_remove(list* l, int index) {
    node *n = l->first;
    for(int i=0;i<index;i++){
        n->next;
    }
    n->prev->next = n->next;
    n->next->prev = n->prev;
    free(n);





}




int main(){
    return 0;
}
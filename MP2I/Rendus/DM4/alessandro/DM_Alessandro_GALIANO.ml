                                                          (* Dm des Vacances *)
                                                          (* Alessandro GALIANO *)
                                            (* Partie 1 *)
type monome = {
  mutable coeff: int;
  mutable degre: int
}

type polynome = monome list
let monome coeff degre = {coeff = coeff; degre = degre}

let rec transfer l l2 = 
  match l with
  |[] -> l2
  |t::q -> transfer q (t::l2)
let rev l = transfer l []
(* Question 1 *)


let rec print_polynome p = 
  match p with
  |[] -> Printf.printf "\n"
  |[t] -> Printf.printf " %d*X^%d " t.coeff t.degre;
           print_polynome []
  |t::q -> Printf.printf " %d*X^%d +" t.coeff t.degre;
           print_polynome q


(* Question 2 *)

let rec est_valide (p : polynome) = 
  match p with
  |[] -> true
  |[t] -> if t.coeff = 0 then false else est_valide []
  |t1::t2::q -> if t1.degre < t2.degre || t1.coeff = 0 then false
                else est_valide (t2::q)  

(* Question 3 *)

let degre p = 
  match p with
  |[] -> -1
  |t::q -> t.degre

(* Question 4 *)

let rec evaluation p (x : int) =
  match p with
  |[] -> 0
  |t::q -> t.coeff*(int_of_float(float_of_int(x)**(float_of_int(t.degre)))) + (evaluation q x) (* Pas très beau mais obligé de changer x en float pour \
                                                                                     pouvoir le mettre à la puissance puis \
                                                                                    de le rechanger en int pour pouvoir multiplier \
                                                                                    avec le coeff qui est entier*)


(* Question 5 *)

let rec composition p (n :int) = 
  match p with
  |[] -> 0
  |t::q -> t.degre <- t.degre*n; 
           composition q n

(* Question 6 *)

let rec somme_aux p1 p2 pf = 
  match p1, p2 with
  |[], [] -> pf
  |[], t::q -> somme_aux p1 q (t::pf)
  |t::q, [] -> somme_aux q p2 (t::pf)
  |t1::q1, t2::q2 -> if t1.coeff = (-t2.coeff) && t1.degre = t2.degre then [] (* Je retire directement cette somme \
                                                                              de polynome car elle sera renvoyée false dans la fonction est_valide *)
                     else if t1.degre > t2.degre then somme_aux q1 p2 (t1::pf)
                     else if t2.degre > t1.degre then somme_aux p1 q2 (t2::pf)
                     else let s_coeff = t1.coeff + t2.coeff in
                     somme_aux q1 q2 ((monome s_coeff t1.degre)::pf)

let somme p1 p2 = rev(somme_aux p1 p2 [])


let produit (p1 :polynome) (p2 : polynome) = 
  (* let p3 = [] in
  let m = [] in
  for i = 0 to (List.length p2)-1 do (
    for j = 0 to (List.length p1)-1 do (
      m = monome (((p1[j]).coeff)*((p2[i]).coeff)) ((p2[i]).degre + (p1[j]).degre);
      p3 <- somme [m] p3 )
    done;
  ) done
   *)
   Printf.printf "Je n'arrive pas à faire fonctionner cette double boucle, à cause d'une erreur que je n'arrive pas à résoudre"


  (* Question 7 *)
let rec derivee_aux p (p2 : polynome) = 
  match p with
  |[] -> p2
  |[t] -> p2
  |t::q -> derivee_aux q ((monome (t.degre*t.coeff) (t.degre-1))::p2)
let derivee p = rev(derivee_aux p [])

(* Test *)
let () = 
  let monome_1 = monome 1 2 in
  let monome_2 = monome (-2) 1 in
  let monome_3 = monome 1 0 in
  let monome_4 = monome 3 4 in
  let monome_5 = monome (-3) 4 in
  let monome_6 = monome 2 3 in
  let poly_1 = [monome_1; monome_2; monome_3] in
  let poly_3 = [monome_5; monome_6; monome_1; monome_2; monome_3] in
  let poly_4 = [monome_4; monome_6; monome_1; monome_2; monome_3] in
  let poly_2 = [monome_3; monome_1; monome_3] in
  print_polynome(poly_1);
  print_polynome(poly_2);
  assert (est_valide poly_1 == true);
  assert (est_valide poly_2 == false);
  assert (evaluation poly_1 2 == 1)





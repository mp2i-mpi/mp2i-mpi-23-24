type monome = {
  coeff : int;
  degre : int
}

type polynome = monome list

(* Question 1 *)
let q1 = [{coeff = 1; degre = 2}; {coeff = -2; degre = 1}; {coeff = 1; degre = 0}]

(* Question 2 *)
let rec est_valide_aux p d c =
  match p with
  | [] -> true
  | t::q ->
    if (c = 'a' || (t.degre < d && d >= 0)) && t.coeff != 0 then
      est_valide_aux q (t.degre) 'b'
    else
      false

let est_valide (p : polynome) =
  est_valide_aux p (-1) 'a'

(* Question 3 *)
let degre (p : polynome) =
  match p with
  | [] -> -1
  | t::q -> t.degre

(* Question 4 *)
let rec evaluation_aux p x res =
  match p with
  | [] -> res
  | t::q -> evaluation_aux q x (res+(t.coeff)*(int_of_float ((float_of_int x)**(float_of_int t.degre))))

let evaluation (p : polynome) (x : int) =
  evaluation_aux p x 0

(* Question 5 *)
let rec composition_aux p n res =
  match p with
  | [] -> res
  | t::q -> composition_aux q n (res@[{coeff = t.coeff; degre = n*(t.degre)}])

let composition (p : polynome) (n : int) =
  composition_aux p n []

(* Question 6 *)
let rec somme_aux p1 p2 res =
  if degre p1 > degre p2 then
    match p1 with
    | [] -> failwith "Polynôme invalide"
    | t::q -> 
      if t.coeff != 0 then
        somme_aux q p2 (res@[t])
      else 
        somme_aux q p2 res
  else if degre p1 < degre p2 then
    match p2 with
    | [] -> failwith "Polynôme invalide"
    | t::q ->
      if t.coeff != 0 then
        somme_aux p1 q (res@[t])
      else
        somme_aux p1 q res
  else
    match p1 with
    | [] -> res
    | t1::q1 ->
      match p2 with
      | [] -> res
      | t2::q2 -> 
        if t1.coeff + t2.coeff != 0 then
          somme_aux q1 q2 (res@[{coeff = t1.coeff + t2.coeff; degre = t1.degre}])
        else 
          somme_aux q1 q2 res

let somme (p1 : polynome) (p2 : polynome) =
  somme_aux p1 p2 []

let rec produit_mp_aux m p res =
  match p with
  | [] -> somme res []
  | t::q -> produit_mp_aux m q (res@[{coeff = (m.coeff)*(t.coeff); degre = (m.degre)+(t.degre)}])

let produit_mp (m : monome) (p : polynome) =
  produit_mp_aux m p []

let rec produit_aux p1 p2 res =
  match p1 with
  | [] -> res
  | t::q -> produit_aux q p2 (somme res (produit_mp t p2))

let produit (p1 : polynome) (p2 : polynome) =
  produit_aux p1 p2 []

(* Question 7 *)
let rec derivee_aux p res =
  match p with
  | [] -> res
  | t::q -> 
    if t.degre - 1 >= 0 then
      derivee_aux q (res@[{coeff = (t.coeff)*(t.degre); degre = (t.degre) - 1}])
    else
      derivee_aux q res

let derivee p =
  derivee_aux p []

let () =
  let p1 = q1 in (* X²-2X+1 *)
  let p2 = [{coeff = 0; degre = 2}; {coeff = -2; degre = 1}; {coeff = 1; degre = 0}] in (* 0X²-2X+1 *)
  let p3 = [{coeff = 1; degre = 2}; {coeff = -2; degre = 1}; {coeff = 0; degre = 0}] in (* X²-2X+0 *)
  let p4 = [{coeff = 1; degre = 2}; {coeff = -2; degre = -1}; {coeff = 1; degre = 0}] in (* X²-2X^(-1)+1 *)
  let p5 = [{coeff = 1; degre = 2}; {coeff = -2; degre = 3}; {coeff = 1; degre = 0}] in (* X²-2X^(3)+1 *)
  let p6 = [] in (* 0 *)
  let p7 = [{coeff = 2; degre = 0}] in (* 2 *)
  let p8 = [{coeff = 1; degre = 5}] in (* X^(5) *)
  (* Question 2 *)
  assert(est_valide p1);
  assert(not (est_valide p2));
  assert(not (est_valide p3));
  assert(not (est_valide p4));
  assert(not (est_valide p5));
  assert(est_valide p6);
  assert(est_valide p7);
  assert(est_valide p8);
  (* Question 3 *)
  assert(degre p1 = 2);
  assert(degre p6 = -1);
  assert(degre p7 = 0);
  assert (degre p8 = 5);
  (* Question 4 *)
  assert(evaluation p1 5 = 16);
  assert(evaluation p6 7 = 0);
  assert(evaluation p7 3 = 2);
  assert(evaluation p8 2 = 32);
  (* Question 5 *)
  assert(composition p1 2 = [{coeff = 1; degre = 4}; {coeff = -2; degre = 2}; {coeff = 1; degre = 0}]);
  assert(composition p6 3 = []);
  assert(composition p7 6 = [{coeff = 2; degre = 0}]);
  assert(composition p8 3 = [{coeff = 1; degre = 15}]);
  assert(est_valide (composition p1 7));
  (* Question 6 *)
  assert(somme p1 p1 = [{coeff = 2; degre = 2}; {coeff = -4; degre = 1}; {coeff = 2; degre = 0}]);
  assert(somme p1 p6 = p1 );
  assert(somme p1 p7 = [{coeff = 1; degre = 2}; {coeff = -2; degre = 1}; {coeff = 3; degre = 0}]);
  assert(somme p1 p8 = [{coeff = 1; degre = 5}; {coeff = 1; degre = 2}; {coeff = -2; degre = 1}; {coeff = 1; degre = 0}]);
  assert(somme p6 p6 = p6);
  assert(somme p7 p8 = [{coeff = 1; degre = 5}; {coeff = 2; degre = 0}]);
  assert(somme p1 [{coeff = -1; degre = 2}] = [{coeff = -2; degre = 1}; {coeff = 1; degre = 0}]);
  assert(est_valide (somme p1 p8));
  assert(produit_mp {coeff = 0; degre = 6} p1 = p6);
  assert(produit_mp {coeff = 7; degre = 6} p6 = p6);
  assert(produit_mp {coeff = 7; degre = 6} p7 = [{coeff = 14; degre = 6}]);
  assert(produit_mp {coeff = 7; degre = 6} p8 = [{coeff = 7; degre = 11}]);
  assert(est_valide (produit_mp {coeff = 7; degre = 6} p1));
  assert(produit p1 p1 = [{coeff = 1; degre = 4}; {coeff = -4; degre = 3}; {coeff = 6; degre = 2}; {coeff = -4; degre = 1}; {coeff = 1; degre = 0}]);
  assert(produit p1 p6 = p6);
  assert(produit p6 p1 = p6);
  assert(produit p1 p7 = [{coeff = 2; degre = 2}; {coeff = -4; degre = 1}; {coeff = 2; degre = 0}]);
  assert(produit p1 p8 = [{coeff = 1; degre = 7}; {coeff = -2; degre = 6}; {coeff = 1; degre = 5}]);
  assert(est_valide (produit p1 p1));
  (* Question 6 *)
  assert(derivee p1 = [{coeff = 2; degre = 1}; {coeff = -2; degre = 0}]);
  assert(derivee p6 = p6);
  assert(derivee p7 = p6);
  assert(derivee p8 = [{coeff = 5; degre = 4}]);
  assert(est_valide (derivee p1))
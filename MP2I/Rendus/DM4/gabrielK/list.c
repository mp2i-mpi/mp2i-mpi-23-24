#include "list.h"

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

typedef struct Node {
    int value;
    struct Node *next;
    struct Node *prev;
} node;

typedef struct List {
    node *first;
    node *last;
} list;

void print_list(const list* l) {
    printf("[ ");
    // Une utilisation astucieuse de la boucle for
    // afin d'itérer sur ce genre de structures :
    for (node* n = l->first; n != NULL; n = n->next) {
        printf("%d -> ", n->value);
    }
    // On aurait aussi pu faire :
    // node* n = l->first;
    // while (n != NULL) {
    //     printf("%d -> ", n->value);
    //     n = n->next;
    // }
    printf("NULL ]\n");
}

bool list_equals(const list* l, const int* values, int size) {
    node *n = l->first;
    for (int i = 0; i < size; ++i) {
        if (n == NULL || n->value != values[i]) {
            return false;
        }
        n = n->next;
    }
    return true;
}

list* list_create() {
    list* l = malloc(sizeof(list));
    if (l == NULL){
        printf("Echec de l'allocation de la mémoire.\n");
        exit(1);
    }
    l->first = NULL;
    l->last = NULL;
    return l;
}

bool list_is_empty(const list* l) {
    return (l->first == NULL && l->last == NULL);
}

void list_push_front(list* l, int value) {
    node* n = malloc(sizeof(node));
    if (n == NULL){
        printf("Echec de l'allocation de la mémoire.\n");
        exit(2);
    }
    n->value = value;
    n->next = l->first;
    n->prev = NULL;
    if (l->first != NULL) {
        l->first->prev = n;
    }
    else {
        l->last = n;
    }
    l->first = n;
}

void list_push_back(list* l, int value) {
    node* n = malloc(sizeof(node));
    if (n == NULL){
        printf("Echec de l'allocation de la mémoire.\n");
        exit(3);
    }
    n->value = value;
    n->next = NULL;
    n->prev = l->last;
    if (l->last != NULL) {
        l->last->next = n;
    }
    else {
        l->first = n;
    }
    l->last = n;
}

int list_pop_front(list* l) {
    if (list_is_empty(l)) {
        printf("Erreur : il est impossible de retirer le premier élément d'une liste vide\n");
        exit(4);
    }
    node* first_node = l->first;
    int value = first_node->value;
    l->first = l->first->next;
    if (l->first != NULL) {
        l->first->prev = NULL;
    } else {
        l->last = NULL;
    }
    free(first_node);
    return value;
}

int list_pop_back(list* l) {
    if (list_is_empty(l)) {
        printf("Erreur : il est impossible de retirer le dernier élément d'une liste vide\n");
        exit(5);
    }
    node* last_node = l->last;
    int value = last_node->value;
    l->last = l->last->prev;
    if (l->last != NULL) {
        l->last->next = NULL;
    } else {
        l->first = NULL;
    }
    free(last_node);
    return value;
}

int list_size(const list* l) {
    if (list_is_empty(l)){
        return 0;
    }
    else {
        int i = 1;
        node* n = l->first;
        while (n->next != NULL){
            n = n->next;
            ++i;
        }
        return i;
    }
}

int list_get(const list* l, int index) {
    if (index > list_size(l)) {
        printf("L'indice donné est supérieur à la longueur de la liste.\n");
        exit(6);
    }
    node* n = l->first;
    for (int i = 0; i < index; ++i){
        n = n->next;
    }
    return n->value;
}

void list_clear(list* l) {
    while (!list_is_empty(l)){
        list_pop_back(l);
    }
    l->first = NULL;
    l->last = NULL;
}

void list_destroy(list* l) {
    list_clear(l);
    free(l);
}

void list_insert(list* l, int value, int index) {
    if (l == NULL) {
        printf("Erreur : la liste n'a pas été initialisée.\n");
        exit(7);
    }
    if (index < 0 || index > list_size(l)) {
        printf("Erreur : l'indice donné est négatif ou supérieur à la longueur de la liste.\n");
        exit(8);
    }
    if (index == 0) {
        list_push_front(l, value);
    } else if (index == list_size(l)) {
        list_push_back(l, value);
    } else {
        node* n = l->first;
        for (int i = 0; i < index - 1; ++i) {
            n = n->next;
        }
        node* new_node = malloc(sizeof(node));
        new_node->value = value;
        new_node->next = n->next;
        new_node->prev = n;
        n->next->prev = new_node;
        n->next = new_node;
    }
}

void list_remove(list* l, int index) {
    if (l == NULL) {
        printf("Erreur : la liste n'a pas été initialisée.\n");
        exit(9);
    }
    if (index < 0 || index >= list_size(l)) {
        printf("Erreur : l'indice donné est négatif ou supérieur à la longueur de la liste.\n");
        exit(10);
    }
    if (index == 0) {
        list_pop_front(l);
    } else if (index == list_size(l) - 1 ) {
        list_pop_back(l);
    } else {
        node* n1 = l->first;
        for (int i = 0; i < index - 1; ++i) {
            n1 = n1->next;
        }
        node* n2 = n1->next;
        node* n3 = n2->next;
        n1->next = n3;
        n3->prev = n1;
        free(n2);
    }
}

void list_from_array(list* l, const int* values, int n_values){
    for (int i = 0; i < n_values; i++) {
        list_push_back(l, values[i]);
    }
}
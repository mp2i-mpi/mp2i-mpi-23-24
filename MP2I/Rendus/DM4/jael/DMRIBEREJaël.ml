



**Exercice1**
let polynome_exemple = [
  {coeff = 1; degre = 2};
  {coeff = -2; degre = 1};
  {coeff = 1; degre = 0}
];

**Exercice2**
let est_valide (p : polynome) : bool =
  List.for_all (fun monome -> monome.coeff <> 0) p
  && List.sort (fun m1 m2 -> m2.degre - m1.degre) p = p;

**Exercice3**
let degre (p : polynome) : int =
  match p with
  | [] -> -1
  | monome :: _ -> monome.degre;

**Exercice4$$
let rec evaluation (p : polynome) (x : int) : int =
  match p with
  | [] -> 0
  | monome :: rest -> monome.coeff * (x ** monome.degre) + evaluation rest x;


**Exercice5**
let composition (p : polynome) (n : int) : polynome =
  List.map (fun monome -> { coeff = monome.coeff; degre = monome.degre * n }) p;


**Exercice6**
let rec somme (p1 : polynome) (p2 : polynome) : polynome =
  match (p1, p2) with
  | ([], p) -> p
  | (p, []) -> p
  | (m1 :: rest1, m2 :: rest2) when m1.degre = m2.degre ->
      { coeff = m1.coeff + m2.coeff; degre = m1.degre } :: somme rest1 rest2
  | (m1 :: rest1, m2 :: rest2) when m1.degre > m2.degre ->
      m1 :: somme rest1 (m2 :: rest2)
  | (m1 :: rest1, m2 :: rest2) ->
      m2 :: somme (m1 :: rest1) rest2;;



let rec produit (p1 : polynome) (p2 : polynome) : polynome =
  match (p1, p2) with
  | ([], _) | (_, []) -> []
  | (m1 :: rest1, p) ->
      somme (produit_par_monome m1 p) (produit rest1 p)
and produit_par_monome (m : monome) (p : polynome) : polynome =
  List.map (fun monome -> { coeff = m.coeff * monome.coeff; degre = m.degre + monome.degre }) p;;

**Exercice7**
let derivee (p : polynome) : polynome =
  List.filter (fun monome -> monome.degre > 0) (List.map (fun monome -> { coeff = monome.coeff * monome.degre; degre = monome.degre - 1 }) p);;

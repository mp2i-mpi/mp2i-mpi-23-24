#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

#include <stdbool.h>

struct Node;
typedef struct Node node;
struct List;
typedef struct List list;

// Déja fourni en exemple
void print_list(const list* l);




// Utile pour les tests
// (pour comparer le contenu d'une liste à celui d'un tableau)
bool list_equals(const list* l, const int* values, int n_values);

// À implémenter

//FOnction d'erreur pour les cas où on en a besoin
int error_handler(const char* message) {
    fprintf(stderr, "Error: %s\n", message);
    return -1;  



// Crée une liste vide
list* list_create(){
    List* l = (List*)malloc(sizeof(List));
    if (l != NULL) {
        l->head = NULL;
    }
    return l;
}



// Renvoie true si la liste est vide, false sinon
bool list_is_empty(const list* l){
    return l->head == NULL;
}




// Ajoute un élément en tête de la liste
void list_push_front(list* l, int value); {
    Node* new_node = (Node*)malloc(sizeof(Node));
    if (new_node != NULL) {
        new_node->value = value;
        new_node->next = l->head;
        l->head = new_node;
    }
}



// Ajoute un élément en queue de la liste
void list_push_back(list* l, int value);{
    Node* new_node = (Node*)malloc(sizeof(Node));
    if (new_node != NULL) {
        new_node->value = value;
        new_node->next = NULL;
        if (list_is_empty(l)) {
            l->head = new_node;
        } else {
            Node* current = l->head;
            while (current->next != NULL) {
                current = current->next;
            }
            current->next = new_node;
        }
    }
}


// Retire et renvoie le premier élément de la liste
int list_pop_front(list* l);{
    int value = 0; 
    if (!list_is_empty(l)) {
        Node* temp = l->head;
        value = temp->value;
        l->head = temp->next;
        free(temp);
    } else {
        error_handler("Impossible dans le cas d'une liste vide");
    }

    return value;
}


// Retire et renvoie le dernier élément de la liste
int list_pop_back(list* l);{
    int value = 0; // Valeur par défaut si la liste est vide

    if (!list_is_empty(l)) {
        if (l->head->next == NULL) {
            // Single node in the list
            value = l->head->value;
            free(l->head);
            l->head = NULL;
        } else {
            Node* current = l->head;
            while (current->next->next != NULL) {
                current = current->next;
            }
            value = current->next->value;
            free(current->next);
            current->next = NULL;
        }
    } else {
        error_handler("Impossible dans le cas d'une liste vide");
    }

    return value;
}



// Supprime tous les éléments de la liste
// (la liste est vide après cet appel)
// (ne pas oublier de libérer la mémoire)
void list_clear(list* l);{
    while (!list_is_empty(l)) {
        list_pop_front(l);
    }
}



// Libère la mémoire associée à la liste
// (la liste ne doit plus être utilisée après cet appel)
// (ne pas oublier de libérer les éléments d'abord)
void list_destroy(list* l);{
    list_clear(l);
    free(l);
}



// Renvoie le nombre d'éléments dans la liste
int list_size(const list* l);{
    int size = 0;
    Node* current = l->head;
    while (current != NULL) {
        size++;
        current = current->next;
    }
    return size;
}



// Renvoie l'élément à l'index donné
int list_get(const list* l, int index);{
    if (index < 0 || index >= list_size(l)) {
        exit(EXIT_FAILURE); // You may choose a different way to handle out-of-bounds.
    }

    Node* current = l->head;
    for (int i = 0; i < index; i++) {
        current = current->next;
    }

    return current->value;
}



// Modifie l'élément à l'index donné
void list_insert(list* l, int value, int index) {
    if (index < 0 || index > list_size(l)) {
        error_handler("Cet indice n'est pas approprié");
        return;
    }

    if (index == 0) {
        list_push_front(l, value);
    } else if (index == list_size(l)) {
        list_push_back(l, value);
    } else {
        Node* new_node = (Node*)malloc(sizeof(Node));
        if (new_node != NULL) {
            new_node->value = value;

            Node* current = l->head;
            for (int i = 0; i < index - 1; i++) {
                current = current->next;
            }

            new_node->next = current->next;
            current->next = new_node;
  
}



// Supprime l'élément à l'index donné
void list_remove(list* l, int index);{
if (index < 0 || index >= list_size(l)) {
        error_handler("Cet indice n'est pas approprié");
        return;
    }

    if (index == 0) {
        Node* temp = l->head;
        l->head = temp->next;
        free(temp);
    } else {
        Node* current = l->head;
        for (int i = 0; i < index - 1; i++) {
            current = current->next;
        }

        Node* temp = current->next;
        current->next = temp->next;
        free(temp);
    }
}



// Crée une liste à partir d'un tableau
void list_from_array(list* l, const int* values, int n_values);{
    for (int i = 0; i < n_values; i++) {
        list_push_back(l, values[i]);
    }
}

#endif // LIST_H_INCLUDED

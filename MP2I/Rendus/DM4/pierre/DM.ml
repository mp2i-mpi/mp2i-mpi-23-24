type monome = {coeff: int; degre: int};;
type polynome = monome list;;
(*Q1*)
let p1 = [{coeff= 1; degre= 2}; {coeff= -2; degre= 1}; {coeff= 1; degre= 0}];;
(*Q2*)
let rec est_valide p = match p with
  | [] -> true 
  | u :: v ->
    if List.length v = 0 || u.degre > (List.hd v).degre then
      if u.coeff != 0 then
        est_valide v
      else
        false
    else
      false;;
(*Q3*)
let degre p = match p with
  | [] -> -1
  | u :: v -> u.degre;;
(*Q4*)
let rec puissInt x n = match n with
  | 0 -> 1
  | _ -> x * puissInt x (n-1);;

let evalue b p =
  let a = ref 0 in
  for i = 0 to (Array.length p)-1 do
    a := !a + p.(i) * puissInt b i
  done;
  !a;;
(*Q5*)
let rec composition p n = match p with
  | [] -> []
  | u :: v -> {coeff=u.coeff; degre=(u.degre)*n} :: composition v n;;
(*Q6*)
let rec somme p q = match p, q with
  | [], [] -> []
  | p , [] -> p
  | [], q -> q
  | u :: v, w :: z ->
    if u.degre = w.degre then
      if u.coeff + w.coeff != 0 then
        {coeff=u.coeff + w.coeff; degre=u.degre} :: (somme v z)
      else
        somme v z
    else
      if u.degre > w.degre then
        u :: (somme v q)
      else
        w :: (somme z p);;
(*Q7*)
(*let p2 = [{coeff=3; degre=3}; {coeff=2; degre=2}; {coeff=(-3); degre=1}; {coeff=(-1); degre=0}];;*)
let print_monome a =
   let c = string_of_int a.coeff in
   let d = string_of_int a.degre in
   print_string("{coeff=" ^ c ^ "; degre=" ^ d ^ "}");;

 let rec print_list lst = match lst with
   | [] -> ()
   | u :: v -> print_monome u;
       print_string";";
       print_list v;
       print_newline ();;


let rec produit p q = match p, q with
  | [], [] -> []
  | [], q -> []
  | p, [] -> []
  | u :: v, q ->
      let lst = ref [] in
      let b = ref q in
      while !b != [] do
        lst := {coeff= u.coeff * (List.hd !b).coeff; degre= u.degre +(List.hd !b).degre} :: !lst;
        b := List.tl !b;
      done;
      lst := List.rev !lst; 
      somme (produit v q) !lst;;

let rec derivee p = match p with
  | [] -> []
  | u :: v ->
    if u.degre > 0 then
      {coeff= u.coeff * u.degre; degre =(u.degre-1)} :: derivee v
    else
      [];;
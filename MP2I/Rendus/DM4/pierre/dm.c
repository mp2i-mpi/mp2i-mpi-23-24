#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
                                                // Partie 2 DM
typedef struct Node {
    int value;
    struct Node *next;
    struct Node *prev;
} node;

typedef struct List {
    node *first;
    node *last;
} list;

void print_list(const list* l) {
    printf("[ ");
    // Une utilisation astucieuse de la boucle for
    // afin d'itérer sur ce genre de structures :
    for (node* n = l->first; n != NULL; n = n->next) {
        printf("%d -> ", n->value);
    }
    // On aurait aussi pu faire :
    // node* n = l->first;
    // while (n != NULL) {
    //     printf("%d -> ", n->value);
    //     n = n->next;
    // }
    printf("NULL ]\n");
}

bool list_equals(const list* l, const int* values, int size) {
    node *n = l->first;
    for (int i = 0; i < size; ++i) {
        if (n == NULL || n->value != values[i]) {
            return false;
        }
        n = n->next;
    }
    return true;
}

list* list_create() {
    list* l = malloc(sizeof(list));
    l->first = NULL;
    l->last = NULL;
    return l;
}

bool list_is_empty(const list* l) {
    if (l->first == NULL) {
        return true;
    }
    return false;
}

void list_push_front(list* l, int value) {
    node * n1 = malloc(sizeof(node));
    n1->value = value;
    n1->prev = NULL;
    n1->next = l->first;
    list* l2 = malloc(sizeof(list));
    l2->first = n1;
    l2->last = l->last;
    l = l2;
}

void list_push_back(list* l, int value) {
    node* n1 = malloc(sizeof(node));
    list* l2 = malloc(sizeof(list));
    n1->value = value;
    n1->prev = l->last;
    n1->next = NULL;
    l2->last = n1;
    l2->first = l->first;
    l = l2;
}

int list_pop_front(list* l) {
    list* l2 = malloc(sizeof(list));
    node*n1 = malloc(sizeof(node));
    node*n2 = malloc(sizeof(node));
    n1->prev = l->first;
    n2->value = (l->first)->value;
    l2->first = n1;
    l2->last = l->last;
    l = l2;
    return n2->value;
}

int list_pop_back(list* l) {
    list* l2 = malloc(sizeof(list));
    node* n1 = malloc(sizeof(node));
    node* n2 = malloc(sizeof(node));
    n1->next = l->last;
    n2->value = (l->last)->value;
    l2->last = n1;
    l2->first = l->first;
    l = l2;
    return n2->value;
}

int list_size(const list* l) {
    int size = 0;
    for (node* n = l->first; n!= NULL; n->next) {
        size++;
    }
    return size;
}

int list_get(const list* l, int index) {
    int i = 0;
    for (node* n = l->first; n!=NULL; n->next) {
        if (i == index) {
            return n->value;
        }
        i++;
    }
    return 0;
}

void list_clear(list* l) {
    for (node* n = l->first; n != NULL; n->next) {
        list_pop_front(l);
        free(n);
    }
}

void list_destroy(list* l) {
    list_clear(l);
    free(l);
}

void list_insert(list* l, int value, int index) {
    int i = 0;
    node* n2 = malloc(sizeof(node));
    node* n3 = malloc(sizeof(node));
    n2->value = value;
    for (node* n = l->first; n!=NULL; n->next) {
        if (i == index) {
            n3 = n->next;
            n->next = n2;
            n2->next = n3;
        }
        i++;
    }
}

void list_remove(list* l, int index) {
    int i = 0;
    node* n2 = malloc(sizeof(node));
    node* n3 = malloc(sizeof(node));
    for (node* n = l->first; n!=NULL; n->next) {
        if (i == (index-1)) {
            n3 = n->next;
            n2 = n3->next;
            n->next = n2;
        }
        i++;
    }
}

void list_from_array(list* l, const int* values, int n_values) {
    int i = 0;
    for (node* n = l->first; n!=NULL; n->next) {
        if (i >= n_values) {
            n->next = NULL;
        }
        n->value = values[i];
        i++;
    }
}
int mult_russe(int a, int b) {
    int res = 0;
    while (a != 0) {
        if (a % 2 == 0) {
            a = a / 2;
            b = b * 2;
        } else {
            a = a / 2;
            res = res + b;
            b = b * 2;
        }
    }
    return res;
}

int mult_russe_2(int a, int b) {
    int res = 0;
    while (a != 0) {
        int q = a / 2;
        int r = a % 2;
        a = q;
        if (r == 1) {
            res = res + b;
        }
        b = b*2;
    }
    return res;
}
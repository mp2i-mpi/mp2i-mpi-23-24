let nb2 (t: int array) (i: int) (j: int): int = failwith "non fait"

let rec dpr_simple (t: int array) (i: int) (j: int): (int*int) =
  if j < i then
    (-1, 0)
  else if j = i then
    (t.(i), 1)
  else (
    let m = (j-i)/2 in
    let (a1, q1) = dpr_simple t i (i+m) in
    let (a2, q2) = dpr_simple t (i+m+1) j in
    match (a1, a2) with
    | -1, -1 -> (-1, 0)
    | a, -1 -> (
      let a_total = q1 + (nb2 t (i+m+1) j) in
      if a_total > m then (a, a_total) else (-1, 0)
    )
    | -1, a -> (
      let a_total = q2 + (nb2 t i (i+m)) in
      if a_total > m then (a, a_total) else (-1, 0)
    )
    | a, b -> (
      let a_total = q1 + (nb2 t (i+m+1) j) in
      let b_total = q2 + (nb2 t i (i+m)) in
      if a_total > m then
        (a, a_total)
      else if b_total > m then
        (b, b_total)
      else
        (-1, 0)
    )
  )
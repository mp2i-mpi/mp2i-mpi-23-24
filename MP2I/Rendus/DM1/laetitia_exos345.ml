(*EXERCICE 3*)
(*creer une fonction min_l : 'a list -> int qui renvoie le minimum d'une liste d'entiers*)

let rec min_l l =
  match l with (*on compare la valeur l vec différents motifs*)
  | [] -> failwith "Liste vide" (*permet d'interrompre l'exection du programme *)
  | [element] -> element 
  | element1 :: element2 :: elementn -> min_l (min (element1 (min_l (element2 :: elementn)))) (* le "::" permet d'ajouter un élément à une liste. Dans cette ligne on prend le min de element 1 et element 2 puis par recurssion on utilise min_l pourle reste de laliste.*)
  
  let test_min_l () =
    assert (min_l [3] = 3);
    assert (min_l [5; -2] = -2);
    assert (min_l [-2; 5] = -2);
    assert (min_l [-3; 12; 7; 4; -27; 1; 3; 43] = -27)
  let () = test_min_l ()



(*EXERCICE 4*)
(*ecrire une fonction print_int_list : int list -> unit qui prend en paramètre une liste d'entiers et 
affiche tous les élémenrs de la liste utiliser cette fonction et vérifier que les bonnes valeurs sont affichées*)

let rec print_int_list l =
  match l with
  | [] -> failwith "Liste vide"
  | [element] -> print_int element
  | element :: autres -> print_int element;
       print_int_list autres 

let () = print_int_list [12; 3; -5; 12; 7];
  print_newline()

(*EXERCICE 5*)
(*Ecrire une fonction read_int_list : int -> int list qui prend
en paramètre un entier n demande à l’utilisateur d’entrer n entiers et renvoie le
resultat sous forme de liste ; on pourra utiliser l’exercice precedent pour afficher
la liste obtenue et verifier que la fonction est correcte*)

let rec read_int_list entiers acc= (*le acc permet d'accumuler les éléments dans la liste *)
  if entiers <=0 then
    [] (*liste vide*)
  else 
    let entiers = read_int () in
    read_int_list (entiers - 1) (entiers :: acc) (* à chaque fois qu'on accumule un entier, il reste un entier de moins à entrer (c'est pour ça qu'on utilise n-1), et la fonction étant recurssive on peut continuer d'accumuler des entiers*)

let l = read_int_list 5 []
  let () =
    print_string " Tu as entre les nombres suivants : " ;
    print_int_list l;
    print_newline ();;


(*Utiliser la fonction dans un programme qui demande aussi le nombre n `a
l’utilisateur ; ainsi, l’utilisateur peut choisir la taille de la liste*)

  let rec read_int_list entiers acc =
    if entiers <= 0 then
      []
    else
      let entier = read_int () in
      read_int_list (entiers - 1) (entier :: acc) (*on met à jour la liste avec chaque nouvelle valeur grâce à la fonction acc et on procède par recurssion*)
  
  let () =
    print_string "Entrez la valeur de n : ";
    let n = read_int () in
    let ma_liste = read_int_list n [] in
    print_string "La liste d'entiers que vous avez entrée est : ";
    print_int_list ma_liste;
    print_newline ();;


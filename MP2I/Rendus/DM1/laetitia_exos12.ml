(*EXERCICE 1*)
(*faire un programme qui demande à l'utilisteur deux nombre et vérifie si le premier divise le second*)

let rec nombre_premier () =
  print_string "Choisis un nombre a : ";
  let a = read_int () in

  if a = 0 then
    (
      print_string "Recommence\n";
      nombre_premier ()
    )
  else
    (
      print_string "Choisis un nombre b : ";
      let b = read_int () in

      if b = 0 then
        (
          print_string "Recommence\n";
          nombre_premier ()
        )
      else if a mod b = 0 then
        (
          print_string "a ne divise pas b, choisis une autre combinaison.\n";
          nombre_premier ()
        )
      else
        Printf.printf "%d divise %d!\n" a b
    )

let () =
  nombre_premier ()



(*EXERCIE 2*)
(*ecrire une fonction pgcd : int -> int -> int qui prend en argument deux entiers et renvoie leur pgcd*)
(*signifie que la fonction prend deux arguments entiers et renvoie un résultat entier*)
let rec pgcd a b = 
  if a = 0 || b = 0 then 
    1
else pgcd b (a mod b) 
    
let () =
  Printf.printf "Entrez la valeur de a :";
  let a = read_int () in
  Printf.printf "entrez la valeur de b :";
  let b = read_int () in
  let resultat = pgcd a b in 
Printf.printf "Le PGCD de %d et %d est : %d" a b resultat 

(*utiliser cette fonction pour faire un programme qui demande deux nombres à l'utilisateur et vérifie si ils sont premiers entre eux*)
(*on sait que deux nombres sont premiers entre eux si leur PGCD vaut 1*)

let rec pgcd a b = 
  if a = 0 || b = 0 then 
    1
else pgcd a (b mod a) 
    
let nombres_premiers () =
  Printf.printf "Entrez la valeur de a :";
  let a = read_int () in
  Printf.printf "entrez la valeur de b :";
  let b = read_int () in
  let renvoyer = pgcd a b in 
  if renvoyer = 1 then 
    (
      Printf.printf "Les nombres %d et %d sont premeirs entre eux" a b 
    )
else 
  (
    print_string "Les deux nombres ne sont pas premeirs entre eux"
  )
let () =
  nombres_premiers ()


  
 
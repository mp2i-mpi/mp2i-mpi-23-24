(*          DM1 : du 23/10 au 29/10          *)

(*      Exercice 1      *)
let divise n d =
  if n mod d = 0 then
    Printf.printf "%d divise %d\n" d n
  else
    Printf.printf "%d ne divise pas %d\n" d n

(* let () =
  print_string ("Donnez un nombre entier : ");
  let n = read_int() in
  print_string ("Donnez un second nombre entier : ");
  let d = read_int() in
  divise n d *)


(*      Exercice 2      *)
(* let rec pgcd a b i =
  if a mod i = 0 && b mod i = 0 then
    i
  else
    pgcd a b (i - 1)
    
let premier a b =
  let d = min (abs a) (abs b) in
  if pgcd a b d = 1 then
    Printf.printf "%d et %d sont premiers entre eux\n" a b
  else
    Printf.printf "%d et %d ne sont pas premiers entre eux\n" a b

let () =
  print_string ("Donnez un nombre entier : ");
  let a = read_int() in
  print_string ("Donnez un second nombre entier : ");
  let b = read_int() in
  premier a b *)

  
(*      Exercice 3      *)
(* let rec min_l l =
  match l with
  | [] -> failwith "Liste vide"
  | [x] -> x
  | x :: reste -> min x (min_l reste)
  

let test_min_l () =
  assert (min_l [3] = 3);
  assert (min_l [5; -2] = -2);
  assert (min_l [ -2; 5] = -2);
  assert (min_l [ -3; 12; 7; 4; -27; 1; 3; 43] = -27)

let () = test_min_l () *)


(*      Exercice 4      *)
let rec print_int_list l =
  match l with
  | [] -> ()
  | debut :: fin ->
    print_int debut;
    print_string " ";
    print_int_list fin

(* let () = print_int_list [12; 3; -5; 12; 7] *)


(*      Exercice 5      *)
let rec read_int_list n =
  if n <= 0 then
    []
  else
    let x = read_int () in
    x :: read_int_list (n - 1)
  ;;

(* let () =
  print_string "Entrez le nombre d'entiers : ";
  let n = read_int () in
  print_string "Entrez les entiers un par un : ";
  let liste = read_int_list n in
  print_string "Tu as entré les nombres suivants : ";
  print_int_list liste;
  print_newline () *)


(*      Exercice 6      *)
(* let rec read_int_list_2 ch =
  let ch = read_line () in
  if ch = "fin" then
    []
  else
    let x = int_of_string ch in
    x :: read_int_list_2 ch  

let () =
  print_string "Entrez les entiers un par un et 'fin' lorsque vous avez terminé : ";
  let liste = read_int_list_2 "fin" in
  print_string "Tu as entré les nombres suivants : ";
  print_int_list liste;
  print_newline (); *)

(* val read_int_list_2 : string -> int list = <fun> *)


(*      Exercice 7      *)
let rec mem liste a =
  match liste with
  | [] -> false
  | debut :: fin -> 
    if debut = a then
      true
    else
      mem fin a

let () =
  let liste = [1; 2; 3; 4; 5] in
  let a = 8 in
  if mem liste a then
    Printf.printf "%d fait partie de la liste.\n" a
  else
    Printf.printf "%d ne fait pas partie de la liste.\n" a
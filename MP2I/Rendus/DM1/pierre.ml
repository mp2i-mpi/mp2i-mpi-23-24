let n = read_int()
let d = read_int()
if n mod d == 0 
  print_endline(d "divise" n  "!")
else : 
  print_endline("perdu")
;;


let a = read_int()
let b = read_int()
let pgcd a b =
  let c = val min(a, b) in 
  let rec a b = 
    if a >= b
      if a mod c == 0:
        print_endline(c)
      else: 
       if c == 1 
        print_endline(a "et" b "sont PEE")
       else :
        c = c mod a
    else  
      if b mod c == 0: 
        print_endline(c)
      else
        if c == 1 
          print_endline(a "et" b "sont PEE")
        else 
          c = c mod b
      ;;   

let rec min_l l =
  match l with
  |[] -> failwith "liste vide"
  |[t] -> q
  |t::q ->
    let min_reste = min_l q in
    if t < min_reste 
      t
    else  
      min_reste 
    ;;



let rec print_int_list l =
  match l with
  | [] -> ()
  | d::t ->
  print_int d;
  print_string " ";
  print_int_list t
  ;;
      
    ;;



let rec read_int_list n =
  if n = 0 then
    []
  else
    let x = read_int () in
    x :: read_int_list (n - 1)
;;

let () =
  print_string "Entrez le nombre d'entiers : ";
  let n = read_int () in
  print_string "Entrez les nombres : ";
  let l = read_int_list n in
  print_string "Tu as entré les nombres suivants : ";
  print_int_list l;
  print_newline ()


let rec read_int_list "finito" =
  print_string "Entrez un entier (ou \" ^ finito ^ "\" pour terminer) : ";
  let input = read_line () in
  if input = finito 
    []
  else
    let x = int_of_string input in
    x :: read_int_list finito
;;

let rec mem l e =
  match l with
  | [] -> false
  | d::t -> (d = e) || mem t e
;;
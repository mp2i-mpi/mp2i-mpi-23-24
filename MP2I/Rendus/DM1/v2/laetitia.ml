(*EXERCICE 1*)
(*faire un programme qui demande à l'utilisteur deux nombre et vérifie si le premier divise le second*)

let rec nombre_premier () =
  print_string "Choisis un nombre a : ";
  let a = read_int () in

  if a = 0 then
    (
      print_string "Recommence\n";
      nombre_premier ()
    )
  else
    (
      print_string "Choisis un nombre b : ";
      let b = read_int () in

      if b = 0 then
        (
          print_string "Recommence\n";
          nombre_premier ()
        )
      else if b mod a = 0 then
        (
          print_string "a divise b!\n";
        )
      else
        Printf.printf "%d ne divise pas %d, choisis une autre combinaison.\n" a b;
      nombre_premier ()
    )

let () =
  nombre_premier ()

(*EXERCIE 2*)
(*ecrire une fonction pgcd : int -> int -> int qui prend en argument deux entiers et renvoie leur pgcd*)
(*signifie que la fonction prend deux arguments entiers et renvoie un résultat entier*)
let rec pgcd a b = 
  if b = 0 then 
    a
else pgcd b (a mod b) 
    
let () =
  Printf.printf "Entrez la valeur de a :";
  let a = read_int () in
  Printf.printf "entrez la valeur de b :";
  let b = read_int () in
  let resultat = pgcd a b in 
Printf.printf "Le PGCD de %d et %d est : %d" a b resultat 

(*utiliser cette fonction pour faire un programme qui demande deux nombres à l'utilisateur et vérifie si ils sont premiers entre eux*)
(*on sait que deux nombres sont premiers entre eux si leur PGCD vaut 1*)

let rec pgcd a b = 
  if b = 0 then 
    a
else pgcd b (a mod b) 
    
let nombres_premiers () =
  Printf.printf "Entrez la valeur de a :";
  let a = read_int () in
  Printf.printf "entrez la valeur de b :";
  let b = read_int () in
  let renvoyer = pgcd a b in 
  if renvoyer = 1 then 
    (
      Printf.printf "Les nombres %d et %d sont premeirs entre eux" a b 
    )
else 
  (
    print_string "Les deux nombres ne sont pas premeirs entre eux"
  )
let () =
  nombres_premiers ()


(*EXERCICE 3*)
(*creer une fonction min_l : 'a list -> int qui renvoie le minimum d'une liste d'entiers*)

let rec min_l l =
  match l with
  | [] -> failwith "Liste vide"
  | [element] -> element
  | element1 :: element2 :: elementn -> min_l (min element1 element2 :: elementn)

let test_min_l () =
  assert (min_l [3] = 3);
  assert (min_l [5; -2] = -2);
  assert (min_l [-2; 5] = -2);
  assert (min_l [-3; 12; 7; 4; -27; 1; 3; 43] = -27)

let () = test_min_l ()

(*EXERCICE 4*)
(*ecrire une fonction print_int_list : int list -> unit qui prend en paramètre une liste d'entiers et 
affiche tous les élémenrs de la liste utiliser cette fonction et vérifier que les bonnes valeurs sont affichées*)

let rec print_int_list l =
  match l with
  | [] -> failwith "Liste vide"
  | [element] -> print_int element
  | element :: autres -> print_int element;
       print_int_list autres 

let () = print_int_list [12; 3; -5; 12; 7];
  print_newline()

(*EXERCICE 5*)
(*Ecrire une fonction read_int_list : int -> int list qui prend
en paramètre un entier n demande à l’utilisateur d’entrer n entiers et renvoie le
resultat sous forme de liste ; on pourra utiliser l’exercice precedent pour afficher
la liste obtenue et verifier que la fonction est correcte*)

let rec read_int_list entiers acc =
  if entiers <= 0 then
    acc
  else
    let entier = read_int () in
    read_int_list (entiers - 1) (entier :: acc)

let print_int_list lst =
  List.iter (fun x -> print_int x; print_char ' ') lst

let () =
  print_string "Entrez la valeur de n : ";
  let n = read_int () in
  let ma_liste = read_int_list n [] in
  print_string "La liste d'entiers que vous avez entrée est : ";
  print_int_list ma_liste;
  print_newline ();;


(*EXERCICE 6*)
(*Adapter la fonction read_int_list afin qu’elle prenne en paramètre une chaîne de caractères à la place de l’entier n ; 
cette chaîne de caractères permettra à l’utilisateur de signaler qu’il a fini de taper des nombres.
Quel est le nouveau type de la fonction read_int_list ?*)

let rec read_int_list entrernombres = 
  print_string "Entrez un entier (ou \"^ entrernombres ^\" pour finir) : "; 
  let input = read_line () in 
  if input = entrernombres then 
    [] 
  else 
    let x = int_of_string input in 
    x :: read_int_list entrernombres
  
  (*le nouveau type de la fonction est val read_int_list : string -> int list on transforme une chaîne de caractères en liste d'entiers*)

(*EXERCICE 7*)
(*Ecrire une fonction  ́ mem : 'a list -> 'a -> bool qui prend en
paramètre une liste et un element et vérifie si cet élément apparaît dans la liste.*)

let rec mem liste element =
match liste with 
| [] -> failwith "Liste vide"
| element1 :: elements -> 
  if element1 = element then 
    true 
  else 
    mem elements element

(*pour vérifier si la fonction fonctionne, on écrit une liste au hasard, on choisit un ou plusieurs nombres et on vérifie si ils sont présents dans la liste, 
si un élément est présent, le compilateur renvoit l'élément est présent dans la liste, sinon il renvoit que l'élément n'est pas présent dans la liste*)

(*exemple 
if mem liste n then 
  Printf.printf "l'élément %d est présent dans la liste"n
else
  Printf.printf "l'élément %d n'est pas présent dans la liste"   

*)
  
 
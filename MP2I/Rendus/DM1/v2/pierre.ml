(*exercice 1*)
let divise n d  =
  if n mod d = 0 then 
    Printf.printf "%d divise %d !" d n 
  else 
    print_endline("perdu")
let () = 
  print_endline("rentre ton nombre qui va être divisé ");
  let n = read_int() in 
  print_endline("rentre ton nombre qui va diviser ");
  let d = read_int() in
  divise n d  
(* exercice 2*)
let rec pgcd a b =
  if b = 0 then a
  else pgcd b (a mod b);;
          
let () = 
  print_endline("rentre ton nombre ");
  let a = read_int() in 
  print_endline("rentre ton nombre ");
  let b = read_int() in        
  Printf.printf "%d est le pgcd"(pgcd a b) 

(*exercice 3*)
let rec min_l = function
  | [] -> failwith ""
  | [x] -> x
  | t :: q -> min t (min_l q)

  let test_min_l () =
  assert (min_l [3] = 3);
  assert (min_l [5; -2] = -2);
  assert (min_l [-2; 5] = -2);
  assert (min_l [-3; 12; 7; 4; -27; 1;3 ; 43] = -27)

let () = test_min_l ()

(*exercice 4*)
let rec print_int_list l =
  match l with
  | [] -> ()
  | t::q ->
  print_int t;
  print_int_list q

let () =
  print_int_list [3;4;5];
  print_newline ()

(*exercice 5*)
let rec read_int_list n =
  if n <= 0 then
    []
  else
    let () = print_string "Entrez un entier : " in
    let value = read_int () in
    value :: read_int_list (n - 1);;
  
let () =
  print_string "Entrez la taille de la liste : ";
  let n = read_int () in
  let l = read_int_list n in
  print_string "Tu as entré les nombres suivants : ";
  print_int_list l;
  print_newline ();;

(*exercice 6*)
let rec read_int_list finito =
  print_string "Entrez un entier (ou \"^ finito ^ pour terminer) : ";
  let input = read_line () in
  if input = finito then 
    []
  else
    let x = int_of_string input in
    x :: read_int_list finito

(*exercice 7*)
let rec mem l e =
  match l with
  | [] -> false
  | t::q -> (t = e) || mem q e



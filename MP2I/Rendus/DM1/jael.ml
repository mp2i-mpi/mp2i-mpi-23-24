(*Exercice 1*)
let divise () =
  print_string "Premier nombre : ";
  let x = read_int () in

  print_string "Deuxième nombre : ";
  let y = read_int () in

  if nombre2 mod nombre1 = 0 then
    print_string "%d divise %d\n"  x y
  else
    print_string "%d ne divise pas %d\n" x y
;

(*Exercice 2*)

let rec pgcd a b =
  if b = 0 then
    a
  else
    pgcd b (a mod b);


let premiers a b =
  let divcom = pgcd a b in
  divcom = 1 ;

print_string "Entrez le premier nombre : ";
let x = read_int () in

print_string"Entrez le deuxième nombre : ";
let y = read_int () in

if premiers x y then
  Printf.printf "%d et %d sont premiers entre eux.\n" x y
else
  Printf.printf "%d et %d ne sont pas premiers entre eux.\n" x y
;;

(*Exercice 3*)

let rec min_l l =
  match l with
  | [] -> failwith "Liste vide"
  | [x] -> x
  | x :: reste -> min x (min_l reste)

;

(*Exercice 4*)
let print_int_liste l =
  let rec aux = function
    | [] -> ()
    | x :: reste ->
    

(*Exercice 5*)
let rec read_int_list n =
  if n <= 0 then
    []
  else
    let x = read_int () in
    x :: read_int_list (n - 1);

print_string "Entrez la taille de la liste : ";
let n = read_int () in
print_string "Entrez %d entiers : " n;
let liste = read_int_list n in
Printf.printf "Liste d'entiers : [%s]\n" liste;

let l = read_int_list 5 let () =
  print_string "Tu as entre les nombres suivants : "; print_int_list l;
  print_newline ()
  
  

(*Exercice 7*)
let rec mem element liste =
  match liste with
  | [] -> false
  | x :: reste ->
    if x = element then
      true
    else
      mem element reste
let divise x y =
  if (x mod y) = 0 then
    Printf.printf " %d divise %d\n" y x
  else
    Printf.printf "%d ne divise pas %d\n" y x

(* let () = divise 10 5
let () = divise 10 3 *)

let rec pgcd a b = 
  if b = 0 then a
  else pgcd b (a mod b)



let rec min_l l = 
  match l with
  | [] -> failwith "Liste vide"
  | [l] -> l
  | t :: q -> min t (min_l q) (* j'utilise une fonction min présente dans ocaml "min (arg)" *)
  
let rec print_int_list l = 
  match l with
  | [] -> failwith "Liste vide"
  | [l] -> print_int l
  | t :: q -> print_int t ; (print_int_list q)
    
let rec read_int_list n = 
  match n with
  | 0 -> []
  | l -> (read_int()) :: read_int_list (l-1)



(* let rec read_int_list2 n = 
  match n with
  | "" -> []
  | l -> read_line() :: (read_int_list2 (n-1)) *)


let rec mem l k = 
  match l with
  | [] -> failwith "Liste vide"
  | [l] -> if k = l then true else false
  | t :: q -> if t = k then true else mem q k



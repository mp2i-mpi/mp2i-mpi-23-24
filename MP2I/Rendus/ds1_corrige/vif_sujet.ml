(* 1 *)

let rec max_l_aux res l =
  match l with
  | [] -> res
  | t::q -> max_l_aux (max res t) q

let max_l l =
  match l with
  | [] -> failwith "Empty"
  | t::q -> max_l_aux t q

(* 2 *)

(*
  La fonction lève une exception du type Failure.
  Pour lever une exception de type Invalid_argument à la place,
  il faut remplacer l'expression `failwith "Empty"` par l'expression
  `raise (Invalid_argument "Empty")` par exemple.
*)

(* 3 *)

let rec split_aux res1 res2 l =
  match l with
  | [] -> res1, res2
  | [x] -> (x::res1), res2
  | t1::t2::q -> split_aux (t1::res1) (t2::res2) q

let split = split_aux [] []

(* 4 *)

let rec merge_aux res l1 l2 =
  match l1, l2 with
  | [], [] -> res
  | t::q, [] | [], t::q -> merge_aux (t::res) q []
  | t1::q1, t2::q2 ->
    if t1 < t2 then
      merge_aux (t1::res) q1 l2
    else
      merge_aux (t2::res) l1 q2

let merge l1 l2 = List.rev (merge_aux [] l1 l2)

(* 5 *)

let rec merge_sort l =
  match l with
  | [] -> []
  | [x] -> [x]
  | _ -> (
    let l1, l2 = split l in
    let sorted1 = merge_sort l1 in
    let sorted2 = merge_sort l2 in
    merge sorted1 sorted2
  )

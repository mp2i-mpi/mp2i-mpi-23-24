type 'a mutablelistnode = {
  mutable value: 'a;
  mutable next: 'a mutablelistnode option;
}

type 'a mutablelist = {
  mutable first: 'a mutablelistnode option;
}

let rec print_mtblst_aux node =
  match node with
  | None -> ()
  | Some node ->
    print_int node.value;
    print_string " ";
    print_mtblst_aux node.next

let print_mtblst l =
  print_string "[ ";
  print_mtblst_aux l.first;
  print_string "]"

(* Fonctions de base *)

(* 1 *)

let empty () = { first = None }

(* 2 *)

let push l x =
  let new_node = { value = x; next = l.first } in
  l.first <- Some new_node

let test_push () =
  let l = empty () in
  push l 1;
  push l 2;
  push l 3;
  let expected = Some { value = 3; next = Some { value = 2; next = Some { value = 1; next = None } } } in
  assert (l.first = expected)

(* 3 *)

(* 3 *)

let pop l =
  match l.first with
  | None -> failwith "Empty list"
  | Some node ->
    l.first <- node.next;
    node.value

let assert_raises f =
  try
    f ();
    assert false
  with
  | Failure _ -> ()

let test_pop () =
  let l = empty () in
  push l 1;
  push l 2;
  push l 3;
  assert (pop l = 3);
  assert (pop l = 2);
  assert (pop l = 1);
  assert_raises (fun () -> pop l)

(* Fonctions de conversion *)

(* 1 *)

let rec of_list_aux acc l =
  match l with
  | [] -> acc
  | t :: q ->
      push acc t;
      of_list_aux acc q

let of_list l = of_list_aux (empty ()) (List.rev l)

let test_of_list () =
  let l = of_list [1; 2; 3] in
  let expected = Some { value = 1; next = Some { value = 2; next = Some { value = 3; next = None } } } in
  assert (l.first = expected)

(* 2 *)

let to_list (l: 'a mutablelist) =
  let rec aux acc node =
    match node with
    | None -> acc
    | Some n ->
        aux (n.value :: acc) n.next
  in
  aux [] l.first

let test_to_list () =
  let l = { first = Some { value = 3; next = Some { value = 2; next = Some { value = 1; next = None } } } } in
  assert (to_list l = [1; 2; 3])

(* Fonctions d'accès et de modification *)

(* 1 *)

let add (l: 'a mutablelist) (i: int) (x: 'a) =
  let rec loop node i =
    match node with
    | None -> failwith "Index out of bounds"
    | Some n ->
        if i = 0 then
          let new_node = { value = x; next = n.next } in
          n.next <- Some new_node
        else
          loop n.next (i - 1)
  in
  if i < 0 then
    failwith "Index out of bounds"
  else if i = 0 then
    push l x
  else
    loop l.first (i - 1)

let test_add () =
  let l = of_list [1; 2; 3] in
  add l 0 0;
  add l 2 4;
  add l 5 5;
  let expected = of_list [0; 1; 4; 2; 3; 5] in
  assert (l = expected)

(* 2 *)

let remove (l: 'a mutablelist) (i: int) =
  let rec loop node i =
    match node with
    | None -> failwith "Index out of bounds"
    | Some n ->
        if i = 0 then
          let next = Option.get n.next in
          n.next <- next.next; next.value
        else
          loop n.next (i - 1)
  in
  if i < 0 then
    failwith "Index out of bounds"
  else if i = 0 then
    pop l
  else
    loop l.first (i - 1)

let test_remove () =
  let l = of_list [1; 2; 3] in
  assert (remove l 0 = 1);
  assert (remove l 1 = 3);
  assert (remove l 0 = 2);
  assert_raises (fun () -> remove l 0)

(* 3 *)

let replace (l: 'a mutablelist) (i: int) (x: 'a) =
  let rec loop node i =
    match node with
    | None -> failwith "Index out of bounds"
    | Some n ->
        if i = 0 then
          n.value <- x
        else
          loop n.next (i - 1)
  in
  if i < 0 then
    failwith "Index out of bounds"
  else
    loop l.first i

let test_replace () =
  let l = of_list [1; 2; 3] in
  replace l 0 0;
  replace l 1 4;
  replace l 2 5;
  let expected = of_list [0; 4; 5] in
  assert (l = expected)

(* 4 *)

let get (l: 'a mutablelist) (i: int) =
  let rec loop node i =
    match node with
    | None -> failwith "Index out of bounds"
    | Some n ->
        if i = 0 then
          n.value
        else
          loop n.next (i - 1)
  in
  if i < 0 then
    failwith "Index out of bounds"
  else
    loop l.first i

let test_get () =
  let l = of_list [1; 2; 3] in
  assert (get l 0 = 1);
  assert (get l 1 = 2);
  assert (get l 2 = 3);
  assert_raises (fun () -> get l 3)

(* Fonctions d'application et de transformation *)

(* 1 *)

let rec iter_aux (f: 'a -> unit) (n: 'a mutablelistnode) =
  f n.value;
  match n.next with
  | None -> ()
  | Some n -> iter_aux f n

let iter (f: 'a -> unit) (l: 'a mutablelist) =
  match l.first with
  | None -> ()
  | Some n -> iter_aux f n

let test_iter () =
  let l = of_list [1; 2; 3] in
  let acc = ref 0 in
  iter (fun x -> acc := !acc + x) l;
  assert (!acc = 6)

(* 2 *)

let rec map_aux (f: 'a -> 'b) (n: 'a mutablelistnode) =
  let new_node = { value = f n.value; next = None } in
  match n.next with
  | None -> new_node
  | Some n ->
      new_node.next <- Some (map_aux f n);
      new_node

let map (f: 'a -> 'b) (l: 'a mutablelist) =
  match l.first with
  | None -> empty ()
  | Some n -> { first = Some (map_aux f n) }

let test_map () =
  let l = of_list [1; 2; 3] in
  let l = map (fun x -> x * 2) l in
  let expected = of_list [2; 4; 6] in
  assert (l = expected)

(* Fonctions de réduction *)

(* 1 *)

let rec fold_left_aux (f: 'a -> 'b -> 'a) (acc: 'a) (n: 'b mutablelistnode) =
  let acc = f acc n.value in
  match n.next with
  | None -> acc
  | Some n -> fold_left_aux f acc n

let fold_left (f: 'a -> 'b -> 'a) (acc: 'a) (l: 'b mutablelist) =
  match l.first with
  | None -> acc
  | Some n -> fold_left_aux f acc n

let test_fold_left () =
  let l = of_list [1; 2; 3] in
  let acc = fold_left (+) 0 l in
  assert (acc = 6)

(* Fonctions de vérification *)

(* 1 *)

let rec forall_aux (f: 'a -> bool) (n: 'a mutablelistnode) =
  f n.value && match n.next with
  | None -> true
  | Some n -> forall_aux f n

let forall (f: 'a -> bool) (l: 'a mutablelist) =
  match l.first with
  | None -> true
  | Some n -> forall_aux f n

let test_forall () =
  let l = of_list [1; 2; 3] in
  assert (forall (fun x -> x > 0) l);
  assert (not (forall (fun x -> x > 1) l))

(* 2 *)

let rec exists_aux (f: 'a -> bool) (n: 'a mutablelistnode) =
  f n.value || match n.next with
  | None -> false
  | Some n -> exists_aux f n

let exists (f: 'a -> bool) (l: 'a mutablelist) =
  match l.first with
  | None -> false
  | Some n -> exists_aux f n

let test_exists () =
  let l = of_list [1; 2; 3] in
  assert (exists (fun x -> x > 0) l);
  assert (exists (fun x -> x > 1) l);
  assert (not (exists (fun x -> x > 3) l))

(* Tests *)

let () =
  test_push ();
  test_pop ();
  test_of_list ();
  test_to_list ();
  test_add();
  test_remove();
  test_replace();
  test_get();
  test_iter();
  test_map();
  test_fold_left();
  test_forall();
  test_exists();
  print_endline "Tests passés !"


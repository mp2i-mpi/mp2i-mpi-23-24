type sign = Plus | Minus
type bigint = sign * (int list)

(* Partie 1 *)

(* 1 *)

(* D'abord une fonction pour additionner des nombres positifs avec retenue *)
let rec add_pos_bigint_aux res vx vy c =
  match vx, vy with
  | [], [] -> if c = 0 then res else c::res
  | [], t::q | t::q, [] ->
    let r = t + c in
    let c, s = r / 10, r mod 10 in
    add_pos_bigint_aux (s::res) q [] c
  | hx::tx, hy::ty ->
    let r = hx + hy in
    let c, s = r / 10, r mod 10 in
    add_pos_bigint_aux (s::res) tx ty c

let add_pos_bigint vx vy =
  add_pos_bigint_aux [] vx vy 0

(* Ensuite une fonction pour soustraire des nombres positifs avec retenue ; on suppose le premier plus petit *)

let rec sub_pos_bigint_aux res vx vy c =
  match vx, vy with
  | [], [] -> if c = 0 then res else failwith "vx < vy"
  | [], t::q | t::q, [] ->
    let r = t - c in
    let c, s = if r < 0 then 1, r + 10 else 0, r in
    sub_pos_bigint_aux (s::res) q [] c
  | hx::tx, hy::ty ->
    let r = hx - hy in
    let c, s = if r < 0 then 1, r + 10 else 0, r in
    sub_pos_bigint_aux (s::res) tx ty c

let sub_pos_bigint vx vy = sub_pos_bigint_aux [] vx vy 0

(* Enfin une fonction pour additionner dans le cas général *)

let add_bigint (s1, vx) (s2, vy) =
  match s1, s2 with
  | Plus, Plus -> Plus, add_pos_bigint vx vy
  | Minus, Minus -> Minus, add_pos_bigint vx vy
  | Plus, Minus -> if vx >= vy then Plus, sub_pos_bigint vx vy else Minus, sub_pos_bigint vy vx
  | Minus, Plus -> if vx >= vy then Minus, sub_pos_bigint vx vy else Plus, sub_pos_bigint vy vx

(* 2 *)

let sub_bigint (s1, vx) (s2, vy) = add_bigint (s1, vx) (if s2 = Plus then Minus, vy else Plus, vy)

(* 3 *)

(* D'abord une fonction qui multiplie un bigint par un seul chiffre *)

let rec mul_bigint_one_factor vx factor c =
  match vx with
  | [] -> if c = 0 then [] else [c]
  | t::q ->
    let r = t * factor + c in
    let c, s = r / 10, r mod 10 in
    s::mul_bigint_one_factor q factor c

(* Ensuite une fonction qui multiplie deux bigints positifs *)

let rec mul_pos_bigint_aux res vx vy =
  match vy with
  | [] -> res
  | t::q ->
    let r = add_pos_bigint (mul_bigint_one_factor vx t 0) res in
    mul_pos_bigint_aux (0::r) vx q

let mul_pos_bigint vx vy = mul_pos_bigint_aux [] vx vy

(* Enfin une fonction qui multiplie deux bigints en calculant le signe *)

let mul_bigint (sx, vx) (sy, vy) =
  match sx, sy with
  | Plus, Plus | Minus, Minus -> Plus, mul_pos_bigint vx vy
  | Plus, Minus | Minus, Plus -> Minus, mul_pos_bigint vx vy

(* Partie 2 *)

let rec string_of_pos_bigint_aux res vx =
  match vx with
  | [] -> res
  | t::q -> string_of_pos_bigint_aux (string_of_int t ^ res) q
  
let string_of_pos_bigint vx =
  match vx with
  | [] -> "0"
  | _ -> string_of_pos_bigint_aux "" vx

let string_of_bigint (s, vx) =
  match s with
  | Plus -> string_of_pos_bigint vx
  | Minus -> "-" ^ string_of_pos_bigint vx

  
let bigint_of_string_aux s =
  let res = ref [] in
  for i = 0 to String.length s - 1 do
    res := (int_of_char s.[i]) :: !res
  done;
  !res

let bigint_of_string s =
  if s.[0] = '-' then
    Minus, bigint_of_string_aux (String.sub s 1 (String.length s - 1))
  else Plus, bigint_of_string_aux s

(* 1 *)

let () = print_endline "Hello world !"

(* 2 *)

let () =
  print_string "Ton nom ? ";
  let nom = read_line () in
  Printf.printf "Hello %s\n" nom

(* 3 *)

let () =
  print_string "Ton nom ? ";
  let nom = read_line () in
  if nom = "Alice" || nom = "Bob" then
    Printf.printf "Hello %s !\n" nom
  else
    print_endline "Ouste inconnu !"

(* 4 *)

let sum_n n = n * (n+1) / 2

(* 5 *)

let () =
  print_string "Choisir un nombre n : ";
  let n = read_int () in
  Printf.printf "S_1^n = %d\n" n

(* 6 *)

let rec sum_n_3_and_5_aux res n =
  if n = 0 then
    res
  else if n mod 3 = 0 || n mod 5 = 0 then
    sum_n_3_and_5_aux (res+n) (n-1)
  else
    sum_n_3_and_5_aux res (n-1)

let sum_n_3_and_5 = sum_n_3_and_5_aux 0

(* 7 *)

let rec guess n =
  print_string "Choisis un nombre : ";
  let choice = read_int () in
  if choice = n then
    print_endline "Bravo, tu as trouvé !"
  else if choice < n then (
    print_endline "Trop petit !";
    guess n
  ) else (
    print_endline "Trop grand !";
    guess n
  )

let () =
  Random.self_init ();
  let n = (Random.int 100) + 1 in
  guess n


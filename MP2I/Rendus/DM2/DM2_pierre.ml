let rec pgcd a b =
  if b = 0 then a
  else 
    pgcd b (a mod b)
(*pgcd est récursive terminal car la dernière instruction est récursive*)

type frac = {
  num : int ;
  denum : int
}

let simp f =
  let facteur = pgcd f.num f.denum in
  let new_num = f.num / facteur in
  let new_denum = f.denum / facteur in 
  if new_denum = 0 then 
    failwith "sayez b diff de 0" 
  else
    if new_denum < 0 then
      { num = (-new_num) ; denum = (-new_denum) }
    else
      { num = new_num ; denum = new_denum }

let frac a b = 
  simp {num = a ; 
  denum = b } 

let add_frac f1 f2 =
  let new_num = (f1.num * f2.denum + f2.num * f1.denum) in 
  let new_denum = (f1.denum * f2.denum) in 
  simp {num = new_num ; denum = new_denum}

let neg_frac f = 
  simp {num = (-f.num); denum = f.denum }

let sub_frac f1 f2 = 
  let neg_f2 = neg_frac f2 in  
  add_frac f1 neg_f2 

let mul_frac f1 f2 = 
  let num_mul_f = f1.num * f2.num in
  let denum_mul_f = f1.denum * f2.denum in
  frac num_mul_f denum_mul_f 

let inv_frac f =
  let a = f.num in
  let b = f.denum in
  frac b a

let div_frac f1 f2 =
  let inv_f2 = inv_frac f2 in
  mul_frac f1 inv_f2

let float_of_frac f = 
  float_of_int(f.num) /. float_of_int(f.denum)

let string_of_frac f =
  string_of_float(float_of_frac f)

let() =
  let f1 = { num = 1 ; denum = 3 } in 
  let f2 = { num = 4 ; denum = 5 } in 
  let res = string_of_frac f1 in 
  Printf.printf "%s\n" res 
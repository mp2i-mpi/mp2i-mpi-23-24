                      (* DM 2 Alessandro Galiano *)

let rec pgcd a b = 
  if b = 0 then a
  else pgcd b (a mod b)

type frac = {num : int; denum : int}

let simp f = if f.denum > 0 then { num = f.num/(pgcd f.num f.denum) ;denum = f.denum/(pgcd f.num f.denum)}
             else { num = -f.num/(pgcd f.num f.denum); denum = f.denum/(pgcd f.num f.denum)}

let frac f = simp {num = f.num ;denum = f.denum}

let add_frac f1 f2 = simp {num = (f1.num*f2.denum + f2.num*f1.denum); denum = (f1.denum*f2.denum)}

let neg_frac f = simp {num = -f.num ; denum = f.denum}

let sub_frac f1 f2 = simp {num = (f1.num*f2.denum - f2.num*f1.denum); denum = (f1.denum*f2.denum) }

let mul_frac f1 f2 = simp {num = (f1.num*f2.num) ;denum = (f1.denum*f2.denum)}

let inv_frac f = simp {num = f.denum; denum = f.num}

let div_frac f1 f2 = simp {num = (f1.num*f2.denum); denum = (f1.denum*f2.num)}

let float_of_frac f = simp {num = f.num ; denum = f.denum}

let string_of_frac f = 
  let f1 = simp { num = f.num ; denum = f.denum} in
  let t = string_of_int(f1.num ; f1.denum) in
  Printf.printf "%d/%d" f1.num f1.denum






let () = string_of_frac({num = 5 ; denum = 10})
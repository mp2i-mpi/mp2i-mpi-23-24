(*          DM2 : du 12/11 au 18/11          *)

(*      Question 1      *)
let rec gcd a b =
  if b = 0 then
    a
  else
    gcd b (a mod b)

(* Cette fonction est bien récursive terminale, la dernière instruction étant bien la dernière instruction. *)

(*      Question 2      *)
type frac = { num : int ; denom : int }

(*      Question 3      *)
let simp ( f : frac ) =
  let fact_com = gcd f.num f.denom in
  let num1 = f.num / fact_com in
  let denom1 = f.denom / fact_com in
  if denom1 = 0 then
    failwith "Le dénominateur doit être non nul."
  else
    if denom1 < 0 then
      { num = (-num1) ; denom = (-denom1) }
    else
      { num = num1 ; denom = denom1 }

(*      Question 4      *)
let frac ( a : int) ( b : int ) =
  if b = 0 then
    failwith "Le dénominateur doit être non nul."
  else
    let frac1 = { num = a ; denom = b} in
    simp frac1

(*      Question 4      *)
let add_frac ( f1 : frac ) ( f2 : frac ) =
  simp { num = ( (f1.num * f2.denom) + (f2.num * f1.denom) ) ; denom = ( f1.denom * f2.denom ) }

let neg_frac ( f1 : frac ) =
  simp { num = (-f1.num) ; denom = f1.denom}

let sub_frac ( f1 : frac ) ( f2 : frac ) =
  simp { num = ( (f1.num * f2.denom) - (f2.num * f1.denom) ) ; denom = ( f1.denom * f2.denom ) }

let mul_frac ( f1 : frac ) ( f2 : frac ) =
  simp { num = (f1.num * f2.num) ; denom = (f1.denom * f2.denom) }

let inv_frac ( f1 : frac ) =
  simp { num = (simp f1).denom ; denom = (simp f1).num }

let div_frac ( f1 : frac ) ( f2 : frac ) =
  mul_frac f1 (inv_frac f2)

let float_of_frac ( f1 : frac ) =
  float_of_int( (simp f1).num ) /. float_of_int( (simp f1).denom )

let string_of_frac ( f1 : frac ) =
  string_of_int( (simp f1).num ) ^ "/" ^ string_of_int( (simp f1).denom )
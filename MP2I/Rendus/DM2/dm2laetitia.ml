(*DM2*)
(*Question 1*)

let rec gcd a b =
  if b = 0 then a
  else 
    gcd b (a mod b)
(*il s'agit bien d'une fonction recurrsive terminale car la dernière opération effectuée est l'appel recursif lui-même*)

(*Question 2*)
type frac = {
  num : int ;
  denom : int
}

(*Question 3*)
let simp f =
  let facteur = gcd f.num f.denom in
  let new_num = f.num / facteur in
  let new_denom = f.denom / facteur in
  if new_denom = 0 then 
    failwith "Le dénominateur doit être différent de 0"
  else
    if new_denom < 0 then
      { num = -new_num; denom = -new_denom }
    else
      { num = new_num; denom = new_denom }

(* Question 4 *)

let frac a b = 
  simp {num = a; denom = b}

let add_frac f1 f2 =
  let new_num = (f1.num * f2.denom + f2.num * f1.denom) in 
  let new_denom = (f1.denom * f2.denom) in 
  simp {num = new_num; denom = new_denom}
 
let neg_frac f =
  simp {num = -f.num; denom = f.denom}

let sub_frac f1 f2 =
  let neg_f2 = neg_frac f2 in  
  add_frac f1 neg_f2

let mul_frac f1 f2 =
  let num_mul_f = f1.num * f2.num in
  let denom_mul_f = f1.denom * f2.denom in
  frac num_mul_f denom_mul_f

let inv_frac f =
  let a = f.num in
  let b = f.denom in
  frac b a

let div_frac f1 f2 =
  let inv_f2 = inv_frac f2 in
  mul_frac f1 inv_f2

let float_of_frac f = 
  float_of_int(f.num) /. float_of_int(f.denom)

let string_of_frac f =
  string_of_float(float_of_frac f)

let() =
  let f1 = { num = 1 ; denom = 3 } in 
  let f2 = { num = 4 ; denom = 5 } in 
  let res = string_of_frac f1 in 
  Printf.printf "%s\n" res 
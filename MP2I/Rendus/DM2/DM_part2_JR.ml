
(*Exercice 1 *)
let rec pgcd a b =
  if b = 0 then
    a 
  else 
    pgcd b (a mod b)


(*Exercice 2 *)
type frac = { num : int; denom : int }

(*Exercice 3 *)
let simp f =
  if f.denom = 0 then
    failwith "On ne divise pas par 0."
  else if f.denom < 0 then
    { num = -f.num; denom = -f.denom }
  else
    let gcd = pgcd f.num f.denom in
    { num = f.num / gcd; denom = f.denom / gcd }


(*Exercice 4 *)
let frac a b =
  simp { num = a; denom = b }

let add_frac f1 f2 =
  let num = f1.num * f2.denom + f2.num * f1.denom in
  let denom = f1.denom * f2.denom in
  simp { num; denom }

let neg_frac f =
  simp { num = -f.num; denom = f.denom }

let sub_frac f1 f2 =
  add_frac f1 (neg_frac f2)

let mul_frac f1 f2 =
  let num = f1.num * f2.num in
  let denom = f1.denom * f2.denom in
  simp { num; denom }

let inv_frac f =
  if f.num = 0 then
    failwith "On ne divise toujours pas par 0"
  else
    simp { num = f.denom; denom = f.num }

let div_frac f1 f2 =
  mul_frac f1 (inv_frac f2)

let float_of_frac f =
  float_of_int f.num /. float_of_int f.denom

let string_of_frac f =
  Printf.sprintf "%d/%d" f.num f.denom






    
(*exo 1 *)


let rec pgcd a b = 
  if a > b then 
    (
      let c = a mod b in
  if c = 0 then (b)
    else 
      pgcd b c 
          )
  else let c = b mod a in
  if c = 0 then (a)
else 
  pgcd a c 



(*exo 2 *)
type frac = {
  num : int;
  denom : int;
}
(*exo 3 *)
(*mon pgcd ne marche pas pour les nombre négatifs donc on va se débrouiller en utilisant la valeur absolue *)
let abs a =
  if a < 0 then
    -a 
  else 
    a 

let simp f =
  let c = pgcd (abs(f.num)) (abs(f.denom)) in 
  if (f.num)*(f.denom) <0
    then 
      (
       (*le cas la fraction est négatif *)
      let fraction_simplife = {num = -abs(f.num)/c ; denom = abs(f.denom)/c }  in  
      fraction_simplife
      )
  else 
    let fraction_simplife = {num = abs(f.num)/c ; denom = abs(f.denom)/c} in 
    fraction_simplife
  
(*
let c = simp {num = 44 ; denom = -4} 
let () = print_int(c.denom)
*)

(*exo 4 *)
let frac n d  =
  let nv_frac = {num = n ; denom = d } in 
  let nv2_frac = simp nv_frac in
  nv2_frac

let add_frac a b =
  let nv_frac = { num = (a.num)*(b.denom) + (b.num)*(a.denom) ; denom = (a.denom)*(b.denom)} in 
  let nv2_frac = simp nv_frac in 
  nv2_frac

let neg_frac f =
  let nv_frac = {num = -(f.num) ; denom = (f.denom)} in 
  let nv2_frac = simp nv_frac in  
  nv2_frac 


let sub_frac a b =
  let c = neg_frac b in 
  add_frac a c 



let mul_frac a b = 
  let nv_frac = {num = (a.num)*(b.num) ; denom = (a.denom)*(b.denom)} in 
  let nv2_frac = simp nv_frac in 
  nv2_frac 

let inv_frac f = 
    let nv_frac = {num = f.denom ; denom = f.num} in 
    let nv2_frac = simp nv_frac in 
    nv2_frac  
 

let div_frac a b = 
  let c = inv_frac b in 
  mul_frac a c 


let frac_to_float f = 
  let a = f.num in 
  let b = f.denom in 
  let nv_float = float_of_int(a)/.( float_of_int(b) )in 
  nv_float


(*
let c = frac_to_float {num = 6 ; denom = 10}  
let () = Printf.printf "c: %f\n" c 
*)

let frac_to_string f = 
  let floooat = frac_to_float f in 
  let striiing = string_of_float(floooat) in 
  striiing
(*
let () = print_endline(frac_to_string {num = 6; denom = 10 })
*)
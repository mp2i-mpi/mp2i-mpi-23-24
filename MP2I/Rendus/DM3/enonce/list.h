#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

#include <stdbool.h>

struct Node;
typedef struct Node node;
struct List;
typedef struct List list;

// Déja fourni en exemple
void print_list(const list* l);

// Utile pour les tests
// (pour comparer le contenu d'une liste à celui d'un tableau)
bool list_equals(const list* l, const int* values, int n_values);

// À implémenter

// Crée une liste vide
list* list_create();
// Renvoie true si la liste est vide, false sinon
bool list_is_empty(const list* l);

// Ajoute un élément en tête de la liste
void list_push_front(list* l, int value);
// Ajoute un élément en queue de la liste
void list_push_back(list* l, int value);
// Retire et renvoie le premier élément de la liste
int list_pop_front(list* l);
// Retire et renvoie le dernier élément de la liste
int list_pop_back(list* l);

// Supprime tous les éléments de la liste
// (la liste est vide après cet appel)
// (ne pas oublier de libérer la mémoire)
void list_clear(list* l);
// Libère la mémoire associée à la liste
// (la liste ne doit plus être utilisée après cet appel)
// (ne pas oublier de libérer les éléments d'abord)
void list_destroy(list* l);

// Renvoie le nombre d'éléments dans la liste
int list_size(const list* l);
// Renvoie l'élément à l'index donné
int list_get(const list* l, int index);
// Modifie l'élément à l'index donné
void list_insert(list* l, int value, int index);
// Supprime l'élément à l'index donné
void list_remove(list* l, int index);

// Crée une liste à partir d'un tableau
void list_from_array(list* l, const int* values, int n_values);

#endif // LIST_H_INCLUDED

#include "list.h"

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

typedef struct Node {
    int value;
    struct Node *next;
    struct Node *prev;
} node;

typedef struct List {
    node *first;
    node *last;
} list;

void print_list(const list* l) {
    printf("[ ");
    // Une utilisation astucieuse de la boucle for
    // afin d'itérer sur ce genre de structures :
    for (node* n = l->first; n != NULL; n = n->next) {
        printf("%d -> ", n->value);
    }
    // On aurait aussi pu faire :
    // node* n = l->first;
    // while (n != NULL) {
    //     printf("%d -> ", n->value);
    //     n = n->next;
    // }
    printf("NULL ]\n");
}

bool list_equals(const list* l, const int* values, int size) {
    node *n = l->first;
    for (int i = 0; i < size; ++i) {
        if (n == NULL || n->value != values[i]) {
            return false;
        }
        n = n->next;
    }
    return true;
}

list* list_create() {
    assert (false && "TODO");
}

bool list_is_empty(const list* l) {
    assert (false && "TODO");
}

void list_push_front(list* l, int value) {
    assert (false && "TODO");
}

void list_push_back(list* l, int value) {
    assert (false && "TODO");
}

int list_pop_front(list* l) {
    assert (false && "TODO");
}

int list_pop_back(list* l) {
    assert (false && "TODO");
}

int list_size(const list* l) {
    assert (false && "TODO");
}

int list_get(const list* l, int index) {
    assert (false && "TODO");
}

void list_clear(list* l) {
    assert (false && "TODO");
}

void list_destroy(list* l) {
    assert (false && "TODO");
}

void list_insert(list* l, int value, int index) {
    assert (false && "TODO");
}

void list_remove(list* l, int index) {
    assert (false && "TODO");
}


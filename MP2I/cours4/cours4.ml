
let annee = 2023
let annee_et_demi = float_of_int annee +. 1.5

let () = Printf.printf "Année prochaine : %f\n" annee_et_demi

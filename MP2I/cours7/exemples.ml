(* let rec somme_naif n =
  if n = 0 then
    0
  else
    n + (somme_naif (n-1))

let rec somme_aux n res =
  if n = 0 then
    res
  else
    somme_aux (n-1) (n+res)

let somme_mieux n = somme_aux n 0 *)

(* let () = Printf.printf "%d\n" (somme_naif 1_000_000) *)
(* let () = Printf.printf "%d\n" (somme_mieux 1_000_000) *)

(* let rec fact_naif n =
  if n = 0 then
    1
  else
    n * (fact_naif (n-1))

let rec fact_aux n res =
  if n = 0 then
    res
  else
    fact_aux (n-1) (n*res)

let fact_mieux n = fact_aux n 1 *)

(* let () = Printf.printf "%d\n" (fact_naif 50) *)
(* let () = Printf.printf "%d\n" (fact_mieux 100000000) *)

type 'a maliste =
  | Vide
  | NonVide of 'a * 'a maliste

let rec somme_liste l =
  match l with
  | Vide -> 0
  | NonVide(t, q) -> t + (somme_liste q)

let rec min_liste l =
  match l with
  | Vide -> failwith "liste vide"
  | NonVide (t, q) ->
      let m = min_liste q in
      if t < m then t else m

let () = Printf.printf "%d\n" (somme_liste (NonVide(5, NonVide(3, NonVide(-2, Vide)))))

let ma_l = NonVide(5.3, NonVide(3.2, NonVide(-2.0, Vide)))
let l = [5.3; 3.2; -2.0]


type ('a,'b) mapaire = MaPaire of 'a * 'b

let p = MaPaire(NonVide(3, NonVide(5, Vide)), 5.2)




















(* let prochain_nombre stop =
  let l = read_line () in
  if l = stop then
    None
  else
    Some (int_of_string l)
 *)

    
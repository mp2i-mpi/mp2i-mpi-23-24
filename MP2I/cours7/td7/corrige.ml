(* 1. rev l renvoie la liste l renversée *)


let rec rev_aux source dest = match source with
  | [] -> dest
  | t::q -> rev_aux q (t::dest)

let rev l = rev_aux l []

let test_rev () =
  assert (rev [1;2;3] = [3;2;1]);
  assert (rev [3;1;6;7] = [7;6;1;3]);
  assert (rev [] = []);
  assert (rev [1] = [1])


(* 2. mem l x renvoie si l'élément x est présent dans la liste l *)

let rec mem l x = match l with
  | [] -> false
  | t::q -> t = x || mem q x

let test_mem() =
  assert (mem [1;2;3] 1);
  assert (mem [1;2;3] 2);
  assert (mem [1;2;3] 3);
  assert (not (mem [1;2;3] 4));
  assert (not (mem [] 1))

(* 3. replace l x y renvoie la liste l en remplaçant tous les x par des y *)

let rec replace l x y = match l with
  | [] -> []
  | t::q ->
      let subres = replace q x y in
      if t = x then
        y::subres
      else
        t::(replace q x y)

let test_replace() =
  assert (replace [1;2;3] 1 4 = [4;2;3]);
  assert (replace [1;2;3] 2 4 = [1;4;3]);
  assert (replace [1;2;3] 3 4 = [1;2;4]);
  assert (replace [1;2;3] 4 5 = [1;2;3]);
  assert (replace [] 1 2 = []);
  assert (replace [1] 1 2 = [2]);
  assert (replace [1;1;1] 1 2 = [2;2;2]);
  assert (replace [1;3;2;2;4;2;3;2] 2 1 = [1;3;1;1;4;1;3;1])

(* 4. replace_pos l i x renvoie la liste l en remplaçant l'élément à l'indice i par x

  0 <= i < longueur de l
*)

let rec replace_pos l i x = match l with
  | [] -> failwith "i > List.length l"
  | t::q ->
      if i = 0 then
        x::q
      else
        t::(replace_pos q (i-1) x)

let test_replace_pos() =
  assert (replace_pos [1;2;3] 0 4 = [4;2;3]);
  assert (replace_pos [1;2;3] 1 4 = [1;4;3]);
  assert (replace_pos [1;2;3] 2 4 = [1;2;4])

(* 5. remove_pos l i renvoie la liste l en supprimant l'élément à l'indice i

  0 <= i < longueur de l
*)

let rec remove_pos l i = match l with
  | [] -> failwith "i > List.length l"
  | t::q ->
      if i = 0 then
        q
      else
        t::(remove_pos q (i-1))

let test_remove_pos() =
  assert (remove_pos [1;2;3] 0 = [2;3]);
  assert (remove_pos [1;2;3] 1 = [1;3]);
  assert (remove_pos [1;2;3] 2 = [1;2])

(* 6. insert_pos l i x renvoie la liste l en insérant x à l'indice i
   
  0 <= i <= longueur de l
  Si i == longueur de l, insert_pos insère à la fin
*)

let rec insert_pos l i x =
  if i = 0 then
    x::l
  else match l with
    | [] -> failwith "i > List.length l"
    | t::q -> t::(insert_pos q (i-1) x)

let test_insert_pos() =
  assert (insert_pos [1;2;3] 0 4 = [4;1;2;3]);
  assert (insert_pos [1;2;3] 1 4 = [1;4;2;3]);
  assert (insert_pos [1;2;3] 2 4 = [1;2;4;3]);
  assert (insert_pos [1;2;3] 3 4 = [1;2;3;4]);
  assert (insert_pos [] 0 1 = [1])

(* 7. insert_bigger l x renvoie la liste l en insérant x juste avant le premier élément plus grand rencontré *)
  
let rec insert_bigger l x =
  match l with
  | [] -> [x]
  | t::q ->
      if x < t then
        x::l
      else
        t::(insert_bigger q x)

let test_insert_bigger() =
  assert (insert_bigger [1;2;3] 0 = [0;1;2;3]);
  assert (insert_bigger [1;2;3] 1 = [1;1;2;3]);
  assert (insert_bigger [1;2;3] 2 = [1;2;2;3]);
  assert (insert_bigger [1;2;3] 3 = [1;2;3;3]);
  assert (insert_bigger [1;2;3] 4 = [1;2;3;4]);
  assert (insert_bigger [] 1 = [1])

(* 9. insertion_sort l renvoie la liste l triée par insertion *)

let rec insertion_sort l = match l with
  | [] -> []
  | t::q -> insert_bigger (insertion_sort q) t

let test_insertion_sort() =
  assert (insertion_sort [1;2;3] = [1;2;3]);
  assert (insertion_sort [3;2;1] = [1;2;3]);
  assert (insertion_sort [1;3;2] = [1;2;3]);
  assert (insertion_sort [3;1;2] = [1;2;3]);
  assert (insertion_sort [2;1;3] = [1;2;3]);
  assert (insertion_sort [2;3;1] = [1;2;3])

(* 10. merge l1 l2 renvoie la liste l1 et l2 fusionnées et triées *)

(* Pour tout tester *)
let check_test test =
  let test_name, test_func = test in
  test_func();
  Printf.printf "%s OK\n" test_name

let all_tests() =
  List.iter (fun test -> check_test test) [
    "test_rev", test_rev;
    "test_mem", test_mem;
    "test_replace", test_replace;
    "test_replace_pos", test_replace_pos;
    "test_remove_pos", test_remove_pos;
    "test_insert_pos", test_insert_pos;
    "test_insert_bigger", test_insert_bigger;
    "test_insertion_sort", test_insertion_sort
  ]

let () = all_tests()

let rec max_l l = List.fold_left max (List.hd l) l
  (* match l with
  | [] -> failwith "empty list"
  | [x] -> x
  | x :: xs -> max x (max_l xs) *)

let test_max() =
  assert (max_l [1; 2; 3] = 3);
  assert (max_l [1; 3; 2] = 3);
  assert (max_l [3; 2; 1] = 3);
  assert (max_l [3; 3; 3] = 3);
  assert (max_l [1] = 1)

let () = test_max(); print_endline "max ok"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

void exo1() {
    char input[256];
    printf("Ton nom : ");
    scanf("%s", input);
    if (strcmp(input, "Alice") == 0 || strcmp(input, "Bob") == 0) {
        printf("Hello %s !\n", input);
    } else {
        printf("Ouste, étranger !\n");
    }
}

void exo2() {
    int n;
    printf("n : ");
    scanf("%d", &n);
    printf("S_k^n k = %d\n", n*(n+1)/2);
}

void exo3() {
    srand(time(NULL));
    int res = (rand() % 100)+1;
    int guess = 0;

    while (guess != res) {
        printf("Choisis un nombre : ");
        scanf("%d", &guess);
        if (guess < 1 || guess > 100) {
            printf("Choisis entre 1 et 100 !\n");
        } else if (guess < res) {
            printf("Trop petit !\n");
        } else if (guess == res) {
            printf("Bravo, tu as trouvé !\n");
        } else {
            printf("Trop grand !\n");
        }
    }
}

int print_tableau(int* tableau, int size) {
    for (int i = 0; i < size; ++i) {
        printf("%d ", tableau[i]);
    }
    printf("\n");
}

int max(int* tableau, int size) {
    int res = 0;
    for (int i = 0; i < size; ++i) {
        res += tableau[i];
    }
    return res;
}

void rev(int* tableau, int size) {
    for (int i = 0; i < size-(i+1); ++i) {
        int j = size - (i+1);
        int tmp = tableau[i];
        tableau[i] = tableau[j];
        tableau[j] = tmp;
    }
}

void exo4() {
    int n = 8;
    int tableau[8] = { 3, -2, 15, 1, -3, 4, 7, 8 };
    print_tableau(tableau, n);
    printf("Max : %d\n", max(tableau, n));
    rev(tableau, n);
    print_tableau(tableau, n);
}

int main() {
    // exo1();
    // exo2();
    // exo3();
    exo4();

    return 0;
}

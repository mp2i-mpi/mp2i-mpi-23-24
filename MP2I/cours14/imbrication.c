#include <stdio.h>

struct Adresse {
    int numero;
    char* rue;
    int code_postal;
    char* ville;
};

typedef struct Adresse adresse;

typedef struct Personne {
    char* prenom;
    int age;
    adresse adresse;
} personne;


void print_personne(personne *p) {
    printf("%s (%d ans)\n", p->prenom, p->age);
    printf("%d %s (%d %s)", p->adresse.numero, p->adresse.rue, p->adresse.code_postal, p->adresse.ville);
}

int main() {
    personne john = {
        .prenom = "John",
        .age = 27,
        .adresse = {
            .numero = 12,
            .rue = "Rue Clodoalde",
            .code_postal = 92210,
            .ville = "Saint-Cloud"
        }
    };

    print_Personne(&john);

    return 0;
}


#include <stdio.h>

int compter_lignes(const char* filename) {
    FILE* f = fopen(filename, "r");
    int nb_lignes = 0;
    while (!feof(f)) {
        if (fgetc(f) == '\n') {
            ++nb_lignes;
        }
    }
    fclose(f);
    return nb_lignes;
}

int main (int argc, char* argv[]) {
    int res = compter_lignes(argv[1]);
    printf("%d lignes\n", res);
    return 0;
}

#include <stdio.h>

struct S {
    int a;
    int b;
};

void print_S(const struct S *s) {
    printf("a = %d, b = %d\n", s->a, s->b);
}

int main() {
    struct S s1 = { 2, 3 };
    print_S(&s1);

    struct S s2 = {
        .a = 2,
        .b = 3
    };

    return 0;
}


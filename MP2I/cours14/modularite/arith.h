#ifndef ARITH_H_INCLUDED
#define ARITH_H_INCLUDED

int power(int x, int n);
int produit_scalaire(int a[], int b[]);
double aire(double rayon);

#endif // ARITH_H_INCLUDED

#include "a.h"

#include <stdio.h>

void print_power(int x, int n) {
    printf("x^n = %d", power(x, n));
}

double pi() {
    return 3.14;
}

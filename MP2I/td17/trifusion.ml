let rec split l =
  match l with
  | [] -> [], []
  | [x] -> [x], []
  | t1::t2::q -> (
      let l1, l2 = split q in
      t1::l1, t2::l2
  )

let rec merge_aux l1 l2 res =
  match l1, l2 with
  | [], [] -> res
  | l, [] | [], l -> (
    (List.rev l) @ res
  )
  | t1::q1, t2::q2 ->
      if t1 < t2 then
        merge_aux q1 l2 (t1::res)
      else
        merge_aux l1 q2 (t2::res)

let merge l1 l2 = List.rev (merge_aux l1 l2 [])

let rec merge_sort l =
  match l with
  | [] | [_  ] -> l
  | _ -> (
    let l1, l2 = split l in
    merge (merge_sort l1) (merge_sort l2)
  )

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void print_tab(int a[], int n) {
    for (int i = 0; i < n; ++i) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

void fusion(const int src[], int dst[], int d, int m, int f) {
    int i1 = d;
    int i2 = m;
    int j = d;
    while (i1 < m && i2 < f) {
        if (src[i1] < src[i2]) {
            dst[j] = src[i1];
            ++i1;
        } else {
            dst[j] = src[i2];
            ++i2;
        }
        ++j;
    }
    while (i1 < m) {
        dst[j] = src[i1];
        ++i1;
        ++j;
    }
    while (i2 < f) {
        dst[j] = src[i2];
        ++i2;
        ++j;
    }
}

int min(int x, int y) {
    return x < y ? x : y;
}

void tri_fusion_ascendant(int a[], int n) {
    int *tmp = calloc(n, sizeof(int));
    for (int len = 1; len < n; len*=2) {
        for (int i = 0; i < n; ++i) {
            tmp[i] = a[i];
        }
        for (int k = 0; k < n-len; k+=2*len) {
            fusion(tmp, a, k, k+len, min(k+2*len, n));
            print_tab(a, n);
        }
        printf("============\n");
    }
    free(tmp);
}



int main() {
    const int n = 13;
    int a[] = { 1, 3, -12, -6, 2, 2, 2, -1, 63, 12, -1, 4, 38 };

    tri_fusion_ascendant(a, n);
    print_tab(a, n);
    

    return 0;
}

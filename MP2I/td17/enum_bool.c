#include <stdbool.h>
#include <stdio.h>
#include <assert.h>

void incr(bool a[], int n) {
    int i = n-1;
    while (i >= 0 && a[i]) {
        a[i] = 0;
        --i;
    }
    if (i >= 0) {
        a[i] = 1;
    }
}

void print_tab(bool a[], int n) {
    for (int i = 0; i < n; ++i) {
        if (a[i]) {
            printf("V ");
        } else {
            printf("F ");
        }
    }
    printf("\n");
}

int main() {
    const int n = 3;
    bool a[] = {0, 0, 0};
    print_tab(a, n);
    incr(a, n);
    print_tab(a, n);
    incr(a, n);
    print_tab(a, n);
    incr(a, n);
    print_tab(a, n);
    incr(a, n);
    print_tab(a, n);
    incr(a, n);
    print_tab(a, n);
    incr(a, n);
    print_tab(a, n);
    incr(a, n);
    print_tab(a, n);
    incr(a, n);
    print_tab(a, n);
}

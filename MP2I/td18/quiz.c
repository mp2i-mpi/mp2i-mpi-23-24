#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

const char* filename = "quiz.csv";

typedef struct Question{
    char enonce[500];
    char* reponses[500];
    int indice;
    int nbr_reponses;

} question;

typedef struct quiz {
    question* questions;
    int taille;
}quiz;





typedef struct Vector {
    question** values;
    int size;
    int capacity; 
} vector;

vector* vector_create() {
    vector* res = malloc(sizeof(vector));
    const int capacity = 8;
    res->size = 0;
    res->capacity = capacity;
    res->values = calloc(capacity, sizeof(question*));
    return res;
}
void vector_destroy(vector* v) {
    free(v->values);
    free(v);
}


int vector_size(const vector* v) {
    return v->size;
}

question* vector_get(const vector* v, int i) {
    assert (i < v->size);
    return v->values[i];
}

void vector_add(vector* v, question* x) {
    if (v->size == v->capacity) {
        const int new_cap = v->capacity * 2;
        question** new_values = calloc(new_cap, sizeof(question*));
        for (int i = 0; i < v->size; ++i) {
            new_values[i] = v->values[i];
        }
        free(v->values);
        v->values = new_values;
        v->capacity = new_cap;
    }
    v->values[v->size] = x;
    ++v->size;
}



question *vector_values(vector* v) {
    question *res = calloc(v->size, sizeof(question));
    for (int i = 0; i < v->size; ++i) {
        res[i] = *vector_get(v, i);
    }
}



char letter_of_index(int i) {
    return 'a' + i;
}

int index_of_letter(char l) {
    return (int)(l - 'a');
}
quiz* vector_to_quiz(const vector* v) {
    quiz* qz = malloc(sizeof(quiz));
    if (qz == NULL) {
        return NULL;
    }
    
    qz->taille = v->size;
    qz->questions = malloc(v->size * sizeof(question));
    if (qz->questions == NULL) {
        free(qz);
        return NULL;
    }
    
    for (int i = 0; i < v->size; ++i) {
        qz->questions[i] = *(v->values[i]); 
    }
    
    return qz;
}

question list_to_qst (char* l[], int n) {
    srand(time(NULL));
    question q = { .indice = 0 };
    strcpy(q.enonce, l[0]);
    for (int i = 2; i < n; i++) {
        strcpy(q.reponses[i], l[i]);
    }
    int nv_indice = 1 + rand() % (n+1);
    for (int i = 0; i < n; i++) {
        if (nv_indice == i) {
            q.indice = nv_indice;
        }
    }
    return q;
}




quiz* remplire_quiz(){
    FILE* f = fopen(filename, "r");
    if (f==NULL){
        printf("pas de fichier");
    }
    vector* questions = vector_create();
    int nbr_questions = 0 ;
    char c ;
    char** tableau =calloc(10, sizeof(char*));
    int indice_tableau = 0;
    int indice_phrase = 0;
    char* phrase = calloc(500,sizeof(char));
    while ((c = fgetc(f)) != EOF) {
        if (c != ';' && c != '\n') {
            phrase[indice_phrase] = c;
            indice_phrase++;
        } else if (c == ';') {
            
            
            tableau[indice_tableau] = strdup(phrase);
            indice_tableau++;
            indice_phrase = 0;
            free(phrase);
            phrase = calloc(500, sizeof(char));
        } else if (c == '\n') {
            
            tableau[indice_tableau] = strdup(phrase);
            question* q = malloc(sizeof(question));
            *q = list_to_qst(tableau, indice_tableau + 1);
            vector_add(questions, q);
            free(phrase); 
            for (int i = 0; i < indice_tableau; i++) {
                free(tableau[i]);
            }
            free(tableau); 
            tableau = calloc(10, sizeof(char*)); 
            indice_tableau = 0;
            nbr_questions++;
            indice_phrase = 0; 
            phrase = calloc(500, sizeof(char)); 
        }
    }
    
    fclose(f);
    quiz* res = vector_to_quiz(questions);
    return  res;
}



bool EntreeValide(char* e, int indice, int nb_reponse){
    int asc_e = e[0];
    if (strlen(e)>1){
        return false;
    }
    else if ( asc_e > nb_reponse + 64 || asc_e < 65 ){
        return false;
    }
    else {
        return true;
    }
}

int PoseQuestion(question q){
    printf("%s\n",q.enonce);
    for (int i = 0; i<q.nbr_reponses; ++i){
        printf("\t%c : %s\n",letter_of_index(i),q.reponses[i]);
    }
    char entree[500];
    scanf("%s",entree);
    char r = q.indice + 65;
    if (!EntreeValide(entree,q.indice,q.nbr_reponses)){
        return 0; // Réponse invalide
    }
    if (entree[0] == r) {
        return 1; // Bonne réponse
    }
    else {
        return 2; // Mauvaise réponse
    }
}

void Quiz(quiz* quiz){
    int b_r = 0;
    int i = 0;
    while (i<quiz->taille){
        int rep = PoseQuestion(quiz->questions[i]);
        if (rep == 1) {
            printf("Bonne réponse !\n\n");
            ++b_r;
            ++i;
        }
        else if (rep == 2) {
            printf ("Mauvaise réponse.\n\n");
            ++i;
        }
        else {
            printf("Veuillez donner une réponse proposée.\n\n");
        }
    }
    printf("Résultat : %d/%d\n",b_r,quiz->taille);
}
void printQuiz(const quiz* q) {
    if (q == NULL || q->questions == NULL || q->taille <= 0) {
        printf("Le quiz est vide ou non initialisé.\n");
        return;
    }

    printf("Quiz :\n");

    for (int i = 0; i < q->taille; ++i) {
        printf("Question %d:\n", i + 1);
        printf("Enoncé: %s\n", q->questions[i].enonce);
        printf("Nombre de réponses possibles: %d\n", q->questions[i].nbr_reponses);
        printf("Réponses:\n");
        for (int j = 0; j < q->questions[i].nbr_reponses; ++j) {
            printf("%d. %s\n", j + 1, q->questions[i].reponses[j]);
        }
        printf("\n");
    }

}

int main(){
    printQuiz(remplire_quiz());
    
    return 0;

}
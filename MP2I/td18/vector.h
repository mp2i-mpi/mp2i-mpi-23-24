#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

#include "question.h"

struct Vector;
typedef struct Vector vector;

// Crée un tableau de question
vector* vector_create();

// Libère toute la mémoire pour un tableau de questions
void vector_destroy(vector* v);

// Taille du tableau
int vector_size(const vector* v);

// Récupére un élément
question* vector_get(const vector* v, int i);

// Change un élément
void vector_set(vector* v, int i, question* x);

// Ajoute un élément
void vector_add(vector* v, question* x);

// Retire un élément
question* vector_pop(vector* v);

// Renvoie une copie de toutes les valeurs
question* vector_values(vector* v);

#endif // VECTOR_H_INCLUDED
#include <malloc.h>

#include <stdio.h>

int* foo() {
    int *a = malloc(sizeof(int));
    *a = 3;

    return a;
}

int main() {
    int* p = foo();

    printf("Résultat : %d\n", *p);

    free(p);

    return 0;
}


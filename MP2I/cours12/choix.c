#include <stdio.h>

int main() {
    int a = 12;
    int b = -8;
    int* choix = NULL;
    char input;

    printf("Modifier a ou b ? ");
    scanf("%c", &input);

    if (input == 'a') {
        choix = &a;
    } else if (input == 'b') {
        choix = &b;
    } else {
        printf("Invalide\n");
        return 1;
    }

    *choix = 3;

    printf("a : %d\n", a);
    printf("b : %d\n", b);

    return 0;
}

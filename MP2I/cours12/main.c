#include <stdio.h>

void incrementer(int *p) {
    printf("Valeur à l'adresse : %d\n", *p);
    *p = *p+1;
}

int main() {
    int i = 5;
    printf("%d\n", i);
    incrementer(&i);
    printf("%d\n", i);

    return 0;
}

type digraph = bool array array

let create (n: int) : digraph = Array.make_matrix n n false
let size (g: digraph) : int = Array.length g

let add_edge (g: digraph) x y : unit = g.(x).(y) <- true

let has_edge g x y = g.(x).(y)

let succ g x =
let res = ref [] in
  for j = 0 to (size g)-1 do
    if has_edge g x j then
      res := j :: !res
  done;
  !res

let edges g =
  let res = ref [] in
  for i = 0 to (size g) - 1 do
    for j = 0 to (size g) - 1 do
      if has_edge g i j then
        res := (i, j) :: !res
      done
    done;
    !res

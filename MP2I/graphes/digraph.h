#ifndef DIGRAPH_H_INCLUDED
#define DIGRAPH_H_INCLUDED

#include <stdbool.h>

typedef struct Digraph digraph;

digraph *digraph_create(int s);
void digraph_destroy(digraph *g);
int digraph_size(digraph *g);
void digraph_add_edge(digraph *g, int x, int y);
bool digraph_has_edge(digraph *g, int x, int y);
list *digraph_succ(digraph *g, int x);

#endif // DIGRAPH_H_INCLUDED

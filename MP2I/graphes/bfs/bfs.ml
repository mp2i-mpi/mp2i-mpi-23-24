module Digraph = struct
  type digraph = int list array
  type t = digraph

  let create (n: int) : digraph = Array.make n []
  let size (g: digraph) : int = Array.length g

  let has_edge (g: digraph) (x: int) (y: int) : bool = 
    List.mem y g.(x)

  let add_edge (g: digraph) (x: int) (y: int) : unit =
    if not (has_edge g x y) then
      g.(x) <- y :: g.(x)

  let add_sym_edge (g: digraph) (x: int) (y: int) : unit =
    add_edge g x y;
    add_edge g y x

  let succ (g: digraph) (x: int) : int list = g.(x)

  let edges (g: digraph) : (int * int) list =
    let res = ref [] in
    for i = 0 to (size g) - 1 do
      let aux j =
        res := (i, j) :: !res
      in
      List.iter aux (succ g i)
    done;
    !res
end

module Queue = struct
  type 'a t = ('a list * 'a list) ref

  let create () : 'a t = ref ([], [])

  let is_empty (q: 'a t) =
    match !q with
    | [], [] -> true
    | _ -> false

  let add (q: 'a t) (x: 'a) : unit =
    let s1, s2 = !q in
    q := (x::s1, s2)

  let rec pop (q: 'a t) : 'a =
    match !q with
    | [], [] -> failwith "vide"
    | s1, h::t -> (
      q := (s1, t);
      h
    )
    | s1, [] -> (
      q := [], (List.rev s1);
      pop q
    )
end

(* Réalise un parcours en largeur du graphe g à partir du sommet x *)
let bfs (g: Digraph.t) (x: int) : int array =
  let q = Queue.create () in
  let dist = Array.make (Digraph.size g) max_int in
  dist.(x) <- 0;
  Queue.add q x;
  while not (Queue.is_empty q) do
    let y = Queue.pop q in
    let d = dist.(y) in
    List.iter (fun z ->
      if dist.(z) = max_int then (
        Queue.add q z;
        dist.(z) <- d + 1
      )
    ) (Digraph.succ g y)
  done;
  dist










let () =
  let g = Digraph.create 8 in
  let add = Digraph.add_sym_edge g in
  add 0 2;
  add 0 3;
  add 0 6;
  add 2 3;
  add 6 5;
  add 5 4;
  add 6 7;
  add 7 1;
  let dist = bfs g 0 in
  Array.iter (Printf.printf "%d ") dist;
  print_newline ()

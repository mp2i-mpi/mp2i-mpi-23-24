type 'a queue

val create : unit -> 'a queue

val is_empty : 'a queue -> bool

val add: 'a queue -> 'a -> unit

val pop: 'a queue -> 'a


let () =
  let q = create () in
  add q 1;
  add q 2;
  add q 3;
  assert (pop q = 1);
  assert (pop q = 2);
  assert (pop q = 3);
  print_endline "OK"
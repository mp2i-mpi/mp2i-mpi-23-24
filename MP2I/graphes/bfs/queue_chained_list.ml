type 'a node = {
  v: 'a;
  mutable next: 'a node option
}

type 'a queue = {
  mutable first: 'a node option;
  mutable last: 'a node option
}

let create () : 'a queue = {
    first = None;
    last = None
  }

let is_empty (q: 'a queue) : bool =
  match q.first, q.last with
  | None, None -> true
  | _ -> false

let add (q: 'a queue) (x: 'a) : unit =
  let n = Some { v = x; next = None } in
  match q.first, q.last with
  | None, None -> (
    q.first <- n;
    q.last <- n
  )
  | Some first, Some last -> (
    last.next <- n;
    q.last <- n
  )
  | _ -> failwith "impossible"

let pop (q: 'a queue) : 'a =
  match q.first, q.last with
  | None, None -> failwith "vide"
  | Some first, Some last -> (
    (
      match first.next with
      | None -> (
        q.first <- None;
        q.last <- None;
      )
      | Some n -> (
        q.first <- Some n
      )
    );
    first.v
  )
  | _ -> failwith "impossible"

let () =
  let q = create () in
  add q 1;
  add q 2;
  add q 3;
  assert (pop q = 1);
  assert (pop q = 2);
  assert (pop q = 3);
  print_endline "OK"
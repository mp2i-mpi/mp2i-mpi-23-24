type 'a queue = ('a list * 'a list) ref

let create () : 'a queue = ref ([], [])

let is_empty (q: 'a queue) : bool =
  match !q with
  | [], [] -> true
  | _ -> false

let add (q: 'a queue) (x: 'a) : unit =
  let s1, s2 = !q in
  q := (x::s1, s2)

let rec pop (q: 'a queue) : 'a =
  match !q with
  | [], [] -> failwith "vide"
  | s1, h::t -> (
    q := (s1, t);
    h
  )
  | s1, [] -> (
    q := [], (List.rev s1);
    pop q
  )

let () =
  let q = create () in
  add q 1;
  add q 2;
  add q 3;
  assert (pop q = 1);
  assert (pop q = 2);
  assert (pop q = 3);
  print_endline "OK"


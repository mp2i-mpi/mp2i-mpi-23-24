type graph = Digraph.digraph

let create = Digraph.create
let size = Digraph.size

let add_edge (g: graph) (x: int) (y: int) : unit =
  Digraph.add_edge g x y;
  Digraph.add_edge g y x

let has_edge = Digraph.has_edge
let succ = Digraph.succ

let edges = Digraph.edges

let () =
  print_endline "GRAPH";
  let g = create 4 in
  add_edge g 0 1;
  add_edge g 1 3;
  add_edge  g 2 0;
  add_edge  g 3 2;
  let n = size g in
  assert (n = 4);
  print_endline "=== has_edge ===";
  for i = 0 to n-1 do
    for j = 0 to n-1 do
      print_string (if has_edge g i j then "V" else "F")
    done;
    print_newline ()
  done;
  print_endline "=== succ ===";
  for i = 0 to n-1 do
    List.iter (Printf.printf "%d ") (succ g i);
    print_newline()
  done;
  print_endline "=== edges ===";
  List.iter (fun (x, y) -> Printf.printf "(%d, %d) " x y) (edges g);
  print_newline()



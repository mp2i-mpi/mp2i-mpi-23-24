module WDigraph = struct
  type wdigraph = (float) array array
  type t = wdigraph

  let create (n: int) : wdigraph = Array.make_matrix n n infinity
  let size (g: wdigraph) : int = Array.length g

  let weight (g: wdigraph) (x: int) (y: int) : float =
    g.(x).(y)

  let has_edge (g: wdigraph) (x: int) (y: int) : bool = 
    (weight g x y) < infinity

  let add_edge (g: wdigraph) (x: int) (y: int) (w: float) : unit =
    g.(x).(y) <- w

  let succ (g: wdigraph) (x: int) : (int * float) list =
    let res = ref [] in
    for j = 0 to (size g)-1 do
      if has_edge g x j then
        res := (j, (weight g x j)) :: !res
    done;
    !res

  let edges (g: wdigraph) : (int * int * float) list =
    let n = size g in
    let res = ref [] in
    for i = 0 to n-1 do
      for j = 0 to n-1 do
        if has_edge g i j then
          res := (i, j, (weight g i j)) :: !res
      done
    done;
    !res
end

let example_graph () =
  let g = WDigraph.create 6 in
  let add = WDigraph.add_edge g in
  (*
      0 <- 1 <- 2
      |  A |  / |
      v /  v V  v
      3 <- 4 <- 5
  *)
  add 0 3 1.0;
  add 1 0 2.0;
  add 1 4 1.0;
  add 2 1 4.0;
  add 2 4 1.0;
  add 2 5 3.0;
  add 3 1 1.0;
  add 4 3 1.0;
  add 4 5 1.0;
  
  g

let floyd_warshall (g: WDigraph.t) : float array array =
  let n = WDigraph.size g in
  let dist = Array.make_matrix n n infinity in
  List.iter (fun (x, y, d) -> dist.(x).(y) <- d) (WDigraph.edges g);
  for x = 0 to n-1 do
    dist.(x).(x) <- 0.0
  done;
  for x = 0 to n-1 do
    for y = 0 to n-1 do
      for z = 0 to n-1 do
        let new_d = dist.(y).(x) +. dist.(x).(z) in
        if new_d < dist.(y).(z) then
          dist.(y).(z) <- new_d
      done
    done
  done;
  dist

let test_floyd_warshall () =
  let g = example_graph () in
  let d = floyd_warshall g in
  assert (d.(2).(1) = 3.0);
  assert (d.(0).(5) = 4.0);
  assert (d.(5).(0) = infinity)

let () =
  test_floyd_warshall ();
  Printf.printf "OK\n"


module WDigraph = struct
  type wdigraph = (int * float) list array
  type t = wdigraph

  let create (n: int) : wdigraph = Array.make n []
  let size (g: wdigraph) : int = Array.length g

  let has_edge (g: wdigraph) (x: int) (y: int) : bool = 
    List.mem_assoc y g.(x)

  let weight (g: wdigraph) (x: int) (y: int) : float =
    List.assoc y g.(x)

  let add_edge (g: wdigraph) (x: int) (y: int) (w: float) : unit =
    if not (has_edge g x y) then
      g.(x) <- (y, w) :: g.(x)


  let succ (g: wdigraph) (x: int) : (int * float) list = g.(x)
end

module Pqueue = struct
  type 'a heap = Empty | Node of 'a heap * 'a * 'a heap

  let empty_heap : 'a heap = Empty


  let rec heap_merge (h1: 'a heap) (h2: 'a heap) : 'a heap =
    match h1, h2 with
    | Empty, h | h, Empty -> h
    | Node (l1, x, r1), Node (l2, y, r2) ->
      if x < y then 
        Node (r1, x, heap_merge l1 h2)
      else
        Node (r2, y, heap_merge h1 l2)

  let heap_add (x: 'a) (t: 'a heap) : 'a heap = heap_merge (Node (Empty, x, Empty)) t

  let heap_pop (t: 'a heap) : 'a * 'a heap =
    match t with
    | Empty -> invalid_arg "pop : empty heap"
    | Node (l, x, r) -> x, heap_merge l r

  
  let is_empty (h: 'a heap) : bool = h = Empty

  (* Fonctions utiles - file de priorité mutable *)
  type 'a t = 'a heap ref
  let create () : 'a t = ref Empty
  let is_empty (h: 'a t) : bool = !h = Empty
  let add (h: 'a t) (x: 'a) : unit = h := heap_add x !h
  let pop (h: 'a t) : 'a = let x, h' = heap_pop !h in h := h'; x
end

(* Réalise un parcours en largeur du graphe g à partir du sommet x *)
let dijkstra (g: WDigraph.t) (s: int) : float array =
  let n = WDigraph.size g in
  let q = Pqueue.create () in
  let dist = Array.make n infinity in
  let visited = Array.make n false in
  let add x d = 
    Pqueue.add q (d, x);
    dist.(x) <- d;
  in
  add s 0.0;
  while not (Pqueue.is_empty q) do
    let dx, x = Pqueue.pop q in
    if not visited.(x) then (
      visited.(x) <- true;
      List.iter (fun (y, dxy) ->
        let new_d = dx +. dxy in
        if new_d < dist.(y) then
          add y new_d
      ) (WDigraph.succ g x)
    )
  done;
  dist

  let example_graph () =
    let g = WDigraph.create 6 in
    let add = WDigraph.add_edge g in
    (*
        0 <- 1 <- 2
        |  A |  / |
        v /  v V  v
        3 <- 4 <- 5
    *)
    add 0 3 1.0;
    add 1 0 2.0;
    add 1 4 1.0;
    add 2 1 4.0;
    add 2 4 1.0;
    add 2 5 3.0;
    add 3 1 1.0;
    add 4 3 1.0;
    add 4 5 1.0;
    
    g

  let test_dijkstra () =
    let g = example_graph () in
    let d2 = dijkstra g 2 in
    assert (d2.(1) = 3.0);
    let d0 = dijkstra g 0 in
    assert (d0.(5) = 4.0);
    let d5 = dijkstra g 5 in
    assert (d5.(0) = infinity)
  
  let () =
    test_dijkstra ();
    Printf.printf "OK\n"
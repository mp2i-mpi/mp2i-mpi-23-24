type digraph

val create: int -> digraph
val size : digraph -> int
val add_edge: digraph -> int -> int -> unit
val has_edge: digraph -> int -> int -> bool
val succ: digraph -> int -> int list
val edges: digraph -> (int * int) list
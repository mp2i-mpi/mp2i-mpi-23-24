type digraph = int list array

let create (n: int) : digraph = Array.make n []

let size (g: digraph) = Array.length g

let has_edge (g: digraph) (x: int) (y: int) : bool = List.mem y g.(x)

let add_edge (g: digraph) (x: int) (y: int) : unit =
  if not (has_edge g x y) then
    g.(x) <- y::g.(x)


let succ (g: digraph) (x: int) : int list = g.(x)


let edges (g: digraph) : (int * int) list =
  let res = ref [] in
  let n = size g in
  for i = 0 to n-1 do
    List.iter (fun j -> res := (i, j) :: !res) g.(i)
  done;
  !res

let () =
  let g = create 4 in
  add_edge g 0 1;
  add_edge  g 1 0;
  add_edge g 1 3;
  add_edge  g 2 0;
  add_edge  g 3 2;
  let n = size g in
  assert (n = 4);
  print_endline "=== has_edge ===";
  for i = 0 to n-1 do
    for j = 0 to n-1 do
      print_string (if has_edge g i j then "V" else "F")
    done;
    print_newline ()
  done;
  print_endline "=== succ ===";
  for i = 0 to n-1 do
    List.iter (Printf.printf "%d ") (succ g i);
    print_newline()
  done;
  print_endline "=== edges ===";
  List.iter (fun (x, y) -> Printf.printf "(%d, %d) " x y) (edges g);
  print_newline()

module Digraph = struct
  type digraph = int list array
  type t = digraph

  let create (n: int) : digraph = Array.make n []
  let size (g: digraph) : int = Array.length g

  let has_edge (g: digraph) (x: int) (y: int) : bool = 
    List.mem y g.(x)

  let add_edge (g: digraph) (x: int) (y: int) : unit =
    if not (has_edge g x y) then
      g.(x) <- y :: g.(x)


  let succ (g: digraph) (x: int) : int list = g.(x)

  let edges (g: digraph) : (int * int) list =
    let res = ref [] in
    for i = 0 to (size g) - 1 do
      let aux j =
        res := (i, j) :: !res
      in
      List.iter aux (succ g i)
    done;
    !res
end

let rec dfs_aux (visited: bool array) (g: Digraph.t) (x: int) : unit =
  if not visited.(x) then (
    visited.(x) <- true;
    (* List.iter (dfs_aux visited g) (Digraph.succ g x) *)
    let rec loop succs = match succs with
      | [] -> ()
      | s::others -> dfs_aux visited g s; loop others
    in loop (Digraph.succ g x);
    Printf.printf "%d " x
  )

(* Faire un parcours en profondeur (préfixe) en affichant chaque sommet visité
   À la fin, on renvoie la liste des sommets visités (n'importe quel ordre) 
*)
let dfs (g: Digraph.t) (x: int) : int list =
  let visited = Array.make (Digraph.size g) false in
  dfs_aux visited g x;
  let res = ref [] in
  for i = 0 to (Digraph.size g)-1 do
    if visited.(i) then
      res := i::!res
  done;
  !res

  let rec post_order_aux (order: int list ref) (visited: bool array) (g: Digraph.t) (x: int) : unit =
    if not visited.(x) then (
      visited.(x) <- true;
      let rec loop succs = match succs with
        | [] -> ()
        | s::others -> post_order_aux order visited g s; loop others
      in loop (Digraph.succ g x);
      order := x :: !order
    )
  
  (* Faire un parcours en profondeur (préfixe) en affichant chaque sommet visité
     À la fin, on renvoie la liste des sommets visités (n'importe quel ordre) 
  *)
  let post_order (g: Digraph.t) (x: int) : int list =
    let visited = Array.make (Digraph.size g) false in
    let order = ref [] in
    post_order_aux order visited g x;
    !order

let () =
  let n = 4 in
  let g = Digraph.create n in
  Digraph.add_edge g 0 1;
  Digraph.add_edge  g 1 0;
  Digraph.add_edge g 1 3;
  Digraph.add_edge  g 2 0;
  Digraph.add_edge  g 3 2;
  for i = 0 to n-1 do
    let order = post_order g 0 in
    List.iter (Printf.printf "%d ") order;
    print_newline()
  done


type wdigraph

val create: int -> wdigraph
val size : wdigraph -> int
val add_edge: wdigraph -> int -> int -> float -> unit
val has_edge: wdigraph -> int -> int -> bool
val weight: wdigraph -> int -> int -> float
val succ: wdigraph -> int -> (float * int) list
type wdigraph = (float) array array

let create (n: int) : wdigraph = Array.make_matrix n n infinity
let size (g: wdigraph) : int = Array.length g

let weight (g: wdigraph) (x: int) (y: int) : float =
  g.(x).(y)

let has_edge (g: wdigraph) (x: int) (y: int) : bool = 
  (weight g x y) < infinity

let add_edge (g: wdigraph) (x: int) (y: int) (w: float) : unit =
  g.(x).(y) <- w

let succ (g: wdigraph) (x: int) : (int * float) list =
  let res = ref [] in
    for j = 0 to (size g)-1 do
      if has_edge g x j then
        res := (j, (weight g x j)) :: !res
    done;
    !res

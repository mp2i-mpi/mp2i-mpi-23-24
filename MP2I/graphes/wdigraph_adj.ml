type wdigraph = (int * float) list array

let create (n: int) : wdigraph = Array.make n []
let size (g: wdigraph) : int = Array.length g

let has_edge (g: wdigraph) (x: int) (y: int) : bool = 
  List.mem_assoc y g.(x)

let weight (g: wdigraph) (x: int) (y: int) : float =
  List.assoc y g.(x)

let add_edge (g: wdigraph) (x: int) (y: int) (w: float) : unit =
  if not (has_edge g x y) then
    g.(x) <- (y, w) :: g.(x)


let succ (g: wdigraph) (x: int) : (int * float) list = g.(x)

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>

#include "eleves.h"
#include "vector.h"

const char* filename = "eleves.csv";

void trim_endline(char* s) {
    int len = strlen(s);
    if (len == 0) return;
    if (s[len-1] == '\n') {
        s[len-1] = '\0';
    }
}

vector* create_eleves_table() {
    return vector_create();
}

void delete_eleves_table(vector* t) {
    for (int i = 0; i < vector_size(t); ++i) {
        if (vector_get(t, i) != NULL) {
            free(vector_get(t, i));
        }
    }
    free(t);
}

vector* load_eleves() {
    FILE* f = fopen(filename, "r");
    if (f == NULL) {
        printf("Le fichier n'existe pas.\n");
        exit(1);
    }
    int id;
    char* name = NULL;
    size_t len = 0;
    vector* res = create_eleves_table();
    while(!feof(f)) {
        eleve* e = create_eleve(id);
        fscanf(f, "%d,", &(e->id));
        fscanf(f, "%d,", &(e->age));
        getline(&name, &len, f);
        trim_endline(name);
        strcpy(e->name, name);
        vector_add(res, e);
    }
    fclose(f);
    return res;
}

void write_eleves(vector *eleves) {
    FILE* f = fopen(filename, "w");
    for (int i = 0; i < vector_size(eleves); ++i) {
        eleve* e = vector_get(eleves, i);
        if (e != NULL) {
            fprintf(f, "%d,%s\n", e->id, e->name);
        }
    }
    fclose(f);
}

void afficher_eleves() {
    vector *eleves = load_eleves();
    for (int i = 0; i < vector_size(eleves); ++i) {
        eleve *e = vector_get(eleves, i);
        if (e != NULL) {
            printf("%d) %s (%d ans)\n", e->id, e->name, e->age);
        }
    }
    delete_eleves_table(eleves);
}

int max(int a, int b) {
    return a < b ? b : a;
}

void ajouter_eleve(const char* new_name) {
    vector* eleves = load_eleves();
    int id_max = 0;
    for (int i = 0; i < vector_size(eleves); ++i) {
        eleve* e = vector_get(eleves, i);
        assert (e != NULL);
        id_max = max(id_max, e->id);
    }
    int new_id = id_max+1;
    eleve* new_e = create_eleve(new_id);
    strcpy(new_e->name, new_name);
    vector_add(eleves, new_e);
    write_eleves(eleves);
    delete_eleves_table(eleves);
}

int main(int argc, char* argv[]) {
    if (argc <= 1) {
        printf("Il faut choisir une commande.\n");
        printf("Commandes possibles : afficher, ajouter\n");
        return 1;
    }
    if (strcmp(argv[1], "afficher") == 0) {
        afficher_eleves();
        return 0;
    }
    if (strcmp(argv[1], "ajouter") == 0) {
        if (argc <= 2) {
            printf("Il faut un nom pour le nouvel élève.\n");
            return 1;
        }
        ajouter_eleve(argv[2]);
        return 0;
    }
    
    printf("Commande inconnue : %s\n", argv[1]);
    return 1;
}

#ifndef ELEVES_H_INCLUDED
#define ELEVES_H_INCLUDED

typedef struct Eleve {
    int id;
    int age;
    char name[50];
} eleve;

eleve* create_eleve(int id);

#endif // ELEVES_H_INCLUDED

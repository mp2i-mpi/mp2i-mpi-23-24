#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#include "vector.h"

typedef struct Vector {
    eleve** values;
    int size;
    int capacity; // size <= capacity
} vector;

vector* vector_create() {
    vector* res = malloc(sizeof(vector));
    const int capacity = 8;
    res->size = 0;
    res->capacity = capacity;
    res->values = calloc(capacity, sizeof(eleve*));
    return res;
}

void vector_destroy(vector* v) {
    free(v->values);
    free(v);
}

int vector_size(const vector* v) {
    return v->size;
}

eleve* vector_get(const vector* v, int i) {
    assert (i < v->size);
    return v->values[i];
}

void vector_set(vector* v, int i, eleve* x) {
    assert (i < v->size);
    v->values[i] = x;
}

void vector_add(vector* v, eleve* x) {
    if (v->size == v->capacity) {
        const int new_cap = v->capacity * 2;
        eleve** new_values = calloc(new_cap, sizeof(eleve*));
        for (int i = 0; i < v->size; ++i) {
            new_values[i] = v->values[i];
        }
        free(v->values);
        v->values = new_values;
        v->capacity = new_cap;
    }
    v->values[v->size] = x;
    ++v->size;
}

eleve* vector_pop(vector* v) {
    assert (v->size != 0);
    v->values[v->size-1];
    --v->size;
}



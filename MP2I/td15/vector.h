#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

#include "eleves.h"

struct Vector;
typedef struct Vector vector;

vector* vector_create();
void vector_destroy(vector* v);
int vector_size(const vector* v);
eleve* vector_get(const vector* v, int i);
void vector_set(vector* v, int i, eleve* x);
void vector_add(vector* v, eleve* x);
eleve* vector_pop(vector* v);

#endif // VECTOR_H_INCLUDED
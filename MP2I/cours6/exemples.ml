
let table_mul n =
  let rec loop k =
    if k < 10 then (
      Printf.printf "%d x %d = %d\n" k n (k*n);
      loop (k+1)
    )
  in
  loop 0

let () = table_mul 7

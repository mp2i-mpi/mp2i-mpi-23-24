(* Problème : calculer la factorielle d'un nombre n *)

(* resultat = 1
Pour i allant de 1 à n
  resultat = resultat * i *)

(* Problème : calculer la factorielle d'un nombre n, sachant que la factorielle de (n-1) a pour valeur fact_n_1 *)

let rec fact n =
  if n = 0 then 1 else (
    let fact_n_1 = fact (n-1) in
    n*fact_n_1
  )

(* Problème : je veux calculer la multiplication de k! et de res
   Il suffit simplement de dire que ce problème est exactement équivalent
   à la multiplication de (k-1)! et de (res*k)
*)

let rec fact_aux k res =
  if k = 0 then
    res
  else (
    fact_aux (k-1) (res*k)
  )

let fact n = fact_aux n 1

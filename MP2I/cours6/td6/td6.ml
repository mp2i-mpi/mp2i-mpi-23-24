type t = A | B | C




let compte_B_consecutifs l =
  let rec loop serie_actuelle serie_max reste =
    match reste with
    | [] -> serie_max
    | t::q ->
        if t = B then (
          let nouvelle_serie_actuelle = serie_actuelle+1 in
          let nouvelle_serie_max = max serie_max nouvelle_serie_actuelle in
          loop nouvelle_serie_actuelle nouvelle_serie_max q
        )
        else (
          let nouvelle_serie_actuelle = 0 in
          let nouvelle_serie_max = serie_max in
          loop nouvelle_serie_actuelle nouvelle_serie_max q
        )
  in loop 0 0 l



#include <stdlib.h>
#include <stdio.h>

typedef struct Node {
    struct Node *left;
    int v;
    struct Node *right;
} bintree;

bintree *bintree_create(bintree *l, int v, bintree *r) {
    bintree *n = malloc(sizeof(bintree));
    n->left = l;
    n->v = v;
    n->right = r;
    return n;
}

void bintree_free(bintree *t) {
    if (t == NULL) {
        return;
    }
    bintree_free(t->left);
    bintree_free(t->right);
    free(t);
}

int bintree_size(bintree *t) {
    if (t == NULL) {
        return 0;
    }
    return 1 + bintree_size(t->left) + bintree_size(t->right);
}

int bintree_height(bintree *t) {
    if (t == NULL) {
        return -1;
    }
    int hl = bintree_height(t->left);
    int hr = bintree_height(t->right);
    return 1 + (hl > hr ? hl : hr);
}

bintree *bintree_perfect(int h) {
    if (h == -1) {
        return NULL;
    }
    return bintree_create(bintree_perfect(h - 1), h, bintree_perfect(h - 1));
}

void print_spaces(int h) {
    for (int i = 0; i <= h; i++) {
        printf("    ");
    }
}

void pretty_print_aux(bintree *t, int h) {
    if (t == NULL) {
        print_spaces(h);
        printf("E\n");
    } else {
        pretty_print_aux(t->right, h + 1);
        print_spaces(h);
        printf("%d\n", t->v);
        pretty_print_aux(t->left, h + 1);
    }
}

void bintree_pretty_print(bintree *n) {
    pretty_print_aux(n, 0);
}

int main() {
    bintree *t = bintree_perfect(3);
    bintree_pretty_print(t);
}
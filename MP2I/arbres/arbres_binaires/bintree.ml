type 'a bintree =
  | E
  | N of 'a bintree * 'a * 'a bintree

let rec size (t: 'a bintree) : int =
  match t with
  | E -> 0
  | N (l, _, r) -> 1 + size l + size r

(* Renvoie un arbre parfait de hauteur h *)
(* Chaque noeud est étiqueté par sa hauteur *)
let rec perfect (h: int) : int bintree =
  if h = -1 then
    E
  else
    let st = perfect (h - 1) in
    N (st, h, st)

(* let rec is_perfect (t: 'a bintree) : bool =
  match t with
  | E -> true
  | N (l, _, r) -> size l = size r && is_perfect l && is_perfect r *)

let rec height (t: 'a bintree) : int =
  match t with
  | E -> -1
  | N (l, _, r) -> 1 + max (height l) (height r)

let rec is_perfect_of_height (t: 'a bintree) (h: int) : bool =
  match t with
  | E -> h = -1
  | N (l, _, r) -> is_perfect_of_height l (h - 1) && is_perfect_of_height r (h - 1)

let is_perfect (t: 'a bintree) = is_perfect_of_height t (height t)

let pretty_print (t: int bintree) : unit =
  let rec print_spaces h =
    for _ = 0 to h do
      print_string "    "
    done
  in
  let rec aux (t: int bintree) (h: int) : unit =
    match t with
    | E -> (
      print_spaces h;
      print_endline "E"
    )
    | N (l, x, r) -> (
      aux r (h + 1);
      print_spaces h;
      print_int x;
      print_newline ();
      aux l (h + 1)
    )
  in
  aux t 0

let rec parcours_prefixe (t: int bintree) : unit =
  match t with
  | E -> ()
  | N(l, v, r) -> (
    Printf.printf "%d\n" v;
    parcours_prefixe l;
    parcours_prefixe r
  )

let rec parcours_infixe (t: int bintree) : unit =
  match t with
  | E -> ()
  | N(l, v, r) -> (
    parcours_infixe l;
    Printf.printf "%d\n" v;
    parcours_infixe r
  )

let rec parcours_postfixe (t: int bintree) : unit =
  match t with
  | E -> ()
  | N(l, v, r) -> (
    parcours_postfixe l;
    parcours_postfixe r;
    Printf.printf "%d\n" v
  )

let () =
  let t = N(
              N(
                  N(
                      E,
                    3,
                      E
                  ),
                1,
                  E
              ),
            0,
              N(
                  E,
                2,
                  N(
                      N(
                          E,
                        5,
                          E
                      ),
                    4,
                      E
                  )
              )
          )
  in
  print_endline "Préfixe";
  parcours_prefixe t;
  print_endline "Infixe";
  parcours_infixe t;
  print_endline "Postfixe";
  parcours_postfixe t


type color = R | B

type 'k arn =
  | E
  | N of (color * 'k arn  * 'k * 'k arn)

let pretty_print (key_print : 'k -> unit) (t: 'k arn) : unit =
  let rec print_spaces h =
    for _ = 0 to h do
      print_string "    "
    done
  in
  let rec aux (t: 'k arn) (h: int) : unit =
    match t with
    | E -> (
      print_spaces h;
      print_endline "E"
    )
    | N (c, l, x, r) -> (
      aux r (h + 1);
      print_spaces h;
      print_string (match c with R -> "R(" | B -> "B(" );
      key_print x;
      print_string ")";
      print_newline ();
      aux l (h + 1)
    )
  in
  aux t 0

let pretty_print_int = pretty_print print_int
let pretty_print_string = pretty_print print_string

let empty = E

let lbalance (n: (color * 'k arn  * 'k * 'k arn)) : 'k arn =
  match n with
  | (B, N(R, N(R, a1, z, a2), y, a3), x, a4) | (B, N(R, a1, z, N(R, a2, y, a3)), x, a4)
    -> N(R, N(B, a1, z, a2), y, N(B, a3, x, a4))
  | (c, l, k, r) -> N (c, l, k, r)

let rbalance (n: (color * 'k arn  * 'k * 'k arn)) : 'k arn =
  match n with
  | (B, a1, x, N(R, a2, y, N(R, a3, z, a4))) | (B, a1, x, N(R, N(R, a2, y, a3), z, a4))
    -> N(R, N(B, a1, x, a2), y, N(B, a3, z, a4))
  | (c, l, k, r) -> N (c, l, k, r)

let rec add_aux a k =
  match a with
  | E -> N (R, E, k, E)
  | N (_, l, x, r) when k = x -> a
  | N (c, l, x, r) when k < x -> lbalance (c, add_aux l k, k, r)
  | N (c, l, x, r) -> rbalance (c, l, x, add_aux r k)

let add a k =
  match add_aux a k with
  | E -> failwith "impossible"
  | N(_, l, x, r)->N(B, l, x, r)

let rec find a k =
  match a with
  | E -> false
  | N (_, l, x, r) when k = x -> true
  | N (_, l, x, r) when k < x -> find l k
  | N (_, l, x, r) -> find l k

let rec build_test_tree_aux l res =
  match l with
  | [] -> res
  | t::q -> (
    pretty_print_int res;
    print_endline "=============";
    build_test_tree_aux q (add res t)
  )

let build_test_tree l = build_test_tree_aux l empty

let test_add () =
  let a = build_test_tree [1; 2; 3; 4; 5; 6; 7; 8; 9] in
  pretty_print_int a


let () =
  test_add ()


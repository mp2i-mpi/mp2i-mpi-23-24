type 'a heap =
  | E
  | N of 'a heap * 'a * 'a heap

let get_min (t: 'a heap) : 'a =
  match t with
  | E -> failwith "tas vide"
  | N (_, x, _) -> x

let rec merge (t1: 'a heap) (t2: 'a heap): 'a heap =
  match t1, t2 with
  | E, t | t, E -> t
  | N(l1, x1, r1), N(l2, x2, r2) -> (
    if x1 < x2 then
      N (merge r1 t2, x1, l1)
    else
      N (merge r2 t1, x2, l2)
  )

let pop_min (t: 'a heap) : ('a heap * 'a) =
  match t with
  | E -> failwith "tas vide"
  | N (l, x, r) -> (merge l r), x

let insert (t: 'a heap) (x: 'a) : ('a heap) =
  merge t (N(E, x, E))
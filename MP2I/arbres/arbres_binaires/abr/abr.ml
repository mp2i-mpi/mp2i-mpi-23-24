type 'k abr =
  | E
  | N of ('k abr * 'k * 'k abr)

let pretty_print (key_print : 'k -> unit) (t: 'k abr) : unit =
  let rec print_spaces h =
    for _ = 0 to h do
      print_string "    "
    done
  in
  let rec aux (t: 'k abr) (h: int) : unit =
    match t with
    | E -> (
      print_spaces h;
      print_endline "E"
    )
    | N (l, x, r) -> (
      aux r (h + 1);
      print_spaces h;
      key_print x;
      print_newline ();
      aux l (h + 1)
    )
  in
  aux t 0

let pretty_print_int = pretty_print print_int
let pretty_print_string = pretty_print print_string

let empty = E

let rec add a k =
  match a with
  | E -> N (E, k, E)
  | N (l, x, r) when k = x -> a
  | N (l, x, r) when k < x -> N (add l k, x, r)
  | N (l, x, r) -> N (l, x, add r k)

let rec find a k =
  match a with
  | E -> false
  | N (l, x, r) when k = x -> true
  | N (l, x, r) when k < x -> find l k
  | N (l, x, r) -> find l k

let rotation_D a =
  match a with
  | N (N(a1, y, a2), x, a3) -> N (a1, y, N(a2, x, a3))
  | _ -> a

let rotation_G a =
  match a with
  | N (a1, x, N(a2, y, a3)) -> N (N(a1, x, a2), y, a3)
  | _ -> a

let rec build_test_tree_aux l res =
  match l with
  | [] -> res
  | t::q -> build_test_tree_aux q (add res t)

let build_test_tree l = build_test_tree_aux l empty

let test_add () =
  let a = build_test_tree [1; 2; 3; 4; 5; 6; 7; 8; 9] in
  pretty_print_int a

let sn k = N(E, k, E)

let test_rotate () =
  let a = N (N(sn("a1"), "y", sn("a2")), "x", sn("a3")) in
  pretty_print_string a;
  print_endline "-------------";
  pretty_print_string (rotation_D a);
  print_endline "=============";

  let a = N (sn("a1"), "x", N(sn("a2"), "y", sn("a3"))) in
  pretty_print_string a;
  print_endline "-------------";
  pretty_print_string (rotation_G a);
  print_endline "=============";

  let a = build_test_tree [5; 12; 4; 8; 35; -5; 12] in
  pretty_print_int a;
  print_endline "-------------";
  pretty_print_int (rotation_G a);
  print_endline "============="

let () =
  test_add ()
  (* test_rotate () *)


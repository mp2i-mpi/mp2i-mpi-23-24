#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

typedef struct Pqueue {
    int *data;
    int size;
    int cap;
} pqueue;

pqueue *pqueue_create(int cap) {
    pqueue *res = malloc(sizeof(pqueue));
    res->cap = cap;
    res->size = 0;
    res->data = calloc(cap, sizeof(int));
    return res;
}

void print_pqueue(const pqueue *q) {
    for (int i = 0; i < q->size; ++i) {
        printf("%d ", q->data[i]);
    }
    for (int i = q->size; i < q->cap; ++i) {
        printf("@ ");
    }
    printf("\n");
}

void pqueue_insert(pqueue *q, int v) {
    assert (q->size < q->cap);
    int i = q->size;
    while(i>0) {
        int parent_i = (i-1)/2;
        int parent = q->data[parent_i];
        if (parent <= v) {
            break;
        }
        q->data[i] = parent;
        i = parent_i;
    }
    q->data[i] = v;
    ++q->size;
}

int pqueue_insert_no_break_aux(pqueue *q, int v) {
    int i = q->size;
    while(i>0) {
        int parent_i = (i-1)/2;
        int parent = q->data[parent_i];
        if (parent <= v) {
            return i;
        }
        q->data[i] = parent;
        i = parent_i;
    }
    return 0;
}

void pqueue_insert_no_break(pqueue *q, int v) {
    assert (q->size < q->cap);
    int final_i = pqueue_insert_no_break_aux(q, v);
    q->data[final_i] = v;
    ++q->size;
}

int pqueue_pop(pqueue *q) {
    assert(q->size > 0);
    int res = q->data[0];
    int n = q->size-1;
    --q->size;
    if (n == 0) {
        return res;
    }
    int x = q->data[n];
    int i = 0;
    while (true) {
        int j = 2*i+1;
        if (j >= n) {
            break;
        }
        if (j+1 < n && q->data[j] > q->data[j+1]) {
            ++j;
        }
        if (x <= q->data[j]) {
            break;
        }
        q->data[i] = q->data[j];
        i = j;
    }
    q->data[i] = x;
    return res;
}

int main() {
    pqueue *q = pqueue_create(11);
    for (int i = 10; i >= 0; --i) {
        pqueue_insert(q, i);
        print_pqueue(q);
    }
    for (int i = 10; i >= 0; --i) {
        printf("%d\n", pqueue_pop(q));
        print_pqueue(q);
    }
}

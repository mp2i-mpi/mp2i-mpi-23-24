module Bintree = struct
  type 'a bintree =
    | E
    | N of 'a bintree * 'a * 'a bintree

  type 'a t = 'a bintree
end

module Tree = struct
  type 'a tree =
    | N of 'a * 'a tree list
    
  type 'a t = 'a tree
end


let rec bintree_of_forest (l: 'a Tree.t list) : 'a Bintree.t =
  match l with
  | [] -> Bintree.E
  | (Tree.N (x, children)) :: q -> Bintree.N (bintree_of_forest children, x, bintree_of_forest q)

let bintree_of_tree (t: 'a Tree.t) : 'a Bintree.t = bintree_of_forest [t]

let rec forest_of_bintree (t: 'a Bintree.t) : 'a Tree.t list =
  match t with
  | Bintree.E -> []
  | Bintree.N (l, x, r) -> (Tree.N(x, forest_of_bintree l)) :: (forest_of_bintree r)

let tree_of_bintree (t: 'a Bintree.t) : 'a Tree.t =
  match forest_of_bintree t with
  | [t] -> t
  | _ -> failwith "vide"

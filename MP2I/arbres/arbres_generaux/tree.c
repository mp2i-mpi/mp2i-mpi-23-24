#include <stdlib.h>

typedef struct Tree {
    int v;

    struct Tree *children;
    struct Tree *next;
} tree;

tree *tree_create(int v) {
    tree *t = malloc(sizeof(tree));
    t->v = v;
    t->children = NULL;
    t->next = NULL;
    return t;
}

void tree_destroy(tree *t) {
    for (tree *c = t->children; c != NULL; c = c->next) {
        tree_destroy(c);
    }
    free(t);
}

void tree_add_child(tree *t, tree *c) {
    if (t->children == NULL) {
        t->children = c;
    } else {
        tree *child = t->children;
        while (child->next != NULL) {
            child = child->next;
        }
        child->next = c;
    }
}

int tree_size(tree *t) {
    int res = 1;
    for (tree *c = t->children; c != NULL; c = c->next) {
        res += tree_size(c);
    }
    return res;
}

void main() {
    
}



type 'a tree =
  | N of 'a * 'a tree list

let is_leaf (a : 'a tree) =
  match a with
  | N(_, []) -> true
  | _ -> false

let is_leaf_simple (N (_, l)) =
  match l with
  | [] -> true
  | _ -> false

let rec size (N (_, l)) =
  1 + size_forest l
and size_forest (l: 'a tree list) =
  match l with
  | [] -> 0
  | t::q -> size t + size_forest l

let rec parcours_prefixe (t: int tree) : unit =
  let N (r, children) = t in
  Printf.printf "%d\n" r;
  List.iter parcours_prefixe children

let rec parcours_postfixe (t: int tree) : unit =
  let N (r, children) = t in
  List.iter parcours_postfixe children;
  Printf.printf "%d\n" r

let leaf x = N(x, [])

let () =
  let t = N(0, [
            N(1, [
              leaf 2
            ]);
            N(3, [
              leaf 4;
              N(5, [
                leaf 7
              ]);
              leaf 6
            ])
          ]) in
  parcours_prefixe t;
  print_newline ();
  print_endline "============";
  parcours_postfixe t

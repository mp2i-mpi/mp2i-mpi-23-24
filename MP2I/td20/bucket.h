#ifndef BUCKET_H_INCLUDED
#define BUCKET_H_INCLUDED

#include <stdbool.h>

typedef struct Bucket bucket;

bucket *bucket_create();
void bucket_destroy(bucket *b);

void bucket_put(bucket *b, char* k, int v);
void bucket_remove(bucket *b, char* k);

bool bucket_contains(bucket *b, char* k);
int bucket_get(bucket *b, char* k);


#endif // BUCKET_H_INCLUDED
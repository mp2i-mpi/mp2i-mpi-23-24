#ifndef HASHTBL_H_INCLUDED
#define HASHTBL_H_INCLUDED

#include <stdbool.h>

typedef struct Hashtbl hashtbl;

/// Crée une table de hachage de capacité `capacity`.
hashtbl *hashtbl_create(int capacity);

/// Détruit la table de hachage `t`.
void hashtbl_destroy(hashtbl *t);

/// Ajoute la clé `k` à la table de hachage `t`, avec la valeur `v`.
/// Si la clé `k` est déjà présente dans la table de hachage, sa valeur est remplacée par `v`.
void hashtbl_put(hashtbl *t, char *k, int v);

/// Retourne vrai si la table de hachage `t` contient la clé `k`.
bool hashtbl_contains(hashtbl *t, char *k);

/// Retourne la valeur associée à la clé `k` dans la table de hachage `t`.
/// Précondition : la clé `k` est présente dans la table de hachage `t`.
int hashtbl_get(hashtbl *t, char *k);

/// Supprime l'entrée de clé `k` de la table de hachage `t`.
void hashtbl_remove(hashtbl *t, char *k);

#endif // HASHTBL_H_INCLUDED
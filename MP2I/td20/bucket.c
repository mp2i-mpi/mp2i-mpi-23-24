#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include "bucket.h"

typedef struct Assoc {
    char* key;
    int value;
} assoc;

void assoc_assign_key(assoc *a, char* k) {
    if (a->key != NULL) {
        free(a->key);
    }
    a->key = calloc(strlen(k)+1, sizeof(char));
    strcpy(a->key, k);
}

void assoc_free_key(assoc *a) {
    free(a->key);
}

typedef struct Node {
    assoc v;
    struct Node *next;
} node;

typedef struct Bucket {
    node *head;
} bucket;

node *node_create(char* k, int v) {
    node *n = malloc(sizeof(node));
    assoc_assign_key(&(n->v), k);
    n->v.value = v;
    n->next = NULL;
    return n;
}

void node_destroy(node *n) {
    assoc_free_key(&(n->v));
    free(n);
}

bucket *bucket_create() {
    bucket *b = malloc(sizeof(bucket));
    b->head = NULL;
    return b;
}

void bucket_destroy(bucket *b) {
    node *n = b->head;
    while (n != NULL) {
        node *next = n->next;
        free(n);
        n = next;
    }
    free(b);
}

void bucket_put(bucket *b, char* k, int v) {
    node *n = b->head;
    if (n == NULL) {
        b->head = node_create(k, v);
        return;
    }
    while (n->next != NULL) {
        if (strcmp(n->v.key, k) == 0) {
            n->v.value = v;
            return;
        }
        n = n->next;
    }
    if (strcmp(n->v.key, k) == 0) {
        n->v.value = v;
        return;
    }
    n->next = node_create(k, v);
}

void bucket_remove(bucket *b, char* k) {
    node *n = b->head;
    if (n == NULL) {
        return;
    }
    if (strcmp(n->v.key, k) == 0) {
        b->head = n->next;
        node_destroy(n);
        return;
    }
    while (n->next != NULL) {
        if (strcmp(n->next->v.key, k) == 0) {
            node *next = n->next->next;
            node_destroy(n->next);
            n->next = next;
            return;
        }
        n = n->next;
    }
}

bool bucket_contains(bucket *b, char* k) {
    node *n = b->head;
    while (n != NULL) {
        if (strcmp(n->v.key, k) == 0) {
            return true;
        }
        n = n->next;
    }
    return false;
}

int bucket_get(bucket *b, char* k) {
    node *n = b->head;
    while (n != NULL) {
        if (strcmp(n->v.key, k) == 0) {
            return n->v.value;
        }
        n = n->next;
    }
    assert (false);
}

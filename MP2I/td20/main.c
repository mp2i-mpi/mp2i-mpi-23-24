#include "hashtbl.h"

#include <assert.h>
#include <stdio.h>

int main() {
    hashtbl *t = hashtbl_create(10);
    printf("Create OK\n");
    fflush(stdout);

    hashtbl_put(t, "hello", 5);
    hashtbl_put(t, "world", -4);
    printf("Put OK\n");
    fflush(stdout);

    assert(hashtbl_contains(t, "hello"));
    assert(hashtbl_contains(t, "world"));
    assert(!hashtbl_contains(t, "blabla"));
    printf("Contains OK\n");
    fflush(stdout);

    assert(hashtbl_get(t, "hello") == 5);
    assert(hashtbl_get(t, "world") == -4);
    printf("Get OK\n");
    fflush(stdout);

    hashtbl_put(t, "hello", 88);
    assert(hashtbl_get(t, "hello") == 88);
    printf("Put existing OK\n");
    fflush(stdout);

    hashtbl_destroy(t);
    return 0;
}

#include "hashtbl.h"
#include "bucket.h"

#include <assert.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct Hashtbl {
    int capacity;
    bucket* *buckets;
} hashtbl;

/// Crée une table de hachage de capacité `capacity`.
hashtbl *hashtbl_create(int capacity) {
    hashtbl *t = malloc(sizeof(hashtbl));
    t->capacity = capacity;
    t->buckets = calloc(capacity, sizeof(bucket*));
    for (int i = 0; i < capacity; i++) {
        t->buckets[i] = bucket_create();
    }
    return t;
}

/// Détruit la table de hachage `t`.
void hashtbl_destroy(hashtbl *t) {
    for (int i = 0; i < t->capacity; i++) {
        bucket_destroy(t->buckets[i]);
    }
    free(t->buckets);
    free(t);
}


int hash(const char *k) {
    int h = 0;
    char c;
    c = *k;
    while (c != '\0') {
        h = 31 * h + (int)c;
        ++k;
        c = *k;
    }
    return h;
}

int hashtbl_bucket(const char* k, int capacity) {
    int i = hash(k) & INT_MAX;
    return i % capacity;
}


/// Ajoute la clé `k` à la table de hachage `t`, avec la valeur `v`.
/// Si la clé `k` est déjà présente dans la table de hachage, sa valeur est remplacée par `v`.
void hashtbl_put(hashtbl *t, char *k, int v) {
    int i = hashtbl_bucket(k, t->capacity);
    bucket_put(t->buckets[i], k, v);
}

/// Retourne vrai si la table de hachage `t` contient la clé `k`.
bool hashtbl_contains(hashtbl *t, char *k) {
    int i = hashtbl_bucket(k, t->capacity);
    return bucket_contains(t->buckets[i], k);
}

/// Retourne la valeur associée à la clé `k` dans la table de hachage `t`.
/// Précondition : la clé `k` est présente dans la table de hachage `t`.
int hashtbl_get(hashtbl *t, char *k) {
    int i = hashtbl_bucket(k, t->capacity);
    return bucket_get(t->buckets[i], k);
}

/// Supprime l'entrée de clé `k` de la table de hachage `t`.
void hashtbl_remove(hashtbl *t, char *k) {
    int i = hashtbl_bucket(k, t->capacity);
    return bucket_remove(t->buckets[i], k);
}

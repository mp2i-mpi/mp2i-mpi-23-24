type t

(* Le dénominateur doit être non nul *)
val create : int -> int -> t
val add: t -> t -> t
val neg : t -> t
val sub : t -> t -> t
val mul : t -> t -> t
val inv : t -> t
val div : t -> t -> t

val to_float : t -> float
val to_string : t -> string


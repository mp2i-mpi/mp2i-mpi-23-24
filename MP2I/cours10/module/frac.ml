let rec pgcd a b = 
  if b = 0 then a
  else pgcd b (a mod b)

type t = {
  num : int;
  denum : int
}

let simp f = if f.denum > 0 then { num = f.num/(pgcd f.num f.denum) ;denum = f.denum/(pgcd f.num f.denum)}
              else { num = -f.num/(pgcd f.num f.denum); denum = f.denum/(pgcd f.num f.denum)}

let create num den =
  assert (den <> 0);
  simp {num = num ;denum = den}

let add f1 f2 = simp {num = (f1.num*f2.denum + f2.num*f1.denum); denum = (f1.denum*f2.denum)}

let neg f = simp {num = -f.num ; denum = f.denum}

let sub f1 f2 = simp {num = (f1.num*f2.denum - f2.num*f1.denum); denum = (f1.denum*f2.denum) }

let mul f1 f2 = simp {num = (f1.num*f2.num) ;denum = (f1.denum*f2.denum)}

let inv f = simp {num = f.denum; denum = f.num}

let div f1 f2 = simp {num = (f1.num*f2.denum); denum = (f1.denum*f2.num)}

let to_float f = (float_of_int f.num) /. (float_of_int f.denum)

let to_string f = 
  Printf.sprintf "%d/%d" f.num f.denum


(* let ask_for_number () =
  Printf.printf "Entrez un nombre : ";
  read_int ()

let exo1 () =
  let n = ask_for_number () in
  let rec sum i =
    if i <= n then
      (if i mod 3 = 0 || i mod 5 = 0 then i else 0) + sum (i + 1)
    else
      0
    in Printf.printf "Somme des multiples de 3 et 5 de 1 à n : %d\n" (sum 1)

(* let () = exo1 () *)

let exo2 () =
  let n = ask_for_number () in
  Printf.printf "Produit (P) ou Somme (S) ? ";
  let answer = read_line () in
  let op = match answer with
    | "P" -> ( * )
    | "S" -> ( + )
    | _ -> failwith "Réponse invalide"
  in
  let neutral = match answer with
    | "P" -> 1
    | "S" -> 0
    | _ -> failwith "Réponse invalide"
  in
  let rec sum i =
    if i <= n then
      op i (sum (i + 1))
    else
      neutral
    in Printf.printf "Résultat : %d\n" (sum 1)

let () = exo2 () *)


let rec max_l l =
  match l with
  | [] -> failwith "liste vide"
  | [t] -> t
  | t::q ->
      let max_reste = max_l q in
      if t > max_reste then
        t
      else
        max_reste

let () = Printf.printf "Résultat : %d\n" (max_l [3;-12;8;7;6;24;-3; 0])

(* let somme_carree x y =
    let somme = x + y in
    somme*somme

let print_int_newline x =
  let () = print_int x in
  print_newline() *)

(* 
let () = print_int_newline (somme_carree 2 3) *)


(* let print_entry x y =
  print_int x;
  print_string " x ";
  print_int y;
  print_string " = ";
  print_int (x*y);
  print_newline() *)
(* 
let print_entry x y = Printf.printf "%d x %d = %d\n" x y (x*y)

let mul_table n =
  let rec aux i =
    if i <= 10 then begin
      print_entry n i;
      aux (i+1)
    end
    else ()
  in
  aux 1

let () = mul_table 4 *)

let rec somme l =
  match l with
  | [] -> 0
  | t::q -> t + somme q

let () = Printf.printf "%d\n" (somme [1;2;3;4;5;6;7;8;9;10])

let dumb2 p =
  let x, y = p in
  x + y

let dumb2_other p =
  match p with
  | x, y -> x + y

#include <stdio.h>
#include <stdlib.h>

void afficher_matrice(int** t, int n, int m) {
    for(int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            printf("%d ", t[i][j]);
        }
        printf("\n");
    }
}

int main() {
    int** mat = calloc(5, sizeof(int*));
    for (int i = 0; i < 5; ++i) {
        mat[i] = calloc(5, sizeof(int));
        for (int j = 0; j < 5; ++j) {
            mat[i][j] = 0;
        }
    }
    mat[2][4] = 12;
    afficher_matrice(mat, 5, 5);

    for (int i = 0; i < 5; ++i) {
        free(mat[i]);
    }
    free(mat);
    return 0;
}
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>

int main() {
    char* s1 = "Alice";
    char* s2 = calloc(6, sizeof(char));
    for (int i = 0; i < 6; ++i) {
        s2[i] = s1[i];
    }
    printf("s2 : %s\n", s2);
    if (strcmp(s1, s2) == 0) {
        printf("Pareil !\n");
    } else {
        printf("Pas pareil !\n");
        
    }
    int i = atoi("dqsks");
    printf("i : %d\n", i);
    return 0;
}
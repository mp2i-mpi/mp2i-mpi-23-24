#include <stdio.h>

void carres(int t[], int n) {
    for(int i = 0; i < n; ++i) {
        int x = t[i];
        t[i] = x*x;
    }
}

void afficher_tableau(int t[], int n) {
    for(int i = 0; i < n; ++i) {
        printf("%d ", t[i]);
    }
}

int main() {
    int t[] = {-2, 3, 6};
    carres(t, 3);
    afficher_tableau(t, 3);   
}
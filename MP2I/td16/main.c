#include <assert.h>
#include <math.h>
#include <stdio.h>

#include "vector.h"

const double test_epsilon = 0.0000001;

void test_simple() {
    vector *v = vector_create();
    vector_add(v, 3.2);
    vector_add(v, -2.0);
    assert(vector_size(v) == 2);
    assert(fabs(vector_get(v, 0) - 3.2) < test_epsilon);
    vector_set(v, 1, 0.0);
    assert(vector_size(v) == 2);
    assert(vector_get(v, 1) == 0);
    assert(fabs(vector_pop(v) - 0.0) < test_epsilon);
    assert(vector_size(v) == 1);
    vector_destroy(v);
}

void test_realloc() {
    vector *v = vector_create();
    for (int i = 0; i < 30; ++i) {
        vector_add(v, (double) i);
    }
    assert (vector_size(v) == 30);
    for (int i = 0; i < vector_size(v); ++i) {
        printf("%f; ", vector_get(v, i));
    }
    vector_destroy(v);
}

int main() {
    test_simple();
    test_realloc();
}

#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

struct Vector;
typedef struct Vector vector;

vector* vector_create();
void vector_destroy(vector* v);
int vector_size(const vector* v);
double vector_get(const vector* v, int i);
void vector_set(vector* v, int i, double x);
void vector_add(vector* v, double x);
double vector_pop(vector* v);

#endif // VECTOR_H_INCLUDED
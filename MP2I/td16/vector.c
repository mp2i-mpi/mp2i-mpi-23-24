#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#include "vector.h"

typedef struct Vector {
    double* values;
    int size;
    int capacity; // size <= capacity
} vector;

vector* vector_create() {
    vector* res = malloc(sizeof(vector));
    const int capacity = 8;
    res->size = 0;
    res->capacity = capacity;
    res->values = calloc(capacity, sizeof(double));
    return res;
}

void vector_destroy(vector* v) {
    free(v->values);
    free(v);
}

int vector_size(const vector* v) {
    return v->size;
}

double vector_get(const vector* v, int i) {
    assert (i < v->size);
    return v->values[i];
}

void vector_set(vector* v, int i, double x) {
    assert (i < v->size);
    v->values[i] = x;
}

void vector_add(vector* v, double x) {
    if (v->size == v->capacity) {
        const int new_cap = v->capacity * 2;
        double* new_values = calloc(new_cap, sizeof(double));
        for (int i = 0; i < v->size; ++i) {
            new_values[i] = v->values[i];
        }
        free(v->values);
        v->values = new_values;
        v->capacity = new_cap;
    }
    v->values[v->size] = x;
    ++v->size;
}

double vector_pop(vector* v) {
    assert (v->size != 0);
    v->values[v->size-1];
    --v->size;
}



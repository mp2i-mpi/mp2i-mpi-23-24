#include <stdio.h>
#include <stdbool.h>

int main() {
    int multiple = 7;
    for (int i = 0; i < 10; ++i) {
        printf("%d x %d = %d\n", multiple, i, multiple*i);
    }
    return 0;
}

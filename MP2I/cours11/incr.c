#include <stdio.h>
#include <stdbool.h>

void print_int(int x) {
    printf("Entier : %d\n", x);
}

int main() {
    int x = 0;
    print_int(x);
    int y = x++;
    print_int(x);
    print_int(y);

    int z = ++x;
    print_int(x);
    print_int(z);

    return 0;
}

#include <stdio.h>
#include <stdbool.h>

int abs(int x) {
    if (x < 0) {
        x = -x;
    }
    return x;
}

int main() {
    for (int x = -10; x <= 10; ++x) {
        printf("|x| = %d\n", abs(x));
    }
    return 0;
}

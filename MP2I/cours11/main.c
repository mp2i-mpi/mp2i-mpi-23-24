#include <stdio.h>
#include <stdbool.h>

int main() {
    int age = 18;
    if (age > 18) {
        printf("Tu es majeur\n");
        printf("Félicitations !\n");
    } else if (age == 18) {
        printf("Tu viens juste d'atteindre la majorité !\n");
    } else {
        printf("Tu es encore mineur\n");
    }
    return 0;
}

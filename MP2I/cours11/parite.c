#include <stdio.h>
#include <stdbool.h>

int est_impair(int x);

int est_pair(int x) {
    if (x == 0) {
        return true; 
    }
    return est_impair(x-1);
}

int est_impair(int x) {
    if (x == 0) {
        return false;
    }
    return est_pair(x-1);
}

int main() {
    if (est_pair(12)) {
        printf("12 est pair\n");
    }
    return 0;
}

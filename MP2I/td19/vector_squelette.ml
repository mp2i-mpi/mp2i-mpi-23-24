(* Représente un tableau redimensionnable *)
type 'a vector

(* Permet de créer un tableau redimensionnable vide (de capacité initiale 8) *)
let create () : 'a vector =
  failwith "TODO"

(* Renvoie l'élément à l'indice i, ou une erreur Failure si l'indice est invalide *)
let get (v: 'a vector) (i: int) : 'a =
  failwith "TODO"

(* Modifie l'élément à l'indice i, ou une erreur Failure si l'indice est invalide *)
let put (v: 'a vector) (i: int) (x: 'a) : unit =
  failwith "TODO"

(* Ajoute un élément à la fin du tableau *)
let push (v: 'a vector) (x: 'a) : unit =
  failwith "TODO"

(* Renvoie la taille du tableau *)
let length (v: 'a vector) : int =
  failwith "TODO"

(* Renvoie la capacité du tableau *)
let capacity (v: 'a vector) : int =
  failwith "TODO"

(* Renvoie un tableau classique contenant les mêmes éléments que le tableau redimensionnable *)
let to_array (v: 'a vector) : 'a array =
  failwith "TODO"

let tests () =
  let v = create () in
  push v 1;
  push v 12;
  push v 5;
  assert (get v 0 = 1);
  assert (get v 1 = 12);
  assert (get v 2 = 5);
  assert (length v = 3);
  push v 1;
  push v 2;
  push v 3;
  push v 4;
  push v 5;
  push v 6;
  assert (get v 8 = 6)

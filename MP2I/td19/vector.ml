(* Représente un tableau redimensionnable *)
type 'a vector = {
  mutable values: 'a option array;
  mutable size: int
}

(* Permet de créer un tableau redimensionnable vide (de capacité initiale 8) *)
let create () : 'a vector = {
  values = Array.make 8 None;
  size = 0
}

(* Renvoie l'élément à l'indice i, ou une erreur Failure si l'indice est invalide *)
let get (v: 'a vector) (i: int) : 'a =
  if i >= v.size then
    failwith "indice invalide"
  else
    Option.get v.values.(i)

(* Modifie l'élément à l'indice i, ou une erreur Failure si l'indice est invalide *)
let put (v: 'a vector) (i: int) (x: 'a) : unit =
  if i >= v.size then
    failwith "indice invalide"
  else
    v.values.(i) <- Some x

let copy_begin src dst =
  for i = 0 to Array.length src do
    dst.(i) <- src.(i)
  done

let resize (v: 'a vector) (new_size: int) =
  assert (new_size > v.size);
  let new_values = Array.make new_size None in
  copy_begin v.values new_values;
  v.values <- new_values

(* Ajoute un élément à la fin du tableau *)
let push (v: 'a vector) (x: 'a) : unit =
  let cap = Array.length v.values in
  if v.size = cap then (
    resize v (2*cap)
  );
  let i = v.size in
  v.size <- v.size + 1;
  put v i x

  

(* Renvoie la taille du tableau *)
let length (v: 'a vector) : int =
  v.size

(* Renvoie la capacité du tableau *)
let capacity (v: 'a vector) : int =
  Array.length v.values

(* Renvoie un tableau classique contenant les mêmes éléments que le tableau redimensionnable *)
let to_array (v: 'a vector) : 'a array =
  Array.init v.size (fun i -> Option.get (v.values.(i)))

let tests () =
  let v = create () in
  push v 1;
  push v 12;
  push v 5;
  assert (get v 0 = 1);
  assert (get v 1 = 12);
  assert (get v 2 = 5);
  assert (length v = 3);
  push v 1;
  push v 2;
  push v 3;
  push v 4;
  push v 5;
  push v 6;
  assert (get v 8 = 6)

(* Représente un tableau redimensionnable *)
type 'a vector

(* Permet de créer un tableau redimensionnable vide (de capacité initiale 8) *)
val create: unit -> 'a vector

(* Renvoie l'élément à l'indice i, ou une erreur Failure si l'indice est invalide *)
val get: 'a vector -> int -> 'a

(* Modifie l'élément à l'indice i, ou une erreur Failure si l'indice est invalide *)
val put: 'a vector -> int -> 'a -> unit

(* Ajoute un élément à la fin du tableau *)
val push: 'a vector -> 'a -> unit

(* Renvoie la taille du tableau *)
val length: 'a vector -> int

(* Renvoie la capacité du tableau *)
val capacity: 'a vector -> int

(* Renvoie un tableau classique contenant les mêmes éléments que le tableau redimensionnable *)
val to_array: 'a vector -> 'a array

#include <stdlib.h>
#include <stdbool.h>

typedef struct LNode {
    int v;
    struct LNode *prec;
    struct LNode *next;
} lnode;

lnode *lnode_create(int v) {
    lnode *n = malloc(sizeof(lnode));
    n->v = v;
    n->prec = NULL;
    n->next = NULL;
    return n;
}

void lnode_destroy(lnode *n) {
    free(n);
}

typedef struct List {
    lnode *first;
    lnode *last;
} list;

list *list_create() {
    list *l = malloc(sizeof(list));
    l->first = NULL;
    l->last = NULL;
    return l;
}

void list_destroy(list *l) {
    lnode *n = l->first;
    while (n != NULL) {
        lnode *next = n->next;
        free(n);
        n = next;
    }
    free(l);
}

void list_push_back(list *l, int v) {
    lnode *n = lnode_create(v);
    if (l->last == NULL) {
        l->first = n;
        l->last = n;
    } else {
        l->last->next = n;
        n->prec = l->last;
        l->last = n;
    }
}

void list_push_front(list *l, int v) {
    lnode *n = lnode_create(v);
    if (l->first == NULL) {
        l->first = n;
        l->last = n;
    } else {
        l->first->prec = n;
        n->next = l->first;
        l->first = n;
    }
}

int list_back(list *l) {
    return l->last->v;
}

int list_front(list *l) {
    return l->first->v;
}

void list_pop_back(list *l) {
    lnode *n = l->last;
    l->last = n->prec;
    if (l->last != NULL) {
        l->last->next = NULL;
    } else {
        l->first = NULL;
    }
    free(n);
}

void list_pop_front(list *l) {
    lnode *n = l->first;
    l->first = n->next;
    if (l->first != NULL) {
        l->first->prec = NULL;
    } else {
        l->last = NULL;
    }
    free(n);
}

bool list_is_empty(list *l) {
    return l->first == NULL;
}

#include <stdio.h>
#include <stdbool.h>

void swap(int *a, int *b) {
    int tmp = *b;
    *b = *a;
    *a = tmp;
}

void swap_tab(int *a, int i, int j) {
    swap(a+i, a+j);
}

void two_way_sort(int a[], int n) {
    int i = 0;
    int j = n-1;
    while (i < j) {
        if (a[i] == 0) {
            ++i;
        } else if (a[j] == 1) {
            --j;
        } else {
            swap_tab(a, i, j);
            ++i;
            --j;
        }
    }
}

void dutch_flag(int a[], int n) {
    int end_z = 0;
    int i = 0;
    int begin_two = n;
    while (i < begin_two) {
        if (a[i] == 0) {
            swap_tab(a, end_z, i);
            ++end_z;
            ++i;
        } else if (a[i] == 1) {
            ++i;
        } else {
            --begin_two;
            swap_tab(a, begin_two, i);
        }
    }

}

int main() {
    const int n = 13;
    int a[] = {0, 1, 2, 1, 1, 2, 2, 0, 1, 2, 1, 0, 1, 0, 0, 1, 0};
    dutch_flag(a, n);
    for (int i = 0; i < n; ++i) {
        printf("%d ", a[i]);
    }
    printf("\n");

    return 0;
}

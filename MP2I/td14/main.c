#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

int binary_search(int x, int a[], int n) {
    int min = 0;
    int max = n;
    while (min < max) {
        int mean = (min+max)/2;
        if (a[mean] == x) {
            return mean;
        }
        if (a[mean] > x) {
            max = mean;
        } else {
            min = mean+1;
        }
    }
    return -1;
}

void test_binary_search() {
    int a[] = { -12, -5, 3, 4, 5, 8, 9, 12, 25 };
    const int n = 9;
    assert (binary_search(3, a, n) == 2);
    assert (binary_search(-12, a, n) == 0);
    assert (binary_search(25, a, n) == 8);
    assert (binary_search(7, a, n) == -1);
    assert (binary_search(-15, a, n) == -1);
    assert (binary_search(28, a, n) == -1);
}

void swap(int *a, int *b) {
    int tmp = *b;
    *b = *a;
    *a = tmp;
}

void swap_tab(int *a, int i, int j) {
    swap(a+i, a+j);
}

void quicksort_rec(int a[], int b, int e) {
    if (b >= e-1) {
        return;
    }
    int p = a[b];
    int l = b;
    int h = e;
    int i = l+1;
    print_tab(a+b, e-b);
    while (i < h) {
        if (a[i] < p) {
            swap_tab(a, i, l);
            ++i;
            ++l;
        } else if (a[i] == p) {
            ++i;
        } else {
            --h;
            swap_tab(a, i, h);
        }
    }
    print_tab(a+b, e-b);
    printf("\n");
    quicksort_rec(a, b, l);
    quicksort_rec(a, h, e);
}

void knuth_shuffe(int a[], int n) {
    for (int i = 1; i < n; i++) {
        swap_tab(a, i, rand() % (i+1));
    }
}

void print_tab(int a[], int n) {
    for (int i = 0; i < n; ++i) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

void try_knuth_shuffle() {
    int a[] = { -12, 23, 2, -15, -28, 3, 3, 3, 5, 9, 2, 12, 13, 12, 4, 8, 1 };
    const int n = 17;
    const int tries = 10;
    for (int k = 0; k < tries; ++k) {
        knuth_shuffe(a, n);
        print_tab(a, n);
    }
}

void autre_fonctyion() {
    assert(false);
}

void quicksort(int a[], int n) {
    // dqnnkldnskqldsq$
    autre_fonctyion();
}

void test_quicksort() {
    int a[] = { -12, 23, 2, -15, -28, 3, 3, 3, 5, 9, 2, 12, 13, 12, 4, 8, 1 };
    int a_sorted[] = { -28, -15, -12, 1, 2, 2, 3, 3, 3, 4, 5, 8, 9, 12, 12, 13, 23 };
    const int n = 17;
    quicksort(a, n);
    for (int i = 0; i < n; ++i) {
        assert (a[i] == a_sorted[i]);
    }
}

// void quicksort(int a[], int n) {
//     knuth_shuffe(a, n);
//     quicksort_rec(a, 0, n);
// }

// void test_quicksort() {
//     int a[] = { -12, 23, 2, -15, -28, 3, 3, 3, 5, 9, 2, 12, 13, 12, 4, 8, 1 };
//     int a_sorted[] = { -28, -15, -12, 1, 2, 2, 3, 3, 3, 4, 5, 8, 9, 12, 12, 13, 23 };
//     const int n = 17;
//     quicksort(a, n);
//     for (int i = 0; i < n; ++i) {
//         assert (a[i] == a_sorted[i]);
//     }
// }

int main() {
    srand(time(NULL));

    // try_knuth_shuffle();
    test_binary_search();
    test_quicksort();
    return 0;
}

module Digraph = struct
  type digraph = int list array
  type t = digraph

  let create (n: int) : digraph = Array.make n []
  let size (g: digraph) : int = Array.length g

  let has_edge (g: digraph) (x: int) (y: int) : bool = 
    List.mem y g.(x)

  let add_edge (g: digraph) (x: int) (y: int) : unit =
    if not (has_edge g x y) then
      g.(x) <- y :: g.(x)

  let add_sym_edge (g: digraph) (x: int) (y: int) : unit =
    add_edge g x y;
    add_edge g y x

  let succ (g: digraph) (x: int) : int list = g.(x)

  let edges (g: digraph) : (int * int) list =
    let res = ref [] in
    for i = 0 to (size g) - 1 do
      let aux j =
        res := (i, j) :: !res
      in
      List.iter aux (succ g i)
    done;
    !res
end

module Graph = struct
  type graph = Digraph.digraph
  type t = graph

  let create = Digraph.create
  let size = Digraph.size

  let add_edge (g: graph) (x: int) (y: int) : unit =
    Digraph.add_edge g x y;
    Digraph.add_edge g y x

  let has_edge = Digraph.has_edge
  let succ = Digraph.succ

  let edges = Digraph.edges
end

let min_color (l: int list) : int =
  let n = List.length l in
  let free = Array.make (n + 1) true in
  List.iter (fun v -> if v <= n then free.(v) <- false) l;
  let rec find i = if free.(i) then i else find (i + 1) in
  find 0

let coloration_glouton (g: Graph.t) (order: int list) : int array =
  let color = Array.make (Graph.size g) 0 in
  let assign v =
    let colors = List.map (fun w -> color.(w)) (Graph.succ g v) in
    color.(v) <- min_color colors in
  List.iter assign order;
  color

let () =
  let g = Graph.create 6 in
  Graph.add_edge g 0 1;
  Graph.add_edge g 0 2;
  Graph.add_edge g 0 3;
  Graph.add_edge g 1 2;
  Graph.add_edge g 1 3;
  Graph.add_edge g 2 3;
  Graph.add_edge g 3 4;
  Graph.add_edge g 3 5;
  Graph.add_edge g 4 5;
  let order = [0; 1; 2; 3; 4; 5] in
  let color = coloration_glouton g order in
  Array.iteri (fun i c -> Printf.printf "color[%d] = %d\n" i c) color

type pyramide = int array array

let max_somme_chemin (p: pyramide) : int =
  let rec s i j =
    if i = Array.length p then
      0
    else
      p.(i).(j) + max (s (i+1) j) (s (i+1) (j+1))
  in
  s 0 0

let max_somme_chemin_memo (p: pyramide) : int =
  let memo = Hashtbl.create 8 in
  let rec s i j =
    if i = Array.length p then
      0
    else (
      try
        Hashtbl.find memo (i, j)
      with Not_found -> (
        let res = p.(i).(j) + max (s (i+1) j) (s (i+1) (j+1)) in
        Hashtbl.add memo (i, j) res;
        res
      )
    )
  in
  s 0 0

let test_max_somme_chemin () =
  let p = [|
    [| 3 |];
    [| 1; 4 |];
    [| 1; 5; 9 |];
    [| 2; 6; 5; 3 |];
  |] in
  assert (max_somme_chemin p = 21);
  Printf.printf "OK\n"

let test_max_somme_chemin_memo () =
  let p = [|
    [| 3 |];
    [| 1; 4 |];
    [| 1; 5; 9 |];
    [| 2; 6; 5; 3 |];
  |] in
  assert (max_somme_chemin_memo p = 21);
  Printf.printf "OK\n"

let () =
  test_max_somme_chemin ();
  test_max_somme_chemin_memo ();
  Stdlib.flush stdout;
  Random.self_init ();
  let big_pyr = Array.init 1000
    (fun i -> 
      Array.init (i+1) (fun j -> (Random.int 20) - 10)) 
  in
  Printf.printf "big_pyr: %d\n" (max_somme_chemin_memo big_pyr)


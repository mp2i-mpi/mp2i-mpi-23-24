(* let rec fibo n =
  if n = 0 then 0
  else if n = 1 then 1
  else fibo (n - 1) + fibo (n - 2) *)

let fibo n =
  let res = Array.make (n + 1) 0 in
  res.(0) <- 0;
  res.(1) <- 1;
  for i = 2 to n do
    res.(i) <- res.(i - 1) + res.(i - 2)
  done;
  res.(n)

let () =
  let n = read_int () in
  print_int (fibo n);
  print_newline ()

let rec split_aux l1 l2 l =
  match l with
  | [] -> (l1, l2)
  | [x] -> (x::l1, l2)
  | t1::t2::q -> split_aux (t1::l1) (t2::l2) q

let split = split_aux [] []

let rec merge_aux res l1 l2 =
  match l1, l2 with
  | [], [] -> res
  | t::q, [] | [], t::q -> merge_aux (t::res) q []
  | t1::q1, t2::q2 -> (
    if t1 < t2 then
      merge_aux (t1::res) q1 l2
    else
      merge_aux (t2::res) l1 q2
  )

let merge l1 l2 = List.rev (merge_aux [] l1 l2)

let rec merge_sort (l: 'a list) : 'a list =
  match l with
  | [] -> []
  | [x] -> [x]
  | _ -> (
    let l1, l2 = split l in
    let s1 = merge_sort l1 in
    let s2 = merge_sort l2 in
    merge s1 s2
  )

let test_merge_sort () =
  assert (merge_sort [3; 2; 1] = [1; 2; 3]);
  assert (merge_sort [3; 1; 2] = [1; 2; 3]);
  assert (merge_sort [1; 2; 3] = [1; 2; 3]);
  assert (merge_sort [] = []);
  assert (merge_sort [1] = [1]);
  assert (merge_sort [1; 1] = [1; 1]);
  assert (merge_sort [1; 7; -5; 12; 8] = [-5; 1; 7; 8; 12])

let () =
  test_merge_sort ();
  print_endline "merge_sort OK"
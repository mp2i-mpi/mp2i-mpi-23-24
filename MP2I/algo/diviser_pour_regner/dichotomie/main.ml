let rec dicho_aux (t: 'a array) (v: 'a) (b: int) (e: int) : bool =
  Printf.printf "dicho_aux %d %d\n" b e;
  if b = e then false
  else (
    let m = (b + e) / 2 in
    if t.(m) = v then true
    else if t.(m) < v then
      dicho_aux t v (m + 1) e
    else
      dicho_aux t v b m
  )

let dicho (t: 'a array) (v: 'a) : bool =
  dicho_aux t v 0 (Array.length t)

let () =
  let t = [|-12; -7; 0; 2; 4; 9; 11; 20; 24; 26; 28; 30; 31; 32; 37; 45; 48; 100|] in
  if dicho t 0 then
    Printf.printf "0 trouvé\n"
  else
    Printf.printf "0 pas trouvé\n";

  if dicho t 7 then
    Printf.printf "7 trouvé\n"
  else
    Printf.printf "7 pas trouvé\n";
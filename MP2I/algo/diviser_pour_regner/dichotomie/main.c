#include <stdbool.h>

bool dicho(int *t, int n, int v) {
    int b = 0;
    int e = n;
    while (b < e) {
        int m = (b+e) / 2;
        if (t[n] < v) {
            b = m+1;
        } else {
            e = m;
        }
    }
    return false;
}

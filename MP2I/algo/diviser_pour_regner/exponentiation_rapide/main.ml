let rec int_pow_aux (a: int) (n: int) (acc: int) : int =
  if n = 0 then acc
  else (
    if n mod 2 = 0 then int_pow_aux (a * a) (n / 2) acc
    else int_pow_aux a (n - 1) (acc * a)
  )

let int_pow (a: int) (n: int) : int =
  int_pow_aux a n 1
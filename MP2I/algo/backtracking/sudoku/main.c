#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

int col(int i) {
  return i % 9;
}

int row(int i) {
  return i / 9;
}

int gr(int i) {
  int col_group = col(i) / 3;
  int row_group = row(i) / 3;
  return row_group * 3 + col_group;
}

void test_col() {
  assert(col(0) == 0);
  assert(col(7) == 7);
  assert(col(11) == 2);
  assert(col(80) == 8);
}

void test_row() {
  assert(row(0) == 0);
  assert(row(11) == 1);
  assert(row(72) == 8);
  assert(row(80) == 8);
}

void test_gr() {
  assert(gr(0) == 0);
  assert(gr(11) == 0);
  assert(gr(72) == 6);
  assert(gr(80) == 8);
}

bool same_zone(int c1, int c2) {
  return col(c1) == col(c2)
      || row(c1) == row(c2)
      || gr(c1) == gr(c2);
}

bool check_position(int g[81], int p) {
  for (int c = 0; c < 81; ++c) {
    if (c != p && same_zone(c, p) && g[p] == g[c]) {
      return false;
    }
  }
  return true;
}

bool solve(int g[81]) {
  for (int c = 0; c < 81; c++) {
    if (g[c] == 0) {
      for (int v = 1; v <= 9; v++) {
        g[c] = v;
        if (check_position(g, c) && solve(g)) {
          return true;
        }
      }
      g[c] = 0;
      return false;
    }
  }
  return true;
}

void print_line_sep() {
  for (int i = 0; i < 30; ++i) {
    printf("-");
  }
}

void print_grid(int g[81]) {
  print_line_sep();
  printf("\n");
  for (int gr = 0; gr < 3; ++gr) {
    for(int r_offset = 0; r_offset < 3; ++r_offset) {
      printf("|");
      for (int gc = 0; gc < 3; ++gc) {
        for (int c_offset = 0; c_offset < 3; ++c_offset) {
          int i = (gr*3 + r_offset) * 9 + (gc * 3 + c_offset);
          if (g[i] == 0) {
            printf("   ");
          } else {
            printf(" %d ", g[i]);
          }
        }
        printf("|");
      }
      printf("\n");
    }
    
    print_line_sep();
    printf("\n");
  }
}

int main() {
    test_col();
    test_row();
    test_gr();
    printf("OK\n");

    int g[81] = { 0 };

    g[0] = 2;
    g[7] = 6;

    g[13] = 7;
    g[14] = 5;
    g[16] = 3;

    g[19] = 4;
    g[20] = 8;
    g[22] = 9;
    g[24] = 1;

    g[30] = 3;
    
    g[36] = 3;
    g[40] = 1;
    g[44] = 9;

    g[50] = 8;

    g[56] = 1;
    g[58] = 2;
    g[60] = 5;
    g[61] = 7;

    g[64] = 8;
    g[66] = 7;
    g[67] = 3;

    g[73] = 9;
    g[80] = 4;


    print_grid(g);

    solve(g);

    print_grid(g);

    return 0;
}

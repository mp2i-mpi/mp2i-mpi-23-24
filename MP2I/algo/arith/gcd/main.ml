let rec gcd u v =
  if v = 0 then u else gcd v (u mod v)

let test_gcd () =
  assert (gcd 0 5 = 5);
  assert (gcd 3 7 = 1);
  assert (gcd 12 15 = 3)

let () =
  test_gcd ();
  print_endline "OK"
  
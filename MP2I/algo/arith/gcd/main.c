#include <stdio.h>
#include <assert.h>

int gcd(int u, int v) {
  while (v != 0) {
    int tmp = v;
    v = u % v;
    u = tmp;
  }
  return u;
}

void test_gcd() {
  assert (gcd(0, 5) == 5);
  assert (gcd(3, 7) == 1);
  assert (gcd(12, 15) == 3);
}


int main() {
  test_gcd();
  printf("OK\n");

  return 0;
}
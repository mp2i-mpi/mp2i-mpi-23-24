#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool *crible(int max) {
  bool *premier = calloc(max+1, sizeof(bool));
  premier[0] = false;
  premier[1] = false;
  for (int i = 2; i <= max; ++i) {
    premier[i] = true;
  }
  for (int n = 2; n*n <= max; ++n) {
    if (premier[n]) {
      for (int m = n*n; m <= max; m+=n) {
        premier[m] = false;
      }
    }
  }
  return premier;
}

int main() {
  const int max = 100;
  bool *c = crible(max);
  
  for (int i = 0; i <= max; ++i) {
    if (c[i]) {
      printf ("%d ", i);
    }
  }
  printf("\n");

  free(c);
}

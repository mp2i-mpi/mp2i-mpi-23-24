#include <stdio.h>

int int_pow(int x, int n) {
    int y = 1;
    while (n > 0) {
        if (n % 2 == 1) {
            y = (y*x);
        }
        x = (x*x);
        n = n / 2;
    }
    return y;
}

int pow_mod(int x, int n, int m) {
    int y = 1;
    while (n > 0) {
        if (n % 2 == 1) {
            y = (y*x) % m;
        }
        x = (x*x) % m;
        n = n / 2;
    }
    return y;
}

int main() {
    printf("10^2500 = %d\n", int_pow(10, 2500));
    printf("10^2500 % 7 = %d\n", int_pow(10, 2500) % 7);
    printf("pow_mod(10, 2500, 7) = %d\n", pow_mod(10, 2500, 7));

    return 0;
}
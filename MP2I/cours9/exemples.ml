(* let acc = ref 0
let sum n =
  for i = 1 to n do
    acc := !acc + i;
    Printf.printf "%d " !acc
  done

let rec sum_aux n k acc =
  if k <= n+1 then (
    Printf.printf "%d " acc;
    sum_aux n (k+1) (acc+k)
  )

let () = sum_aux 10  2 1 *)
(* 
let acc = ref 0
let k = ref 0
let () =
  while !acc < 100 do
    Printf.printf "%d " !acc;
    k := !k+1;
    acc := !acc + !k
  done *)
(* 
let () =
  print_endline "avant";
  let n = 3/0 in
  print_endline "après";
  print_int n;
  print_newline()

let fonction_erreur x =
  raise (Invalid_argument "argument incorrect")

let failwith s =
  raise (Failure s)

let rec max l = match l with
  | [] -> raise (Invalid_argument "liste vide")
  | [t] -> t
   *)

(* let () =
  print_string "Ton âge : ";
  let age =
    try
      Some (read_int ())
    with
    | Failure s when s = "int_of_string" -> None
  in match age with
  | None -> print_endline "Tape ton âge correctement"
  | Some n ->   if n < 18 then
                print_endline "Tu es mmineur"
              else
                print_endline "Tu es majeur" *)

let () =
  let f = open_in "nouvelle.txt" in
  let rec loop () =
    try
      let line = input_line f in
      print_endline line;
      loop()
    with
    | End_of_file -> print_endline "Finito"
  in loop ()


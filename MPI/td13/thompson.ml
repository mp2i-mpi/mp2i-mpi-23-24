module Auto = struct
  (* Automates finis, déterministes ou non

    Les états sont 0,1,...,n-1.
    Les caractères sont les 256 caractères ASCII.
    L'indice 256 représente une transition spontanée.
    L'état initial est toujours 0.
  *)

  type state = int

  type auto = {
    trans : ((state list) array) array; (* état -> caractères -> liste d'états *)
    final : bool array;
  }

  type t = auto

  let create n =
    let auto = {
      trans = Array.make_matrix n 257 [];
      final = Array.make n false 
    } in
    auto


  let size a = Array.length a.final

  let is_det_trans pc_trans = match pc_trans with
    | [] | [ _ ] -> true
    | _ -> false
  let is_det auto =
    Array.for_all (fun p_trans -> Array.for_all is_det_trans p_trans) auto.trans

  let has_epsilon a = Array.exists (fun p_trans -> p_trans.(256) <> []) a.trans
    
  let trans a p c = a.trans.(p).(Char.code c)

  let eps_trans a q = a.trans.(q).(256)

  (* Ne fais rien si déjà dedans *)
  let rec insert_sorted_list_aux l x res =
    match l with
    | [] -> List.rev (x::res)
    | t::q when t < x -> insert_sorted_list_aux q x (t::res)
    | t::q when t = x -> (List.rev res) @ t::q
    | t::q -> (List.rev res) @ (x::t::q)
  let insert_sorted_list l x = insert_sorted_list_aux l x []

  let add_trans_aux a p idx q =
    a.trans.(p).(idx) <- insert_sorted_list (a.trans.(p).(idx)) q

  let add_trans a p c q = add_trans_aux a p (Char.code c) q

  let add_eps_trans a p q =
    assert (p <> q);
    add_trans_aux a p 256 q

  let add_trans_opt a p c q =
    match c with
    | None -> add_eps_trans a p q
    | Some c -> add_trans a p c q

  let copy a = {
      trans = Array.map (fun p_trans -> Array.copy p_trans) a.trans;
      final = Array.copy a.final
    }


  let all_trans a q =
    let acc_trs = ref [] in
    Array.iteri
      (fun ch trs ->
        if trs <> [] && ch <= 255 then acc_trs := (Char.chr ch, trs) :: !acc_trs)
      a.trans.(q);
    (List.rev !acc_trs, (eps_trans a q))

  let all_trans_opt a q =
    let char_trans, e_trans = all_trans a q in
    let assign_char opt_c states = List.map (fun s -> (opt_c, s)) states in
    let char_trans = List.flatten (
      List.map (fun (c, states) -> assign_char (Some c) states)
    char_trans) in
    char_trans @ (assign_char None e_trans)

  let set_final a q = a.final.(q) <- true
  let unset_final a q = a.final.(q) <- false
  let is_final a q = a.final.(q)

  (* accept *)

  let rec decode_string s = List.of_seq (String.to_seq s)

  (* Méthode backtracking *)

  (* let rec check_final a q =
    is_final a q || List.exists (fun q2 -> check_final a q2) (eps_trans a q)

  let rec accept_aux (a: auto) (w: char list) (q_init: state) =
    match w with
    | [] -> check_final a q_init
    | h::tail -> (
                let potential_trans = trans a q_init h in
                List.exists (fun q -> accept_aux a tail q) potential_trans
                || List.exists (fun q -> accept_aux a w q) (eps_trans a q_init)
              )


  let accept (a: auto) (w: string) = accept_aux a (decode_string w) 0 *)

  (* Meilleure méthode *)

  let rec merge_sorted_list_aux l1 l2 res =
    match l1, l2 with
    | [], [] -> res
    | [], t::q -> merge_sorted_list_aux [] q (t::res)
    | _, [] -> merge_sorted_list_aux [] l1 res
    | t1::q1, t2::q2 -> (
      if (t1 = t2) then
        merge_sorted_list_aux q1 q2 (t1::res)
      else if t1 < t2 then
        merge_sorted_list_aux q1 l2 (t1::res)
      else
        merge_sorted_list_aux l1 q2 (t2::res)
    )
  let merge_sorted_list l1 l2 = List.rev (merge_sorted_list_aux l1 l2 [])

  let rec eps_depth_search a q : state list =
    close_eps a (eps_trans a q)

  and close_eps a s =
    let size = List.length s in
    let new_s = List.fold_left merge_sorted_list s (List.map (eps_trans a) s) in
    if size = List.length new_s then
      s
    else close_eps a new_s
    

  let rec accept_aux a w s =
    match w with
    | [] -> List.exists (is_final a) s
    | h::tail -> (
      Printf.printf "%c : { " h;
      List.iter (Printf.printf "%d ") s;
      print_string " }";
      print_newline();
      let new_s = List.fold_left merge_sorted_list [] (List.map (fun q -> trans a q h) s) in
      accept_aux a tail (close_eps a new_s)
    )


  let accept a w =
    Printf.printf "==============\n%s\n------------\n" w;
    accept_aux a (decode_string w) (close_eps a [0])

  (* print *)

  let print fmt a =
    Format.fprintf fmt "%d@\n" (size a);
    let e =
      Array.fold_left (Array.fold_left (fun e l -> e + List.length l)) 0 a.trans
    in
    Format.fprintf fmt "%d@\n" e;
    for i = 0 to size a - 1 do
      let trans i c l =
        List.iter
          (fun j ->
            Format.fprintf fmt "%d '%s' %d@\n" i
              (if c < 256 then String.make 1 (Char.chr c) else "ε")
              j)
          l
      in
      Array.iteri (trans i) a.trans.(i)
    done;
    for i = 0 to size a - 1 do
      if is_final a i then Format.fprintf fmt "final %d@\n" i
    done

  let print_dot fmt a =
    Format.fprintf fmt
      "digraph auto {\n\
      \  fontname=\"Helvetica,Arial,sans-serif\"\n\
      \  node [fontname=\"Helvetica,Arial,sans-serif\"]\n\
      \  edge [fontname=\"Helvetica,Arial,sans-serif\"]\n\
      \  rankdir=LR;\n\
      \  node [shape = doublecircle];";
    for i = 0 to size a - 1 do
      if is_final a i then Format.fprintf fmt " %d" i
    done;
    Format.fprintf fmt ";\n  node [shape = circle];@\n";
    let trans i n j =
      let c = Char.chr n in
      let s =
        if (65 <= n && n <= 90) || (97 <= n && n <= 122) then String.make 1 c
        else string_of_int n
      in
      Format.fprintf fmt "  %d -> %d [label = \"%s\"];@\n" i j s
    in
    let trans i c l = List.iter (trans i c) l in
    let trans i = Array.iteri (trans i) in
    Array.iteri trans a.trans;
    Format.fprintf fmt "}@\n"
    
  let print_dot_to_file ~file a =
    let c = open_out file in
    let fmt = Format.formatter_of_out_channel c in
    Format.fprintf fmt "%a@." print_dot a;
    close_out c


  (* Internal tests *)

  let test_insert_sorted_list () =
    assert (insert_sorted_list [] 3 = [3]);
    assert (insert_sorted_list [1; 3; 7] 4 = [1; 3; 4; 7]);
    assert (insert_sorted_list [1; 3; 7] 8 = [1; 3; 7; 8]);
    assert (insert_sorted_list [1; 3; 7] 3 = [1; 3; 7]);
    assert (insert_sorted_list [1; 3; 7] 7 = [1; 3; 7])

  let test_merge_sorted_list () =
    assert (merge_sorted_list [] [] = []);
    assert (merge_sorted_list [1; 3; 5] [] = [1; 3; 5]);
    assert (merge_sorted_list [] [1; 3; 5] = [1; 3; 5]);
    assert (merge_sorted_list [1; 3; 5] [7; 8; 9] = [1; 3; 5; 7; 8; 9]);
    assert (merge_sorted_list [1; 3; 5] [1; 2; 3; 4; 7; 8; 9] = [1; 2; 3; 4; 5; 7; 8; 9]);
end

module Re = struct
  type re =
    | Empty
    | Epsilon
    | Char of char
    | Alt of re * re
    | Concat of re * re
    | Star of re

  let rec has_epsilon re = match re with
    | Empty -> false
    | Epsilon -> true
    | Char _ -> false
    | Alt (lhs, rhs) -> has_epsilon lhs || has_epsilon rhs
    | Concat (lhs, rhs) -> has_epsilon lhs && has_epsilon rhs
    | Star _ -> true
end

let transfer_trans (offset: int) (dest: Auto.t) (p : Auto.state) (c: char option) (q: Auto.state) =
  match c with
  | None -> Auto.add_eps_trans dest (p+offset) (q+offset)
  | Some ch -> Auto.add_trans dest (p+offset) ch (q+offset)

let transfer_trans_adapter (offset: int) (dest: Auto.t) (q : Auto.state) (trans: char option * Auto.state)  =
  let c, q_next = trans in
  transfer_trans offset dest q c q_next
  

let rec transfer (offset: int) (source: Auto.t) (dest: Auto.t) =
  assert ((Auto.size source) >= (Auto.size dest) + offset); 
  for q = 0 to ((Auto.size source) - 1) do
    List.iter (transfer_trans_adapter offset dest q) (Auto.all_trans_opt source q)
  done
  

let empty_thompson () =
  let res = Auto.create 2 in
  Auto.set_final res 1;
  res

let epsilon_thompson () =
  let res = empty_thompson () in
  Auto.add_eps_trans res 0 1;
  res

let char_thompson (c: char) =
  let res = empty_thompson() in
  Auto.add_trans res 0 c 1;
  res

let alt_thompson (t_lhs: Auto.t) (t_rhs: Auto.t) = Auto.create 1
let concat_thompson (t_lhs: Auto.t) (t_rhs: Auto.t) = Auto.create 1
let star_thompson (t0: Auto.t) = Auto.create 1


let rec thompson r =
  match r with
  | Re.Empty -> empty_thompson()
  | Re.Epsilon -> epsilon_thompson()
  | Re.Char c -> char_thompson c
  | Re.Alt (lhs, rhs) ->
      let t_lhs = thompson lhs in
      let t_rhs = thompson rhs in
      alt_thompson t_lhs t_rhs
  | Re.Concat (lhs, rhs) ->
      let t_lhs = thompson lhs in
      let t_rhs = thompson rhs in
      concat_thompson t_lhs t_rhs
  | Re.Star r0 -> star_thompson (thompson r0)


let test_transfer_trans () =
  let dest = Auto.create 8 in
  transfer_trans 3 dest 1 (Some 'a') 2;
  assert (Auto.trans dest 4 'a' = [5]);
  transfer_trans 4 dest 1 None 3;
  assert (Auto.eps_trans dest 5 = [7])

let test_transfer () =
  let source = Auto.create 3 in
  Auto.add_trans source 0 'a' 1;
  Auto.add_eps_trans source 1 2;
  let dest = Auto.create (2+3) in
  transfer 2 source dest;
  assert (Auto.trans dest 2 'a' = [3]);
  assert (Auto.eps_trans dest 3 = [4])


let tests = [
  ("test_transfer_trans", test_transfer_trans);
  ("test_transfer", test_transfer)
]

let run_tests() =
  List.iter (
    fun (name, test) -> (
      print_string name;
      test();
      print_endline " OK"
    )
  ) tests


let () =
  run_tests()


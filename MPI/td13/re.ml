type re =
  | Empty
  | Epsilon
  | Char of char
  | Alt of re * re
  | Concat of re * re
  | Star of re

let rec has_epsilon re = match re with
  | Empty -> false
  | Epsilon -> true
  | Char _ -> false
  | Alt (lhs, rhs) -> has_epsilon lhs || has_epsilon rhs
  | Concat (lhs, rhs) -> has_epsilon lhs && has_epsilon rhs
  | Star _ -> true
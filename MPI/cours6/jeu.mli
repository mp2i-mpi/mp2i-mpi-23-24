(* Représente un joueur *)
type player

(* Représente un état *)
type state

(* Renvoie le contrôleur d'un état *)
val player: state -> player

(* Renvoie la liste des coups possibles pour un état *)
val moves: state -> state list

(* 
    Cette fonction doit être appliquée uniquement sur des états finaux
    Elle renvoie le joueur vainqueur s'il y en a un, ou None
    s'il y a match nul
*)
val outcome: state -> player option

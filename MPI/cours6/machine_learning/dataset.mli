module type S = sig
  type t
  val load: (unit -> float) -> t
  val store: t -> (float -> unit) -> unit
end

module Make (M : S) : sig
  
  type t = M.t array
  val load : string -> t
  val store : t -> string -> unit
end


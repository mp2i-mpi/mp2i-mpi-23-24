type label = int
type data = label array

let shannon l =
  let num_1 = List.fold_left (+) 0 l in
  let total = List.length l in
  let num_0 = total - num_1 in
  let p_1 = float_of_int num_1 /. float_of_int total in
  let p_0 = float_of_int num_0 /. float_of_int total in
  -. p_1 *. log p_1 -. p_0 *. log p_0

let gain attr target =
  let combined = List.combine attr target in
  let g1 = List.filter (fun (a, t) -> a = 1) combined in
  let g0 = List.filter (fun (a, t) -> a = 0) combined in
  let h1 = shannon (List.map snd g1) in
  let h0 = shannon (List.map snd g0) in
  let h = shannon target in
  let p1 = float_of_int (List.length g1) /. float_of_int (List.length combined) in
  let p0 = float_of_int (List.length g0) /. float_of_int (List.length combined) in
  h -. p1 *. h1 -. p0 *. h0

type dataset = (data*label) list

type dectree =
  | Leaf of label
  | Node of int * dectree * dectree

let rec argmax l = failwith "unimplemented"

let rec build_tree_aux (dataset: dataset) (remaining_attrs_indices: label list) (prev_most_repr: label) =
  (* Cas de base *)
  if remaining_attrs_indices = [] then
    (* On a utilisé tous les attributs *)
    Leaf (argmax (List.map snd dataset))
  else if dataset = [] then (
    (* S vide i.e il n'y a plus de données *)
    Leaf (prev_most_repr)
  ) else (
    let l = snd (List.hd dataset) in
    if List.for_all (fun d -> snd d = l) dataset then
      Leaf l
    else (
      let get_attr i = (List.map (fun d -> (fst d).(i)) dataset) in
      let target = List.map snd dataset in
      let gains = List.map (fun attr_idx -> gain (get_attr attr_idx) target) remaining_attrs_indices in
      let best_attr = argmax gains in
      let filter_arg d = (fst d).(best_attr) = 1 in

      let g1 = List.filter filter_arg dataset in
      let g0 = List.filter (fun d -> not (filter_arg d)) dataset in

      let new_remaining_attrs_indices = List.filter (fun i -> i <> best_attr) remaining_attrs_indices in
      let most_repr = argmax (List.map snd dataset) in
      let st1 = build_tree_aux g1 new_remaining_attrs_indices most_repr in
      let st0 = build_tree_aux g0 new_remaining_attrs_indices most_repr in
      Node (best_attr, st1, st0)
    )
  )

  

let dataset = [
  [|1;1;1;1|], 1;
  [|1;1;1;0|], 0;
  [|1;1;0;1|], 1;
  [|1;1;0;0|], 0;
  [|1;0;1;1|], 1;
  [|1;0;1;0|], 0;
  [|1;0;0;1|], 0;
  [|1;0;0;0|], 0;
  [|0;0;1;1|], 0;
  [|0;0;1;0|], 0;
  [|0;0;0;1|], 1;
  [|0;0;0;0|], 0
]

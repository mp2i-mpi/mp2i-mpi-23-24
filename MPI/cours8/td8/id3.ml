type label = int
type data = label array

let shannon l =
  let num_1 = List.fold_left (+) 0 l in
  let total = List.length l in
  let num_0 = total - num_1 in
  let p_1 = float_of_int num_1 /. float_of_int total in
  let p_0 = float_of_int num_0 /. float_of_int total in
  -. p_1 *. log p_1 -. p_0 *. log p_0

let gain attr target =
  let combined = List.combine attr target in
  let g1 = List.filter (fun (a, t) -> a = 1) combined in
  let g0 = List.filter (fun (a, t) -> a = 0) combined in
  let h1 = shannon (List.map snd g1) in
  let h0 = shannon (List.map snd g0) in
  let h = shannon target in
  let p1 = float_of_int (List.length g1) /. float_of_int (List.length combined) in
  let p0 = float_of_int (List.length g0) /. float_of_int (List.length combined) in
  h -. p1 *. h1 -. p0 *. h0

type dataset = (data*label) list

type dectree =
  | Leaf of label
  | Node of int * dectree * dectree

let string_of_depth d = String.make (2*d) ' '
let rec print_tree_aux depth t = match t with
| Leaf (l) -> Printf.printf "%sLeaf(%d)" (string_of_depth depth) l
| Node (idx, l, r) -> (
    Printf.printf "%sNode(%d,\n" (string_of_depth depth) idx;
    print_tree_aux (depth+1) l;
    print_endline ",";
    print_tree_aux (depth+1) r;
    Printf.printf "%s\n)" (string_of_depth depth)
  )

let print_tree t =
  print_tree_aux 0 t;
  print_newline()

let rec mode l =
  let num_1 = List.fold_left (+) 0 l in
  let num_0 = List.length l - num_1 in
  if num_0 > num_1 then 0 else 1
let rec argmax_aux l cur res max = match l with
| [] -> res
| t::q -> (
    if t > max then
      argmax_aux q (cur+1) cur t
    else
      argmax_aux q (cur+1) res max
  )

let argmax l = match l with
| [] -> failwith "liste vide"
| t::q -> argmax_aux q 1 0 t

let rec build_tree_aux (dataset: dataset) remaining_attrs_indices (prev_mode: label) =
  (*Cas de base*)
  (* Il n'y a plus d'attributs pour discriminer *)
  if remaining_attrs_indices = [] then (
    Leaf (mode (List.map snd dataset))
  ) else (
    (* S (dataset) vide i.e il n'y a plus de données *)
    match dataset with
    | [] -> failwith "unimplemented"
    | t::_ -> (
      (* Tous les éléments dans S ont la même étiquette *)
      let l = snd (List.hd dataset) in
      if (List.for_all (fun x -> (snd x) = l) dataset) then (
        Leaf l
      ) else (
        let get_attr i = (List.map (fun d -> (fst d).(i)) dataset) in
        let target = List.map snd dataset in
        let gains = List.map (fun attr_idx -> gain (get_attr attr_idx) target) remaining_attrs_indices in
        let best_attr = argmax gains in
        let filter_arg d = (fst d).(best_attr) = 1 in

        let g1 = List.filter filter_arg dataset in
        let g0 = List.filter (fun d -> not (filter_arg d)) dataset in

        let new_remaining_attrs_indices = List.filter (fun i -> i <> best_attr) remaining_attrs_indices in

        let cur_mode = (mode (List.map snd dataset)) in
        let st1 = build_tree_aux g1 new_remaining_attrs_indices cur_mode in
        let st0 = build_tree_aux g0 new_remaining_attrs_indices cur_mode in
        Node (best_attr, st1, st0)
      )
    )
  )

let range n = List.init n (fun idx -> idx)

let build_tree (dataset : dataset) =
  let example = List.hd dataset in
  let nb_attributs = Array.length (fst example) in
  let remaining_attr_indices = range nb_attributs in
  let cur_mode = mode (List.map snd dataset) in
  build_tree_aux dataset remaining_attr_indices cur_mode

let dataset = [
  [|1;1;1;1|], 1;
  [|1;1;1;0|], 0;
  [|1;1;0;1|], 1;
  [|1;1;0;0|], 0;
  [|1;0;1;1|], 1;
  [|1;0;1;0|], 0;
  [|1;0;0;1|], 0;
  [|1;0;0;0|], 0;
  [|0;0;1;1|], 0;
  [|0;0;1;0|], 0;
  [|0;0;0;1|], 1;
  [|0;0;0;0|], 0
]

let dectree = build_tree dataset
let () = print_tree dectree

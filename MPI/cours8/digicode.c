#include <stdio.h>

// Code : 1419
int main() {
    int num_digits = 0;
    while (num_digits < 4) {
        int c = getchar();
        if (c == '\n') continue;
        if (c == '1' && num_digits == 0) num_digits = 1;
        else if (c=='4' && num_digits == 1) num_digits = 2;
        else if (c=='1' && num_digits == 2) num_digits = 3;
        else if (c=='9' && num_digits == 3) num_digits = 4;
        else if (c=='1') num_digits = 1;
        else num_digits = 0;
    }
    printf("Code correct\n");
    return 0;
}
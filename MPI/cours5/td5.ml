let mod_carre a m = (a*a) mod m

let rec mod_exp_aux acc a n m =
  if n = 0 then acc mod m else (
    if n mod 2 = 0 then
      mod_exp_aux acc (mod_carre a m) (n/2) m
    else
      mod_exp_aux ((a * acc) mod m) a (n-1) m
  )

let mod_exp = mod_exp_aux 1

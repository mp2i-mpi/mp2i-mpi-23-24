(* Automates finis, déterministes ou non

   Les états sont 0,1,...,n-1.
   Les caractères sont les 256 caractères ASCII.
   L'indice 256 représente une transition spontanée.
   L'état initial est toujours 0.
*)

type state = int

type auto = {
  trans : ((state list) array) array; (* état -> caractères -> liste d'états *)
  final : bool array;
}

let create n =
  let auto = {
    trans = Array.make_matrix n 257 [];
    final = Array.make n false 
  } in
  auto


let size a = Array.length a.final

let is_det_trans pc_trans = match pc_trans with
  | [] | [ _ ] -> true
  | _ -> false
let is_det auto =
  Array.for_all (fun p_trans -> Array.for_all is_det_trans p_trans) auto.trans

let has_epsilon a = Array.exists (fun p_trans -> p_trans.(256) <> []) a.trans
  
let trans a p c = a.trans.(p).(Char.code c)

let eps_trans a q = a.trans.(q).(256)

(* Ne fais rien si déjà dedans *)
let rec insert_sorted_list_aux l x res =
  match l with
  | [] -> List.rev (x::res)
  | t::q when t < x -> insert_sorted_list_aux q x (t::res)
  | t::q when t = x -> (List.rev res) @ t::q
  | t::q -> (List.rev res) @ (x::t::q)
let insert_sorted_list l x = insert_sorted_list_aux l x []

let add_trans_aux a p idx q =
  a.trans.(p).(idx) <- insert_sorted_list (a.trans.(p).(idx)) q

let add_trans a p c q = add_trans_aux a p (Char.code c) q

let add_eps_trans a p q =
  assert (p <> q);
  add_trans_aux a p 256 q

let add_trans_opt a p c q =
  match c with
  | None -> add_eps_trans a p q
  | Some c -> add_trans a p c q

let copy a = {
    trans = Array.map (fun p_trans -> Array.copy p_trans) a.trans;
    final = Array.copy a.final
  }


let all_trans a q =
  let acc_trs = ref [] in
  Array.iteri
    (fun ch trs ->
      if trs <> [] && ch <= 255 then acc_trs := (Char.chr ch, trs) :: !acc_trs)
    a.trans.(q);
  (List.rev !acc_trs, (eps_trans a q))

let all_trans_opt a q =
  let char_trans, e_trans = all_trans a q in
  let assign_char opt_c states = List.map (fun s -> (opt_c, s)) states in
  let char_trans = List.flatten (
    List.map (fun (c, states) -> assign_char (Some c) states)
  char_trans) in
  char_trans @ (assign_char None e_trans)

let set_final a q = a.final.(q) <- true
let unset_final a q = a.final.(q) <- false
let is_final a q = a.final.(q)

let accept (a: auto) (w: string) = false

let print fmt a =
  Format.fprintf fmt "%d@\n" (size a);
  let e =
    Array.fold_left (Array.fold_left (fun e l -> e + List.length l)) 0 a.trans
  in
  Format.fprintf fmt "%d@\n" e;
  for i = 0 to size a - 1 do
    let trans i c l =
      List.iter
        (fun j ->
          Format.fprintf fmt "%d '%s' %d@\n" i
            (if c < 256 then String.make 1 (Char.chr c) else "ε")
            j)
        l
    in
    Array.iteri (trans i) a.trans.(i)
  done;
  for i = 0 to size a - 1 do
    if is_final a i then Format.fprintf fmt "final %d@\n" i
  done

let print_dot fmt a =
  Format.fprintf fmt
    "digraph auto {\n\
    \  fontname=\"Helvetica,Arial,sans-serif\"\n\
    \  node [fontname=\"Helvetica,Arial,sans-serif\"]\n\
    \  edge [fontname=\"Helvetica,Arial,sans-serif\"]\n\
    \  rankdir=LR;\n\
    \  node [shape = doublecircle];";
  for i = 0 to size a - 1 do
    if is_final a i then Format.fprintf fmt " %d" i
  done;
  Format.fprintf fmt ";\n  node [shape = circle];@\n";
  let trans i n j =
    let c = Char.chr n in
    let s =
      if (65 <= n && n <= 90) || (97 <= n && n <= 122) then String.make 1 c
      else string_of_int n
    in
    Format.fprintf fmt "  %d -> %d [label = \"%s\"];@\n" i j s
  in
  let trans i c l = List.iter (trans i c) l in
  let trans i = Array.iteri (trans i) in
  Array.iteri trans a.trans;
  Format.fprintf fmt "}@\n"
  
let print_dot_to_file ~file a =
  let c = open_out file in
  let fmt = Format.formatter_of_out_channel c in
  Format.fprintf fmt "%a@." print_dot a;
  close_out c


(* Internal tests *)

let test_insert_sorted_list () =
  assert (insert_sorted_list [] 3 = [3]);
  assert (insert_sorted_list [1; 3; 7] 4 = [1; 3; 4; 7]);
  assert (insert_sorted_list [1; 3; 7] 8 = [1; 3; 7; 8]);
  assert (insert_sorted_list [1; 3; 7] 3 = [1; 3; 7]);
  assert (insert_sorted_list [1; 3; 7] 7 = [1; 3; 7])

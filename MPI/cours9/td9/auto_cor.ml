(* Automates finis, déterministes ou non

   Les états sont 0,1,...,n-1.
   Les caractères sont les 256 caractères ASCII.
   L'indice 256 représente une transition spontannée.
   L'état initial est toujours 0.
*)

type state = int

type auto = {
  trans : ((state list) array) array; (* état -> caractères -> liste d'états *)
  final : bool array;
}

let create n =
  {
    trans = Array.init n (fun _ -> Array.make 257 []);
    final = Array.make n false;
  }

let copy auto =
  {
    trans = Array.map (fun a -> Array.copy a) auto.trans;
    final = Array.copy auto.final;
  }

let has_epsilon a = Array.exists (fun t -> t.(256) <> []) a.trans
let trans a q c = a.trans.(q).(Char.code c)

let all_trans a q =
  let acc_trs = ref [] in
  Array.iteri
    (fun ch trs ->
      if trs <> [] && ch <= 255 then acc_trs := (Char.chr ch, trs) :: !acc_trs)
    a.trans.(q);
  (!acc_trs, a.trans.(q).(256))

let eps_trans a q = a.trans.(q).(256)

let all_trans_opt a q =
  let trs, etrs = all_trans a q in
  List.fold_left
    (fun acc (ch, states) ->
      List.fold_left (fun acc s -> (Some ch, s) :: acc) acc states)
    (List.fold_left (fun acc s -> (None, s) :: acc) [] etrs)
    trs

let rec insert (q : state) l =
    match l with
     | [] -> [ q ]
     | p::ll -> if q < p then q :: l
     else if q = p then l
     else p :: insert q ll

let add_trans_aux a q1 i q2 =
  let t = a.trans.(q1) in
  t.(i) <- insert q2 t.(i)

let add_trans a q1 c q2 = add_trans_aux a q1 (Char.code c) q2

let add_eps_trans a q1 q2 =
  (*if q1 = q2 then invalid_arg "add_eps_trans";*)
  add_trans_aux a q1 256 q2

let add_trans_opt a q1 c q2 =
  match c with None -> add_eps_trans a q1 q2 | Some c -> add_trans a q1 c q2

let set_final a q = a.final.(q) <- true
let unset_final a q = a.final.(q) <- false
let is_final a q = a.final.(q)
let size a = Array.length a.trans

let is_det a =
  let check t = Array.for_all (function [] | [ _ ] -> true | _ -> false) t in
  Array.for_all check a.trans && not (has_epsilon a)

open Scanf

let load file =
  let c = Scanning.open_in file in
  let n = bscanf c "%d\n" (fun n -> n) in
  let a = create n in
  let e = bscanf c "%d\n" (fun e -> e) in
  for _ = 1 to e do
    bscanf c "%d %C %d\n" (fun i c j -> add_trans a i c j)
  done;
  (try
     while true do
       bscanf c "final %d\n" (set_final a)
     done
   with End_of_file -> ());
  a

  let remove_states auto states =
    if states = [] then copy auto
    else
      let is_empty = ref false in
      let smap = Hashtbl.create 16 in
      List.iter
        (fun s ->
          if not !is_empty then is_empty := s = 0;
          Hashtbl.replace smap s (-1))
        states;
      (* Si on retire l'état initial *)
      if !is_empty then create 1
      else
        let state_counter = ref ~-1 in
        let next () =
          incr state_counter;
          !state_counter
        in
        is_empty := true;
        for q = 0 to size auto - 1 do
          if not (Hashtbl.mem smap q) then (
            let nq = next () in
            (* si on garde au moins un état acceptant *)
            if is_final auto q && !is_empty then is_empty := false;
            Hashtbl.add smap q nq)
        done;
        if !is_empty then create 1
        else
          let nauto = create (!state_counter + 1) in
          let keep_state q = Hashtbl.find smap q >= 0 in
          let map_state q = Hashtbl.find smap q in
          Hashtbl.iter
            (fun q nq ->
              if nq >= 0 then (
                if is_final auto q then set_final nauto nq;
                let trs = all_trans_opt auto q in
                List.iter
                  (fun (ch, s) ->
                    if keep_state s then
                      add_trans_opt nauto nq ch (map_state s))
                  trs))
            smap;
          nauto
open Format

let print fmt a =
  fprintf fmt "%d@\n" (size a);
  let e =
    Array.fold_left (Array.fold_left (fun e l -> e + List.length l)) 0 a.trans
  in
  fprintf fmt "%d@\n" e;
  for i = 0 to size a - 1 do
    let trans i c l =
      List.iter
        (fun j ->
          fprintf fmt "%d '%s' %d@\n" i
            (if c < 256 then String.make 1 (Char.chr c) else "ε")
            j)
        l
    in
    Array.iteri (trans i) a.trans.(i)
  done;
  for i = 0 to size a - 1 do
    if is_final a i then fprintf fmt "final %d@\n" i
  done

let print_dot fmt a =
  fprintf fmt
    "digraph auto {\n\
    \  fontname=\"Helvetica,Arial,sans-serif\"\n\
    \  node [fontname=\"Helvetica,Arial,sans-serif\"]\n\
    \  edge [fontname=\"Helvetica,Arial,sans-serif\"]\n\
    \  rankdir=LR;\n\
    \  node [shape = doublecircle];";
  for i = 0 to size a - 1 do
    if is_final a i then fprintf fmt " %d" i
  done;
  fprintf fmt ";\n  node [shape = circle];@\n";
  let trans i n j =
    let c = Char.chr n in
    let s =
      if (65 <= n && n <= 90) || (97 <= n && n <= 122) then String.make 1 c
      else string_of_int n
    in
    fprintf fmt "  %d -> %d [label = \"%s\"];@\n" i j s
  in
  let trans i c l = List.iter (trans i c) l in
  let trans i = Array.iteri (trans i) in
  Array.iteri trans a.trans;
  fprintf fmt "}@\n"

let print_dot_to_file ~file a =
  let c = open_out file in
  let fmt = formatter_of_out_channel c in
  fprintf fmt "%a@." print_dot a;
  close_out c

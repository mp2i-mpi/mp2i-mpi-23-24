#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

bool word_end(int c) {
    return c == '\n' || c == EOF;
}

bool q0();
bool q1();
bool q2();
bool q_p();

bool q0() {
    printf("q0\n");
    int c = getchar();
    if (word_end(c)) return false;
    if (q2() || q1()) return true;
    if (c == 'a') return (q2() || q1());
    if (c == 'b') return q1();

    assert (false);
}

bool q1() {
    printf("q1\n");
    int c = getchar();
    if (word_end(c)) return true;
    if (c == 'a' || c == 'b') return q_p();

    assert (false);
}

bool q2() {
    printf("q2\n");
    int c = getchar();
    if (word_end(c)) return true;
    if (c == 'a' || c == 'b') return q2();

    assert (false);
}

bool q_p() {
    printf("q_p\n");
    int c = getchar();
    if (word_end(c)) return false;
    if (c == 'a' || c == 'b') return q_p();

    assert (false);
}

int main() {
    if (q0()) {
        printf("Accepté\n");
    } else {
        printf("Refusé\n");
    }
    return 0;
}

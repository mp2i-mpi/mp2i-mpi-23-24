module Lexer = struct
  let list_of_string s = List.of_seq (String.to_seq s)

  let is_letter c = (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')
  let is_digit c = c >= '0' && c <= '9'

  let rec identifier_q1 w res =
    match w with
    | [] -> Some res
    | t :: q when is_letter t || is_digit t -> (identifier_q1 q (res ^ (String.make 1 t)))
    | _ -> None

  let identifier_q0 w =
    match w with
    | [] -> None
    | t :: q when is_letter t -> identifier_q1 q (String.make 1 t)
    | _ -> None

  let identifier w = identifier_q0 (list_of_string w)

  let test_identifier () =
    assert (identifier "a" = Some "a");
    assert (identifier "a1" = Some "a1");
    assert (identifier "a1b2" = Some "a1b2");
    assert (identifier "1" = None);
    assert (identifier "1a" = None);
    assert (identifier "" = None)

  let parse_digit c = Char.code c - Char.code '0'

  let rec number_q1 w res =
    match w with
    | [] -> Some res
    | t::q when is_digit t -> number_q1 q (res * 10 + (parse_digit t))
    | _ -> None

  let rec number_q0 w =
    match w with
    | [] -> None
    | '+'::q -> number_q0 q
    | '-'::q -> Option.map (fun n -> -n) (number_q0 q)
    | t::q when is_digit t -> number_q1 q (parse_digit t)
    | _ -> None

  let number w = number_q0 (list_of_string w)

  type token =
  | Id of string
  | Num of int
  | Sym of char

  let string_of_token t =
    match t with
    | Id s -> "Id(" ^ s ^ ")"
    | Num n -> "Num(" ^ (string_of_int n) ^ ")"
    | Sym c -> "Sym(" ^ (String.make 1 c) ^ ")"

  let print_token t = print_string (string_of_token t)

  let rec q0 w =
    match w with
    | [c] when not ((is_letter c) || is_digit c) -> Sym c
    | '+'::_ | '-'::_ -> (
        match number_q0 w with
        | None -> failwith "Invalid syntax"
        | Some n -> Num n
    )
    | a::q when is_digit a -> (
        match number_q0 w with
        | None -> failwith "Invalid syntax"
        | Some n -> Num n
    )
    | a::q when is_letter a -> (
        match identifier_q0 w with
        | None -> failwith "Invalid syntax"
        | Some id -> Id id
    )
    | _ -> failwith "Invalid syntax"

  let test_number () =
    assert (number "1" = Some 1);
    assert (number "+1" = Some 1);
    assert (number "-1" = Some (-1));
    assert (number "123" = Some 123);
    assert (number "a" = None);
    assert (number "a1" = None);
    assert (number "" = None)

  let tokenize text =
    let words = String.split_on_char ' ' text in
    List.map (fun w -> q0 (list_of_string w)) words

  let test_tokenize () =
    let result = tokenize "a 1 +1 -1 123" in
    assert (result = [Id "a"; Num 1; Num 1; Num (-1); Num 123])
end

module Ast = struct
  type expr =
  | Id of string
  | Num of int
  | Binop of expr * char * expr

  let rec string_of_expr e =
    match e with
    | Id id -> "Id(" ^ id ^ ")"
    | Num n -> "Num(" ^ (string_of_int n) ^ ")"
    | Binop (e1, op, e2) -> Printf.sprintf "Binop(%s, %c, %s)" (string_of_expr e1) op (string_of_expr e2)

  let print_expr e = print_string (string_of_expr e)

  type affect = {
    id: string;
    e: expr
  }

  let string_of_affect a = Printf.sprintf "Affect(%s, %s)" a.id (string_of_expr a.e)
  let print_affect a = print_string (string_of_affect a)

  type ast = affect list

  let string_of_ast a = String.concat "\n" (List.map string_of_affect a)
  let print_ast a = print_string (string_of_ast a)
end

module Parser = struct
  let _is_binop c = match c with
    | '+' | '-' | '*' | '/' -> true
    | _ -> false
  let rec parse_binop lhs tokens =
    match tokens with
    | Lexer.Sym c :: q when _is_binop c -> (
        let rhs, remaining = parse_expr q in
        parse_binop (Ast.Binop (lhs, c, rhs)) remaining
      )
    | _ -> lhs, tokens

  and parse_expr tokens : Ast.expr * Lexer.token list =
    match tokens with
    | Lexer.Id id :: q -> parse_binop (Ast.Id id) q
    | Lexer.Num n :: q -> parse_binop (Ast.Num n) q
    | Lexer.Sym '(' :: q -> (
        let e, remaining = parse_expr q in
        match remaining with
        | Sym ')' :: q -> e, q
        | _ -> failwith "Invalid syntax : expected ')'"
    )
    | _ -> failwith "TODO"

  let parse_affect tokens =
    (* print_endline ("parse_affect(" ^ (String.concat ", " (List.map Lexer.string_of_token tokens)) ^ ")"); *)
    match tokens with
    | Lexer.Id id :: Lexer.Sym '=' :: q -> (
        let e, remaining = parse_expr q in
        match remaining with
        | Lexer.Sym ';' :: q ->
            Ast.{
              id = id;
              e = e
            }, q
        | _ -> failwith "Invalid syntax : expected ';'"
    )
    | _ -> failwith "Invalid syntax : expected 'id = ...'"

  let rec parse_program_aux (tokens: Lexer.token list) (acc: Ast.affect list) : Ast.affect list =
    match tokens with
    | [] -> acc
    | _ -> (
        let affect, remaining = parse_affect tokens in
        parse_program_aux remaining (affect :: acc)
    )
  let parse_program (tokens: Lexer.token list) : Ast.ast =
    match tokens with
    | [] -> failwith "Empty program"
    | _ -> List.rev (parse_program_aux tokens [])

  let test_parse () =
    let tokens = [
      Lexer.Id "a"; Lexer.Sym '='; Lexer.Num 1; Lexer.Sym ';';
      Lexer.Id "b"; Lexer.Sym '='; Lexer.Id "a"; Lexer.Sym ';';
      Lexer.Id "c"; Lexer.Sym '='; Lexer.Num 3; Lexer.Sym ';';
      Lexer.Id "d"; Lexer.Sym '='; Lexer.Sym('('); Lexer.Num 4; Lexer.Sym(')'); Lexer.Sym ';';
    ] in
    let ast = parse_program tokens in
    assert (ast = [
      Ast.{ id = "a"; e = Ast.Num 1 };
      Ast.{ id = "b"; e = Ast.Id "a" };
      Ast.{ id = "c"; e = Ast.Num 3 };
      Ast.{ id = "d"; e = Ast.Num 4 };
    ])
end


let () =
  Lexer.test_identifier ();
  Lexer.test_number ();
  Lexer.test_tokenize ()

let () =
  Parser.test_parse ()

open Graphics ;;


let carre x = x *. x

let proba m n i j = 
  let num = (4. *. (carre   (( (Int.to_float i) -. (Int.to_float n  -. 1.) /.2.)))    +.   (carre   (( (Int.to_float j) -. (Int.to_float n  -. 1.) /.2.))) )    in
  let den = (carre (Int.to_float n -. 1.) +. carre (Int.to_float m -. 1.)) in 
  let frac = num/.den in

  1. -. frac

let list_to_array l = Array.of_seq(List.to_seq l)

let construire_grille n m p =
  Random.self_init () ;
  let mat = Array.make_matrix n m 0 in 
  let i = 0 in 
  let j = 0 in 
  while (i < n) do 
    while (j < m) do 
      if (Random.float 1.0 ) < (p*. proba m n i j) then mat.(i).(j) <- 1 
    done;
  done;
  mat 


let coords mat = 
  let res = ref [] in
  let i = 0 in 
  let j = 0 in 
  while (i < Array.length mat) do 
    while (j < Array.length mat.(0)) do 
      if (mat.(i).(j) = 1) then (res := (i, j) :: !res )

    done; 
  done;
  list_to_array !res

let tracer coords_tab k =
  Graphics.set_color black ;
  Graphics.set_line_width k ;
  Graphics.resize_window (k*Array.length mat) (k*Array.length mat.(0)) ; 

  Graphics.plots coords_tab 


let tracer_grille mat k =
  tracer (coords mat) k 





let test_proba() =
  let i = 1 in 
  let j = 2 in 
  let n = 5 in 
  let m = 5 in

  print_float (proba m n i j);
  print_endline ("") 



let () =
Graphics.open_graph ;
test_proba () ;
print_endline("tout est ok")  
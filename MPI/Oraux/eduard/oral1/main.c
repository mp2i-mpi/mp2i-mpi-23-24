#include <stdio.h>
#include <stdlib.h>

#include "temps.h"
#include "list.h"

// Q3

typedef struct Gtemp {
    int n;
    list **adj_lists;
} gtemp;

gtemp *gtemp_create(int n) {
    gtemp *res = malloc(sizeof(gtemp));
    res->n = n;
    res->adj_lists = calloc(n, sizeof(list));
    for (int i = 0; i < n; ++i) {
        res->adj_lists[i] = list_create();
    }
    return res;
}

gtemp *gtemp_destroy(gtemp *g) {
    for (int i = 0; i < g->n; ++i) {
        list_destroy(g->adj_lists[i]);
    }
    free(g->adj_lists);
    free(g);
}

void add_arete(gtemp *g, int x, int y, temps td, temps ta) {
    arete a = {
        .succ = y,
        .td = td,
        .ta = ta
    };
    list_push_back(g->adj_lists[x], a);
}

list* get_aretes(gtemp *g, int x, int y) {
    list *res = list_create();
    list *adj_list = g->adj_lists[x];
    for (lnode *n = adj_list->first; n != NULL; n = n->next) {
        if (n->v.succ == y) {
            list_push_back(res, n->v);
        }
    }
    return res;
}

// Q4

int duree_chemin(gtemp *g, int *c, int n_c, temps t) {
    temps t_actuel = t;
    for (int i = 0; i < n_c-1; ++i) {
        int pred = c[i];
        int succ = c[i+1];
        list *aretes = get_aretes(g, pred, succ);
        assert (!list_is_empty(aretes));
        bool temps_trouve = false;
        temps t_arrivee = t_actuel;
        for (lnode *n = aretes->first; n != NULL; n = n->next) {
            if (difference(&n->v.td, &t_actuel) >= 0) {
                temps_trouve = true;
                if (!temps_trouve || difference(&t_arrivee, &n->v.ta) > 0) {
                    t_arrivee = n->v.ta;
                }
            }
        }
        assert(temps_trouve);
        t_actuel = t_arrivee;
        list_destroy(aretes);
    }

    return difference(&t_actuel, &t);
}

// Q5 : Impossible d'aller de D à C -> avec l'arc, c'est possible

// Q6

void add_arete_1011(gtemp *g, int x, int y, int md, int ma) {
    temps td = { 10, md };
    temps ta = { 10, ma };
    add_arete(g, x, y, td, ta);
}

gtemp *creer_graphe_exemple() { 
    gtemp *res = gtemp_create(5);
    const int A = 0; 
    const int B = 1; 
    const int C = 2; 
    const int D = 3; 
    const int E = 4;

    add_arete_1011(res, A, B, 8, 13);
    add_arete_1011(res, A, E, 48, 51);
    add_arete_1011(res, B, C, 15, 27);
    add_arete_1011(res, C, B, 42, 49);
    add_arete_1011(res, C, D, 31, 34);
    add_arete_1011(res, D, A, 42, 46);
    add_arete_1011(res, D, B, 33, 38);
    add_arete_1011(res, E, D, 0, 3);
    add_arete_1011(res, E, D, 53, 56);

    add_arete_1011(res, D, A, 4, 6);

    return res;
}

// Q7

int duree_chemin_periodique(gtemp *g, int *c, int n_c, temps t) {
    temps t_actuel = t;
    for (int i = 0; i < n_c-1; ++i) {
        int pred = c[i];
        int succ = c[i+1];
        list *aretes = get_aretes(g, pred, succ);
        assert (!list_is_empty(aretes));
        temps t_arrivee = t_actuel;
        for (lnode *n = aretes->first; n != NULL; n = n->next) {
            temps t_d = n->v.td;
            temps t_a = n->v.ta;
            while (difference(&t_d, &t_actuel) < 0 && difference(&t_arrivee, &t_a) > 0) {
                t_d.h += 24;
                t_a.h += 24;
            }
            if (difference(&t_arrivee, &t_a) > 0) {
                    t_arrivee = n->v.ta;
            }
        }
        t_actuel = t_arrivee;
        list_destroy(aretes);
    }

    return difference(&t_actuel, &t);
}

// Q8 : Il suffit qu'un chemin existe, indépendamment des temps ; on pourra toujours le prendre le lendemain du moment d'arrivée.
// On peut alors utiliser l'algorithme de Kosaraju pour vérifier qu'il n'y a qu'une seule composante connexe.

// Q9

void convert(const char *s, temps *dest) {
    int ignore_seconds;
    sscanf(s, "%d%d%d:%d", &dest->h, &dest->m, ignore_seconds);
}

// Q10



int main() {}
#include <stdlib.h>
#include <stdbool.h>

#include "list.h"

lnode *lnode_create(arete v) {
    lnode *n = malloc(sizeof(lnode));
    n->v = v;
    n->prev = NULL;
    n->next = NULL;
    return n;
}

list *list_create() {
    list *l = malloc(sizeof(list));
    l->first = NULL;
    l->last = NULL;
    return l;
}

void list_destroy(list *l) {
    lnode *n = l->first;
    while (n != NULL) {
        lnode *next = n->next;
        free(n);
        n = next;
    }
    free(l);
}

// Aussi possible avec une boucle for :
void list_destroy_v2(list *l) {
    for (lnode *n = l->first; n != NULL; n = n->next) {
        free(n);
    }
    free(l);
}

bool list_is_empty(list *l) {
    return l->first == NULL;
}

int list_size(list *l) {
    int size = 0;
    for (lnode *n = l->first; n != NULL; n = n->next) {
        ++size;
    }
    return size;
}

void list_push_back(list *l, arete v) {
    lnode *n = lnode_create(v);
    if (l->last == NULL) {
        l->first = n;
        l->last = n;
    } else {
        l->last->next = n;
        n->prev = l->last;
        l->last = n;
    }
}

void list_push_front(list *l, arete v) {
    lnode *n = lnode_create(v);
    if (l->first == NULL) {
        l->first = n;
        l->last = n;
    } else {
        l->first->prev = n;
        n->next = l->first;
        l->first = n;
    }
}

arete *list_back(list *l) {
    return &l->last->v;
}

arete *list_front(list *l) {
    return &l->first->v;
}

void list_pop_back(list *l) {
    lnode *n = l->last;
    l->last = n->prev;
    if (l->last != NULL) {
        l->last->next = NULL;
    } else {
        l->first = NULL;
    }
    free(n);
}

void list_pop_front(list *l) {
    lnode *n = l->first;
    l->first = n->next;
    if (l->first != NULL) {
        l->first->prev = NULL;
    } else {
        l->last = NULL;
    }
    free(n);
}

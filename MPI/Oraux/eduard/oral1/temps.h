#ifndef TEMPS_H_INCLUDED
#define TEMPS_H_INCLUDED

// Q1

typedef struct Temps {
    int h;
    int m;
} temps;

// Q2

int difference(temps* t1, temps* t2) {
    return (t1->h - t2->h) * 60 + (t1->m - t2->m);
}

#endif // TEMPS_H_INCLUDED
#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

#include <stdbool.h>

#include "temps.h"

typedef struct Arete {
    int succ;
    temps td;
    temps ta;
} arete;

typedef struct LNode {
    arete v;
    struct LNode *prev;
    struct LNode *next;
} lnode;

typedef struct List {
    struct LNode *first;
    struct LNode *last;
} list;

list *list_create();
void list_destroy(list *l);

bool list_is_empty(list *l);
int list_size(list *l);

void list_push_back(list *l, arete a);
void list_push_front(list *l, arete a);

arete* list_back(list *l);
arete* list_front(list *l);

void list_pop_back(list *l);
void list_pop_front(list *l);

#endif // LIST_H_INCLUDED

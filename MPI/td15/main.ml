module Lexer = struct
  let list_of_string s = List.of_seq (String.to_seq s)

  let is_letter c = (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')
  let is_digit c = c >= '0' && c <= '9'

  let rec identifier_q1 w res =
    match w with
    | [] -> Some res
    | t :: q when is_letter t || is_digit t -> (identifier_q1 q (res ^ (String.make 1 t)))
    | _ -> None

  let identifier_q0 w =
    match w with
    | [] -> None
    | t :: q when is_letter t -> identifier_q1 q (String.make 1 t)
    | _ -> None

  let identifier w = identifier_q0 (list_of_string w)

  let test_identifier () =
    assert (identifier "a" = Some "a");
    assert (identifier "a1" = Some "a1");
    assert (identifier "a1b2" = Some "a1b2");
    assert (identifier "1" = None);
    assert (identifier "1a" = None);
    assert (identifier "" = None)

  let parse_digit c = Char.code c - Char.code '0'

  let rec number_q1 w res =
    match w with
    | [] -> Some res
    | t::q when is_digit t -> number_q1 q (res * 10 + (parse_digit t))
    | _ -> None

  let rec number_q0 w =
    match w with
    | [] -> None
    | '+'::q -> number_q0 q
    | '-'::q -> Option.map (fun n -> -n) (number_q0 q)
    | t::q when is_digit t -> number_q1 q (parse_digit t)
    | _ -> None

  let number w = number_q0 (list_of_string w)

  type token =
  | Id of string
  | Num of int
  | Sym of char

  let string_of_token t =
    match t with
    | Id s -> "Id(" ^ s ^ ")"
    | Num n -> "Num(" ^ (string_of_int n) ^ ")"
    | Sym c -> "Sym(" ^ (String.make 1 c) ^ ")"

  let rec q0 w =
    match w with
    | [c] when not ((is_letter c) || is_digit c) -> Sym c
    | '+'::_ | '-'::_ -> (
        match number_q0 w with
        | None -> failwith "Invalid syntax"
        | Some n -> Num n
    )
    | a::q when is_digit a -> (
        match number_q0 w with
        | None -> failwith "Invalid syntax"
        | Some n -> Num n
    )
    | a::q when is_letter a -> (
        match identifier_q0 w with
        | None -> failwith "Invalid syntax"
        | Some id -> Id id
    )
    | _ -> failwith "Invalid syntax"

  let test_number () =
    assert (number "1" = Some 1);
    assert (number "+1" = Some 1);
    assert (number "-1" = Some (-1));
    assert (number "123" = Some 123);
    assert (number "a" = None);
    assert (number "a1" = None);
    assert (number "" = None)

  let tokenize text =
    let words = String.split_on_char ' ' text in
    List.map (fun w -> q0 (list_of_string w)) words

  let test_tokenize () =
    let result = tokenize "a 1 +1 -1 123" in
    assert (result = [Id "a"; Num 1; Num 1; Num (-1); Num 123])
end


let () =
  Lexer.test_identifier ();
  Lexer.test_number ();
  Lexer.test_tokenize ()

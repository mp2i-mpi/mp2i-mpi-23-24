#include <stdbool.h>

void swap(int* x, int* y) {
    int tmp = *x;
    *x = *y;
    *y = tmp;
}

bool is_sorted(int a[], int n) {
    for (int i = 0; i < n-1; ++i) {
        if (a[i] > a[i+1]) {
            return false;
        }
    }
    return true;
}

void swap_array(int a[], int i, int j) {
    swap(a+i, a+j);
}

void two_way_sort(int a[], int n) {
    int i = 0;
    int j = n-1;
    while (i < j) {
        if (a[i] > a[j]) {
            swap_array(a, i, j);
            ++i;
        }
        ++j;
    }
}

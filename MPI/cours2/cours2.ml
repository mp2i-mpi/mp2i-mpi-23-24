type t = B | N | R

let permute_1 = function 
  | B -> N
  | N -> R
  | R -> B
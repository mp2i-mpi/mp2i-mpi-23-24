public enum Player
{
    Player1,
    Player2,
    ChancePublic,
    GameEnd
}


public class InformationSet
{
    public float[] RegretSum;
    public float[] StrategySum;
    public InformationSet(int NumberOfActions)
    {
        //Initialise float arrays to required length and set elements ofarray to zero
        this.RegretSum = new float[NumberOfActions];
        this.StrategySum = new float[NumberOfActions];
    }
}

public class InformationSetCFRLogic
{
    public float[] GetStrategy(InformationSet infoSet)
    {
        float[] PosRegrets = infoSet.RegretSum.Select(e => Math.Max(0,e)).ToArray();
        return Helpers.Normalise(PosRegrets);
    }
    public void AddToStrategySum(InformationSet infoSet, float[]strategy, float activePlayerReachProbability)
    {
        infoSet.StrategySum = infoSet.StrategySum.Zip(strategy, (x, y)=> x + y * activePlayerReachProbability).ToArray();
    }
    public float[] GetFinalStrategy(InformationSet infoSet)
    {
        return Helpers.Normalise(infoSet.StrategySum);
    }
    public void AddToCumulativeRegrets
        (InformationSet infoSet, GameStateNode gameStateNode, float[]actionUtilities, float nodeUtility)
    {
        if (gameStateNode.ActivePlayer == Player.Player1)
        {
            float counterFactualReachProbability = gameStateNode.ReachProbabiltyP2;
            for (int i = 0; i < actionUtilities.Length; i++)
            {
                infoSet.RegretSum[i] = infoSet.RegretSum[i] +counterFactualReachProbability * (actionUtilities[i]- nodeUtility);
            }
        }
        else if (gameStateNode.ActivePlayer == Player.Player2)
        {
            float counterFactualReachProbability = gameStateNode.ReachProbabiltyP1;
            for (int i = 0; i < actionUtilities.Length; i++)
            {
                infoSet.RegretSum[i] = infoSet.RegretSum[i] +counterFactualReachProbability * (nodeUtility -actionUtilities[i]);
            }
        }
    }
}

public class GameStateNode
{
    //Node Properties
    public List<string> History { get; set; }
    public List<string> Player1Cards { get; set; }
    public List<string> Player2Cards { get; set; }
    public float ReachProbabiltyP1 { get; set; }
    public float ReachProbabiltyP2 { get; set; }
    public Player ActivePlayer { get; set; }
    public List<string> ActionOptions { get; set; }
    public GameStateNode() { }
    //Create an initial node
    public static GameStateNode GetStartingNode(List<string> history, List<string> player1Cards, List<string> player2Cards)
    {
        GameStateNode newNode = new GameStateNode();
        //Initiate new node properties
        newNode.History = history;
        newNode.Player1Cards = player1Cards;
        newNode.Player2Cards = player2Cards;
        newNode.ReachProbabiltyP1 = 1;
        newNode.ReachProbabiltyP2 = 1;
        newNode.ActivePlayer = PokerRules.GetActivePlayer(newNode.History);
        //Set Action Options
        if (newNode.ActivePlayer == Player.Player1 || newNode.ActivePlayer == Player.Player2)
        {
            newNode.ActionOptions = PokerRules.AvailablePlayerAction(newNode.History);
        }
        else if (newNode.ActivePlayer == Player.ChancePublic)
        {
            newNode.ActionOptions = PokerRules.AvailableChanceAction(newNode.History, newNode.Player1Cards, newNode.Player2Cards);
        }
        return newNode;
    }
    //Constructor for creating child node from parent node
    public GameStateNode(GameStateNode parentGameState, string action,float actionProbability)
    {
        //Initiate node properties
        this.History = new List<string>(parentGameState.History);
        this.History.Add(action);
        this.Player1Cards = new List<string>(parentGameState.Player1Cards);
        this.Player2Cards = new List<string>(parentGameState.Player2Cards);
        this.ActivePlayer = PokerRules.GetActivePlayer(this.History);
        this.ReachProbabiltyP1 = parentGameState.ReachProbabiltyP1;
        this.ReachProbabiltyP2 = parentGameState.ReachProbabiltyP2;
        //Update Reach Probabilities
        if (parentGameState.ActivePlayer == Player.Player1)
        {
            this.ReachProbabiltyP1 = parentGameState.ReachProbabiltyP1 *actionProbability;
        }
        else if (parentGameState.ActivePlayer == Player.Player2)
        {
            this.ReachProbabiltyP2 = parentGameState.ReachProbabiltyP2 *actionProbability;
        }
        //Set Action Options
        if (this.ActivePlayer == Player.Player1 || this.ActivePlayer ==Player.Player2)
        {
            this.ActionOptions = PokerRules.AvailablePlayerAction(History);
        }
        else if (this.ActivePlayer == Player.ChancePublic)
        {
            this.ActionOptions = PokerRules.AvailableChanceAction(History, Player1Cards, Player2Cards);
        }
    }
}

public class VanillaCFRTrainer
{
    public Dictionary<string, InformationSet> InfoSetMap { get; set; }
    public BestResponseUtility BestResponseUtility { get; set; }
    public InformationSetCFRLogic InformationSetMethods { get; set; }
    public int Iteration { get; set; }
    public Player UpdatingPlayer { get; set; }
    public VanillaCFRTrainer()
    {
        InfoSetMap = new Dictionary<string, InformationSet>();
    }
    
    public InformationSet GetInformationSet(GameStateNode gameStateNode)
    {
        List<string> infoSetHistory = new List<string>(gameStateNode.History);
        if (gameStateNode.ActivePlayer == Player.Player1)
        {
            infoSetHistory.InsertRange(0, gameStateNode.Player1Cards);
        }
        else if (gameStateNode.ActivePlayer == Player.Player2)
        {
            infoSetHistory.InsertRange(0, gameStateNode.Player2Cards);
        }
        string key = string.Join("_", infoSetHistory);
        if (InfoSetMap.ContainsKey(key) == false)
        {
            InfoSetMap[key] = new InformationSet(gameStateNode.ActionOptions.Count);
        }
        return InfoSetMap[key];
    }
    
    public float Train(int numberIterations, List<string> boardArranged, List<string[]> handCombosP1, List<string[]> handCombosP2)
    {
        InformationSetMethods = new InformationSetCFRLogic();
        BestResponseUtility = new BestResponseUtility(InfoSetMapInformationSetMethods);
        Iteration = 0;
        float P1Util = 0;
        int utilP1Count = 0;
        List<string> startHistory = new List<string>(boardArranged);
        
        for (int i = 0; i < numberIterations; i++)
        {
            Iteration = i;
            UpdatingPlayer = (Player) (i % 2);
            P1Util = 0;
            utilP1Count = 0;
            //Iterate through all possible hand combinations
            for (int indexP1 = 0; indexP1 < handCombosP1.Count; indexP1++)
            {
                //Dont include p1 hands that conflict with the board
                if (boardArranged.Contains(handCombosP2[indexP1][0]) |boardArranged.Contains(handCombosP2[indexP1][1]))
                {
                    continue;
                }
                for (int indexP2 = 0; indexP2 < handCombosP2.Count; indexP2++)
                {
                    //Dont include p2 hands that conflict with curren phands
                    if (handCombosP2[indexP2].Contains(handCombosP1[indexP1][0]) |handCombosP2[indexP2].Contains(handCombosP1[indexP1][1]))
                    {
                        continue;
                    }
                    //Dont include p2 hands that conflict with the board
                    if(boardArranged.Contains(handCombosP2[indexP2][0] || boardArranged.Contains(handCombosP2[indexP2][1])))
                    {
                        continue;
                    }
                    //Initialise startNode
                    GameStateNode startNode = GameStateNode.GetStartingNode(startHistory, handCombosP1[indexP1].ToList(), handCombosP2[indexP2].ToList());
                    //Begin the CFR Recursion
                    P1Util += CalculateNodeUtility(startNode);
                    utilP1Count++;
                }
            }
            
            Console.WriteLine($"Iteration {i} complete.");
            Console.WriteLine($"Strategy Exploitability Percentage{BestResponseUtility.TotalDeviation(boardArranged, handCombosP1, handCombosP2)}");
            Console.WriteLine();
        }
        
        //return player 1 utility of last iteration
        return P1Util / utilP1Count;
    }
    
    //Returns utility from player 1 perspective
    public float CalculateNodeUtility(GameStateNode gameStateNode) {
        ///// TERMINAL NODE /////
        if (gameStateNode.ActivePlayer == Player.GameEnd)
        {
            var u = PokerRules.CalculatePayoff(gameStateNode.HistorygameStateNode.Player1Cards, gameStateNode.Player2Cards)
            return u;
        }
        float[] actionUtilities = new float[gameStateNode.ActionOptions.Count];
        float nodeUtility;
        ///// CHANCE NODE /////
        if (gameStateNode.ActivePlayer == Player.ChancePublic)
        {
            float actionProbability = (float)1 / gameStateNode.ActionOptions.Count;
            List<Task<float>> tasks = new List<Task<float>>();
            //get utility of each action
            for (int i = 0; i < actionUtilities.Length; i++)
            {
                GameStateNode childGameState = new GameStateNode(gameStateNode, gameStateNode.ActionOptions[i]actionProbability);
                actionUtilities[i] = CalculateNodeUtility(childGameSta);
            }
            //average utility for node calculated by Dot product actioutilities and action probabilities
            nodeUtility = actionUtilities.Select(u => u actionProbability).Sum()
            return nodeUtility;
        }
        
        ///// DECISION NODE /////
        else
        {
            float activePlayerReachProbability;
            if (gameStateNode.ActivePlayer == Player.Player1)
            {
                activePlayerReachProbability = gameStateNode.ReachProbabiltyP1;
            }
            else //ActivePlayer == Player2
            {
                activePlayerReachProbability = gameStateNode.ReachProbabiltyP2;
            }
            InformationSet infoSet = GetInformationSet(gameStateNode);
            var strategy = InformationSetMethods.GetStrategy(infoSet);
            
            //Only update on updating player
            if (gameStateNode.ActivePlayer == UpdatingPlayer)
            {
                InformationSetMethods.AddToStrategySum(infoSet, strate, activePlayerReachProbability);
            }
            //get utility of each action
            for (int i = 0; i < actionUtilities.Length; i++)
            {
                var actionProbability = strategy[i];
                GameStateNode childGameState = new GameStateNo(gameStateNode, gameStateNode.ActionOptions[i]actionProbability);
                actionUtilities[i] = CalculateNodeUtility(childGameSta);
            }
            //average utility for node calculated by Dot product actioutilities and action probabilities
            nodeUtility = actionUtilities.Zip(strategy, (x, y) => x *).Sum();
            
            //Only update on updating player
            if (gameStateNode.ActivePlayer == UpdatingPlayer)
            {
                InformationSetMethods.AddToCumulativeRegrets(infoSet, gameStateNode, actionUtilities, nodeUtility);
            }
            return nodeUtility;
        }
    }
}



internal class Program
{
    static void Main(string[] args)
    {
        //Game Inputs
        int numIterations = 50;
        List<string> board = new List<string>() { "2d", "2s", "2c", "2h", "3d"};
        List<string> player1Range = new List<string>() { "AA", "KK", "QQ"};
        List<string> player2Range = new List<string>() { "AA", "KK", "QQ"};
        int startPotSize = 50;
        int effectiveStackSize = 100;
        List<float> availableBetSizes = new List<float>() { (float)0.5 };

        //set pot size, effective stacks and player actions
        PokerRules.SetStart(startPotSize, effectiveStackSize);
        PlayerAction.SetPlayerActions(availableBetSizes);
        
        //arrange board and hand combos
        List<string> boardArranged = Card.ArrangeCards(board);
        List<string[]> P1HandCombos = new List<string[]>();
        List<string[]> P2HandCombos = new List<string[]>();
        foreach (string hand in player1Range)
        {
            P1HandCombos.AddRange(Card.GetArrangedHandCombos(hand));
        }
        foreach (string hand in player2Range)
        {
            P2HandCombos.AddRange(Card.GetArrangedHandCombos(hand));
        }
        
        //create CFRTrainer object
        VanillaCFRTrainer trainer = new VanillaCFRTrainer();
        //Train Trainer
        float avgUtil = trainer.Train(numIterations, boardArranged, P1HandCombos, P2HandCombos);

        Console.WriteLine($"Player 1 Utility: {avgUtil}");
        InfoSetUI.View(trainer, board.Count);
    }
}
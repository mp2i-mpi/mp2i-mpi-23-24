module Graph = struct
  (*
    * Représente un graphe pondéré
    * Les sommets sont les entiers de 0 à n-1
    * Avec n la taille du tableau
    * On utilise des listes d'adjacence
    * L'arête (i, j) de poids w
    * est représentée par la paire (j, w)
    * dans la liste d'adjacence de i
  *)
  type t = int list array

  (*
    * Crée un graphe pondéré de taille n
  *)
  let make n : t = Array.make n []

  (*
    * Renvoie la taille du graphe
  *)
  let size (g: t) : int = Array.length g

  (*
    * Renvoie vrai si le graphe a une arête (i, j)
  *)
  let has_edge (g: t) (i: int) (j: int) : bool =
    List.mem j g.(i)

  (*
    * Ajoute une arête (i, j)
  *)
  let add_edge (g: t) (i: int) (j: int) : unit =
    if not (has_edge g i j) then
      g.(i) <- j :: g.(i);
      g.(j) <- i :: g.(j)

  (*
    * Renvoie la liste des successeurs de i
  *)
  let succ (g: t) (i: int) : int list = g.(i)

  (*
    * Renvoie la liste de toutes les arêtes du graphe
  *)
  let edges (g: t) : (int * int) list =
    let l = ref [] in
    for i = 0 to size g - 1 do
      let add_i_edge j =
        if i < j then
          l := (i, j) :: !l
      in
      List.iter add_i_edge g.(i)
    done;
    !l
end

(* Vérifie si la liste d'arêtes l est un couplage dans le graphe g *)
let is_matching (g: Graph.t) (l: (int*int) list) : bool =
  if not (List.for_all (fun (i, j) -> Graph.has_edge g i j) l) then
    false
  else (
    let n = Graph.size g in
    let visited = Array.make n false in
    let rec aux = function
      | [] -> true
      | (i, j) :: q ->
        if visited.(i) || visited.(j) then 
          false
      else (
        visited.(i) <- true;
        visited.(j) <- true;
        aux q
      )
    in
    aux l
  )




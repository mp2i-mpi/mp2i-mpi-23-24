type uf = {
  parents: int array;
  ranks: int array
}

let create (n: int) : uf = {
  parents = Array.init n (fun i -> i);
  ranks = Array.make n 0
}

let rec find (uf: uf) (i: int) : int =
  if uf.parents.(i) = i then
    i
  else
    find uf uf.parents.(i)

let union (uf: uf) (i: int) (j: int) : unit =
  let ri = find uf i in
  let rj = find uf j in
  if ri <> rj then (
    if uf.ranks.(ri) < uf.ranks.(rj) then
      uf.parents.(ri) <- rj
    else (
      uf.parents.(rj) <- ri;
      if uf.ranks.(ri) = uf.ranks.(rj) then
        uf.ranks.(ri) <- uf.ranks.(ri) + 1
    )
  )

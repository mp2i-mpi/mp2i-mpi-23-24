type uf = int array

let create (n: int) : uf = Array.init n (fun i -> i)

let rec find (uf: uf) (i: int) : int =
  if uf.(i) = i then
    i
  else
    find uf uf.(i)

let union (uf: uf) (i: int) (j: int) : unit =
  uf.(find uf i) <- find uf j

(*

let x = 10 
let x = 
  let y = 10 + x in 
  let y = let x = y * x in y + x in
  y+x
let z = x + 100
let () = print_int z

*)

let rec interval_aux res i j =
  if i = j then
    i::res
  else
    interval_aux (j::res) i (j-1)

let interval i j = if i > j then failwith "i > j" else interval_aux [] i j

let () =
    let l = interval 3 12 in
    print_string "[|3, 12|] = ";
    let rec loop = function
      | [] -> ()
      | t::q -> Printf.printf "%d " t; loop q
    in loop l;
    print_newline()


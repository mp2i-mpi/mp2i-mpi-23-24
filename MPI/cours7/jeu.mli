type player
type state

val player: state -> player
val moves: state -> state list
val outcome: state -> player option

val heuristique: player -> state -> int
val hmax: int
val hmin: int

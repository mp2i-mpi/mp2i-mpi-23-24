module MyList = struct
  (* Utile pour les versions récursives terminales *)
  (* qui ont tendance à renverser la liste *)
  let rec rev (l: 'a list) : 'a list =
    let rec rev_aux (l: 'a list) (acc: 'a list) : 'a list =
      match l with
      | [] -> acc
      | x::xs -> rev_aux xs (x::acc)
    in
    rev_aux l []

  let rec mem (x: 'a) (l: 'a list) : bool =
    match l with
    | [] -> false
    | y::ys -> x = y || mem x ys

  (* Version simple *)
  let rec map_simple (f: 'a -> 'b) (l: 'a list) : 'b list =
    match l with
    | [] -> []
    | x::xs -> (f x)::(map_simple f xs)

  (* Version récursive terminale *)
  let map (f: 'a -> 'b) (l: 'a list) : 'b list =
    let rec map_aux (f: 'a -> 'b) (l: 'a list) (acc: 'b list) : 'b list =
      match l with
      | [] -> acc
      | x::xs -> map_aux f xs ((f x)::acc)
    in
    rev (map_aux f l [])

  let rec iter (f: 'a -> unit) (l: 'a list) : unit =
    match l with
    | [] -> ()
    | x::xs -> f x; iter f xs

  let rec fold_left (f: 'a -> 'b -> 'a) (acc: 'a) (l: 'b list) : 'a =
    match l with
    | [] -> acc
    | x::xs -> fold_left f (f acc x) xs

  let rec for_all (f: 'a -> bool) (l: 'a list) : bool =
    match l with
    | [] -> true
    | x::xs -> f x && for_all f xs

  let rec exists (f: 'a -> bool) (l: 'a list) : bool =
    match l with
    | [] -> false
    | x::xs -> f x || exists f xs
end

module MyArray = struct
  let map (f: 'a -> 'b) (a: 'a array) : 'b array =
    let n = Array.length a in
    let b = Array.make n (f a.(0)) in
    for i = 1 to n - 1 do
      b.(i) <- (f a.(i))
    done;
    b

  let iter (f: 'a -> unit) (a: 'a array) : unit =
    let n = Array.length a in
    for i = 0 to n - 1 do
      f a.(i)
    done

  let fold_left (f: 'a -> 'b -> 'a) (acc: 'a) (a: 'b array) : 'a =
    let n = Array.length a in
    let acc_ref = ref acc in
    for i = 0 to n - 1 do
      acc_ref := f !acc_ref a.(i)
    done;
    !acc_ref

  let for_all (f: 'a -> bool) (a: 'a array) : bool =
    let n = Array.length a in
    let rec for_all_aux (f: 'a -> bool) (a: 'a array) (i: int) : bool =
      if i = n then true
      else if f a.(i) then for_all_aux f a (i + 1)
      else false
    in
    for_all_aux f a 0

  let exists (f: 'a -> bool) (a: 'a array) : bool =
    let n = Array.length a in
    let rec exists_aux (f: 'a -> bool) (a: 'a array) (i: int) : bool =
      if i = n then false
      else if f a.(i) then true
      else exists_aux f a (i + 1)
    in
    exists_aux f a 0
end

#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

#include <stdbool.h>

typedef struct LNode {
    int v;
    struct LNode *prev;
    struct LNode *next;
} lnode;

typedef struct List {
    int v;
    struct LNode *first;
    struct LNode *last;
} list;

list *list_create();
void list_destroy(list *l);

bool list_is_empty(list *l);
int list_size(list *l);

void list_push_back(list *l, int v);
void list_push_front(list *l, int v);

int list_back(list *l);
int list_front(list *l);

void list_pop_back(list *l);
void list_pop_front(list *l);

#endif // LIST_H_INCLUDED

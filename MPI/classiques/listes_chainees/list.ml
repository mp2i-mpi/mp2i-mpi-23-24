type 'a node = {
  v: 'a;
  mutable prev: 'a node option;
  mutable next: 'a node option
}

let create_node (v: int) =
  { v = v; prev = None; next = None }

type 'a ll = {
  mutable first: 'a node option;
  mutable last: 'a node option
}

let create () =
  { first = None; last = None }

let is_empty ll =
  ll.first = None

let length ll =
  let rec loop acc n = match n with
    | None -> acc
    | Some node -> loop (acc + 1) node.next
  in
  loop 0 ll.first

let push_back ll value =
  let n = create_node value in
  match ll.last with
  | None -> (
    ll.first <- Some n;
    ll.last <- Some n
  )
  | Some l -> (
    n.prev <- Some l;
    n.next <- l.next;
    l.next <- Some n;
    ll.last <- Some n
  )


let push_front ll value =
  let n = create_node value in
  match ll.first with
  | None -> (
    ll.first <- Some n;
    ll.last <- Some n
  )
  | Some f -> (
    n.prev <- f.prev;
    n.next <- Some f;
    f.prev <- Some n;
    ll.first <- Some n
  )


let back ll =
  match ll.last with
  | None -> failwith "empty list"
  | Some l -> l.v

let front ll =
  match ll.first with
  | None -> failwith "empty list"
  | Some f -> f.v

let pop_back ll : 'a =
  match ll.last with
  | None -> failwith "empty list"
  | Some l -> (
    let v = l.v in
    match l.prev with
    | None -> (
      ll.first <- None;
      ll.last <- None
    )
    | Some prev -> (
      prev.next <- l.next;
      ll.last <- Some prev
    );
    v
  )

let pop_front ll =
  match ll.first with
  | None -> failwith "empty list"
  | Some f -> (
    let v = f.v in
    match f.next with
    | None -> (
      ll.first <- None;
      ll.last <- None
    )
    | Some next -> (
      next.prev <- f.prev;
      ll.first <- Some next
    );
    v
  )

type 'a queue = 'a ll

let enqueue = push_back
let dequeue = pop_front

type 'a stack = 'a ll

let push = push_front
let pop = pop_front
  
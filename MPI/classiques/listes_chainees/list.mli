type 'a node = {
  v: int;
  mutable prev: 'a node option;
  mutable next: 'a node option
}

type 'a ll = {
  mutable first: 'a node option;
  mutable last: 'a node option
}

val create: unit -> 'a ll

val is_empty: 'a ll -> bool
val length: 'a ll -> int

val push_back: 'a ll -> 'a -> unit
val push_front: 'a ll -> 'a -> unit

val back: 'a ll -> 'a
val front: 'a ll -> 'a

val pop_back: 'a ll -> unit
val pop_front: 'a ll -> unit

val map: ('a -> 'b) -> 'a ll -> 'b ll
val iter: ('a -> unit) -> 'a ll -> unit
val fold_left: ('a -> 'b -> 'a) -> 'a -> 'b ll -> 'a
val for_all: ('a -> bool) -> 'a ll -> bool
val exists: ('a -> bool) -> 'a ll -> bool

type 'a queue

val create: unit -> 'a queue
val is_empty: 'a queue -> bool
val enqueue: 'a queue -> 'a -> unit
val dequeue: 'a queue -> 'a

type 'a stack


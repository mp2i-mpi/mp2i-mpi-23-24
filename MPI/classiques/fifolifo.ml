type 'a stack = 'a list ref

let create () = ref []
let is_empty s = !s = []

let push x s = s := x :: !s
let pop s =
  match !s with
  | [] -> failwith "Empty stack"
  | x :: xs -> s := xs; x

type 'a queue = 'a list * 'a list

let queue_create () = [], []

let queue_is_empty (front, back) = front = [] && back = []

let rec queue_enqueue x (front, back) = front, x :: back
let rec queue_dequeue (front, back) =
  match front, back with
  | [], [] -> failwith "Empty queue"
  | [], _ -> queue_dequeue (List.rev back, [])
  | x :: xs, _ -> x, (xs, back)

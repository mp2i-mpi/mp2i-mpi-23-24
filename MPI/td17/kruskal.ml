module WDigraph = struct
  (*
    * Représente un graphe pondéré
    * Les sommets sont les entiers de 0 à n-1
    * Avec n la taille du tableau
    * On utilise des listes d'adjacence
    * L'arête (i, j) de poids w
    * est représentée par la paire (j, w)
    * dans la liste d'adjacence de i
  *)
  type t = (int * float) list array

  (*
    * Crée un graphe pondéré de taille n
  *)
  let make n : t = Array.make n []

  (*
    * Renvoie la taille du graphe
  *)
  let size (g: t) : int = Array.length g

  (*
    * Renvoie vrai si le graphe a une arête (i, j)
  *)
  let has_edge (g: t) (i: int) (j: int) : bool =
    List.exists (fun (k, _) -> k = j) g.(i)

  (*
    * Renvoie le poids de l'arête (i, j)
    * Renvoie +inf si l'arête n'existe pas
  *)
  let get_weight (g: t) (i: int) (j: int) : float =
    try
      List.assoc j g.(i)
    with Not_found -> infinity

  (*
    * Ajoute une arête (i, j) de poids w
  *)
  let add_edge (g: t) (i: int) (j: int) (w: float) : unit =
    if not (has_edge g i j) then
      g.(i) <- (j, w) :: g.(i);
      g.(j) <- (i, w) :: g.(j)

  (*
    * Renvoie la liste des successeurs de i
  *)
  let succ (g: t) (i: int) : (int * float) list = g.(i)

  (*
    * Renvoie la liste de toutes les arêtes du graphe
  *)
  let edges (g: t) : (int * int * float) list =
    let l = ref [] in
    for i = 0 to size g - 1 do
      let add_i_edge (j, w) =
        if i < j then
          l := (i, j, w) :: !l
      in
      List.iter add_i_edge g.(i)
    done;
    !l
end

module UF = struct
  type t = {
    parents: int array;
    ranks: int array
  }

  let create (n: int) : t = {
    parents = Array.init n (fun i -> i);
    ranks = Array.make n 0
  }

  let rec find (uf: t) (i: int) : int =
    let p = uf.parents.(i) in
    if p = i then
      i
    else (
      let r = find uf p in
      uf.parents.(i) <- r;
      r
    )

  let union (uf: t) (i: int) (j: int) : unit =
    let ri = find uf i in
    let rj = find uf j in
    if ri <> rj then (
      if uf.ranks.(ri) < uf.ranks.(rj) then
        uf.parents.(ri) <- rj
      else (
        uf.parents.(rj) <- ri;
        if uf.ranks.(ri) = uf.ranks.(rj) then
          uf.ranks.(ri) <- uf.ranks.(ri) + 1
      )
    )
end

module Pqueue = struct
  (* Détails d'implémentation *)

  type 'a heap = Empty | Node of 'a heap * 'a * 'a heap

  let empty_heap : 'a heap = Empty


  let rec heap_merge (h1: 'a heap) (h2: 'a heap) : 'a heap =
    match h1, h2 with
    | Empty, h | h, Empty -> h
    | Node (l1, x, r1), Node (l2, y, r2) ->
      if x < y then 
        Node (r1, x, heap_merge l1 h2)
      else
        Node (r2, y, heap_merge h1 l2)

  let heap_add (x: 'a) (t: 'a heap) : 'a heap = heap_merge (Node (Empty, x, Empty)) t

  let heap_pop (t: 'a heap) : 'a * 'a heap =
    match t with
    | Empty -> invalid_arg "pop : empty heap"
    | Node (l, x, r) -> x, heap_merge l r

  
  (* Fonctions utiles *)

  let is_empty (h: 'a heap) : bool = h = Empty

  type 'a t = 'a heap ref
  let create () : 'a t = ref Empty
  let is_empty (h: 'a t) : bool = !h = Empty
  let add (h: 'a t) (x: 'a) : unit = h := heap_add x !h
  let pop (h: 'a t) : 'a = let x, h' = heap_pop !h in h := h'; x
end

let kruskal (g : WDigraph.t) =
  let n = WDigraph.size g in
  let uf = UF.create n in
  let pqueue = Pqueue.create () in
  List.iter (fun (i, j, p) -> Pqueue.add pqueue (p, (i, j))) (WDigraph.edges g);
  let t = ref [] in
  let size_t = ref 0 in
  while !size_t < n-1 do
    let a = Pqueue.pop pqueue in
    let (_, (x, y)) = a in
    if UF.find uf x <> UF.find uf y then (
      UF.union uf x y;
      t := a::!t;
      size_t := !size_t + 1
    )
  done;
  t

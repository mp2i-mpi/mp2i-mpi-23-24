module Pqueue = struct
  type 'a heap = Empty | Node of 'a heap * 'a * 'a heap

  let empty_heap : 'a heap = Empty


  let rec heap_merge (h1: 'a heap) (h2: 'a heap) : 'a heap =
    match h1, h2 with
    | Empty, h | h, Empty -> h
    | Node (l1, x, r1), Node (l2, y, r2) ->
      if x < y then 
        Node (r1, x, heap_merge l1 h2)
      else
        Node (r2, y, heap_merge h1 l2)

  let heap_add (x: 'a) (t: 'a heap) : 'a heap = heap_merge (Node (Empty, x, Empty)) t

  let heap_pop (t: 'a heap) : 'a * 'a heap =
    match t with
    | Empty -> invalid_arg "pop : empty heap"
    | Node (l, x, r) -> x, heap_merge l r

  
  let is_empty (h: 'a heap) : bool = h = Empty

  type 'a t = 'a heap ref
  let create () : 'a t = ref Empty
  let is_empty (h: 'a t) : bool = !h = Empty
  let add (h: 'a t) (x: 'a) : unit = h := heap_add x !h
  let pop (h: 'a t) : 'a = let x, h' = heap_pop !h in h := h'; x
end

(*
  * Représente un graphe pondéré
  * Les sommets sont les entiers de 0 à n-1
  * Avec n la taille du tableau
  * On utilise des listes d'adjacence
  * L'arête (i, j) de poids w
  * est représentée par la paire (j, w)
  * dans la liste d'adjacence de i
*)
type wdigraph = (int * float) list array

(*
  * Crée un graphe pondéré de taille n
*)
let make_wdigraph n : wdigraph = Array.make n []

(*
  * Renvoie la taille du graphe
*)
let wdigraph_size (g: wdigraph) : int = Array.length g

(*
  * Renvoie vrai si le graphe a une arête (i, j)
*)
let has_edge (g: wdigraph) (i: int) (j: int) : bool =
  List.exists (fun (k, _) -> k = j) g.(i)

(*
  * Renvoie le poids de l'arête (i, j)
  * Renvoie +inf si l'arête n'existe pas
*)
let wdigraph_get_weight (g: wdigraph) (i: int) (j: int) : float =
  try
    List.assoc j g.(i)
  with Not_found -> infinity

(*
  * Ajoute une arête (i, j) de poids w
*)
let add_edge (g: wdigraph) (i: int) (j: int) (w: float) : unit =
  if not (has_edge g i j) then
    g.(i) <- (j, w) :: g.(i)

(*
  * Renvoie la liste des successeurs de i
*)
let wdigraph_succ (g: wdigraph) (i: int) : (int * float) list = g.(i)

(*
  * Renvoie la liste de toutes les arêtes du graphe
*)
let wdigraph_edges (g: wdigraph) : (int * int * float) list =
  let l = ref [] in
  for i = 0 to wdigraph_size g - 1 do
    let add_i_edge (j, w) = l := (i, j, w) :: !l in
    List.iter add_i_edge g.(i)
  done;
  !l

let grid_index w j i = w*j+i

let print_wall_line (w: int) =
  for i = 0 to w-1 do
    print_string "|   "
  done;
  print_string "|";
  print_newline()

let print_permeable_wall_line (g: wdigraph) (w: int) (j: int) =
  let index = grid_index w j in
  print_string "|";
  for i = 0 to w-2 do
    print_string "   ";
    print_string (if has_edge g (index i) (index (i+1)) then " " else "|")
  done;
  print_string "   |";
  print_newline()

let print_ceiling_line (w: int) =
  print_string "T";
  for i = 0 to w-1 do
    print_string "TTTT";
  done;
  print_newline()

let print_permeable_ceiling_line (g: wdigraph) (w: int) (j: int) =
  let index = grid_index w in
  print_string "T";
  for i = 0 to w-1 do
    print_string (if has_edge g (index j i) (index (j+1) i) then "   " else "TTT");
    print_string "T"
  done;
  print_newline()


let print_grid_graph (g: wdigraph) (w: int) (h: int): unit =
  let n = wdigraph_size g in
  assert (n = w*h);
  print_ceiling_line w;
  for j = 0 to h-2 do
    print_wall_line w;
    print_permeable_wall_line g w j;
    print_wall_line w;
    if j = h-1 then
      print_ceiling_line w
    else
      print_permeable_ceiling_line g w j
  done

let add_horizontal_door g w i j =
  let index = grid_index w j in
  add_edge g (index i) (index i+1) 1.

let add_vertical_door g w i j =
  let index = grid_index w in
  add_edge g (index j i) (index (j+1) i) 1.

let () =
  let w = 5 in
  let h = 4 in
  let grid = make_wdigraph (w*h) in
  for i = 0 to w-2 do
    add_horizontal_door grid w i 0
  done;
  add_vertical_door grid w 4 0;
  add_horizontal_door grid w 3 1;
  add_vertical_door grid w 3 1;
  add_horizontal_door grid w 3 2;
  print_grid_graph grid w h
  
  

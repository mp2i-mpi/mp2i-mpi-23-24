(* Pour exécuter : ocamlopt -o testauto auto.mli auto.ml test_auto.ml && ./testauto *)

open Auto
  
let test_create_size () =
  let a = create 3 in
  assert (size a = 3)

let test_add_trans () =
  let a = create 3 in
  add_trans a 0 'a' 1;
  add_trans a 1 'b' 2;
  assert (trans a 0 'a' = [1]);
  assert (trans a 0 'b' = []);
  assert (trans a 1 'b' = [2]);
  add_trans a 0 'b' 2;
  add_trans a 0 'a' 2;
  assert (trans a 0 'b' = [2]);
  assert (trans a 0 'a' = [1;2])
  
let test_is_det () =
  let a = create 3 in
  add_trans a 0 'a' 1;
  add_trans a 1 'b' 2;
  assert (is_det a);
  add_trans a 0 'a' 2;
  assert (not (is_det a))

let test_has_epsilon () =
  let a = create 3 in
  add_trans a 0 'a' 1;
  add_trans a 1 'b' 2;
  assert (not (has_epsilon a));
  add_eps_trans a 0 1;
  assert (has_epsilon a)

let test_eps_trans() =
  let a = create 3 in
  add_trans a 0 'a' 1;
  add_trans a 1 'b' 2;
  assert (eps_trans a 0 = []);
  add_eps_trans a 0 1;
  assert (eps_trans a 0 = [1]);
  add_eps_trans a 0 1;
  assert (eps_trans a 0 = [1])


let test_copy () =
  let a = create 3 in
  add_trans a 0 'a' 1;
  add_trans a 1 'b' 2;
  let b = copy a in
  assert (trans b 0 'a' = [1]);
  assert (trans b 1 'b' = [2]);
  add_trans a 0 'b' 1;
  assert (trans b 0 'b' = []);
  add_trans b 0 'a' 2;
  assert (trans a 0 'a' = [1])

let test_all_trans () =
  let a = create 3 in
  add_trans a 0 'a' 1;
  add_eps_trans a 0 1;
  add_trans a 0 'a' 0;
  add_trans a 0 'b' 2;
  let result = all_trans a 0 in
  assert (result = (
    [
      ('a', [0; 1]);
      ('b', [2])
    ],
    [1]
  ));
  let opt_result = all_trans_opt a 0 in
  assert (opt_result = [
    (Some 'a', 0);
    (Some 'a', 1);
    (Some 'b', 2);
    (None, 1);
  ])

let test_accept () =
  let a = create 3 in
  add_trans a 0 'a' 1;
  add_trans a 0 'b' 0;
  set_final a 1;
  assert (not (accept a ""));
  assert (accept a "a");
  assert (accept a "bbba");
  assert (not (accept a "bbaba"));
  assert (not (accept a "aba"))

  let test_accept_eps () =
    let a = create 3 in
    add_trans a 0 'a' 1;
    add_trans a 0 'b' 0;
    add_eps_trans a 0 1;
    add_trans a 1 'c' 1;
    set_final a 1;
    assert (accept a "");
    assert (accept a "a");
    assert (accept a "bbba");
    assert (not (accept a "bbaba"));
    assert (not (accept a "aba"));
    assert (accept a "bbbbb");
    assert (accept a "bbbbaccc");
    assert (accept a "bbbbccc")

let tests = [
  "test_insert_sorted_list", test_insert_sorted_list;
  "test_merge_sorted_list", test_merge_sorted_list;
  "test_create_size", test_create_size;
  "test_add_trans", test_add_trans;
  "test_is_det", test_is_det;
  "test_copy", test_copy;
  "test_has_epsilon", test_has_epsilon;
  "test_eps_trans", test_eps_trans;
  "test_all_trans", test_all_trans;
  "test_accept\n", test_accept;
  "test_accept_eps\n", test_accept_eps
]

let run_tests() =
  List.iter (
    fun (name, test) -> (
      print_string name;
      test();
      print_endline " OK"
    )
  ) tests


let () =
  run_tests();
  let a = create 5 in
  add_trans a 0 'a' 1;
  print (Format.std_formatter) a;
  print_dot_to_file ~file:"file.txt" a
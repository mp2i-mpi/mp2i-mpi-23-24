type state = int
(** Le types des états, de simples entiers. *)

type auto
(** Le type des automates finis. Permet de représenter un DFA,
    un NFA ou un εNFA. C'est une structure mutable.
    Les états sont les entiers de 0 à n-1.
    Σ es l'ensemble des caractères représentable
    par le type char.
*)
val create : int -> auto
(** [create n] renvoie un automate à [n] états, sans transition
    ni état acceptant. L'état initial est [0]. *)

val size : auto -> int
(** Renvoie le nombre d'états de l'automate. *)

val is_det : auto -> bool
(** Renvoie [true] si l'automate est déterministe. *)

val has_epsilon : auto -> bool
(** Renvoie [true] si l'automate possède au moins une
    transition spontanée. *)

val trans : auto -> state -> char -> state list
(** Renvoie la liste des états destination pour l'état q et le
    caractère c. La liste est triée et sans doublons. *)

val eps_trans : auto -> state -> state list
(** Renvoie la liste des états destination pour l'état q
    et ε. La liste est triée et sans doublons. *)

val all_trans : auto -> state -> (char * state list) list * state list (* Couple (transitions normales, epsilon-transitions) *)
(** Renvoie toutes les transitions sortantes pour un état donné.
    Chaque liste d'état est triée et sans doublons.
*)

val all_trans_opt : auto -> state -> (char option * state) list
(** Renvoie toutes les transitions sortantes pour un état donné.
    (None, q) indique une transition spontanée vers q,
    (Some c, q) indique une transition pour le caractère c vers q.
*)


val add_trans : auto -> state -> char -> state -> unit
(** [add_trans auto p c q] ajoute la transition à
    l'automate. *)

val add_eps_trans : auto -> state -> state -> unit
(** [add_trans auto p c q] ajoute la transition à
    l'automate. *)

val add_trans_opt : auto -> state -> char option -> state -> unit
(** [add_trans auto p c q] ajoute la transition à
    l'automate. [None] représente ε *)


val copy : auto -> auto

(* val remove_states : auto -> state list -> auto *)
val set_final : auto -> state -> unit
val unset_final : auto -> state -> unit
val is_final : auto -> state -> bool

val accept: auto -> string -> bool
(** [accept auto s] renvoie [true] si l'automate accepte la chaîne
    de caractères [s]. *)


(* val load : string -> auto *)
val print : Format.formatter -> auto -> unit

(* Affichage au format de Graphviz. Cf https://graphviz.org/

   On peut en particulier envoyer le résultat sur `dot -Tps | gv -`
   ou encore sur `dot -Tpdf | zathura -` *)

val print_dot : Format.formatter -> auto -> unit
val print_dot_to_file : file:string -> auto -> unit

(* Internal tests *)

val test_insert_sorted_list : unit -> unit
val test_merge_sorted_list : unit -> unit

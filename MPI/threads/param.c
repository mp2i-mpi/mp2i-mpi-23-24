#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

int x = 0;

void *f(void *arg) {
    char* str = (char*) arg;
    for (int i = 0; i < 1000000; i++) {
        printf("%s%d\n", str, i);
    }
    pthread_exit(NULL);
}

int main() {
    pthread_t t1;
    pthread_t t2;
    pthread_create(&t1, NULL, f, (void*)"A");
    pthread_create(&t2, NULL, f, (void*)"B");
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    printf("x = %d\n", x);
    return 0;
}
let buffer = Queue.create ()
let size = 5
let nb_p = 2
let nb_c = 4

let empty = Semaphore.Counting.make size
let full = Semaphore.Counting.make 0
let m = Mutex.create ()

let producer i = 
  for k = 1 to 10 do
    Semaphore.Counting.acquire empty;
    Mutex.lock m;
    let v = Random.int 100 in
    Printf.printf "%d produit %d \n" i v;
    Queue.push v buffer;
    Mutex.unlock m;
    Semaphore.Counting.release full;
    Thread.yield()
  done

let consumer i =
  for k = 1 to 5 do
    Semaphore.Counting.acquire full;
    Mutex.lock m;
    let v = Queue.pop buffer in
    Printf.printf "%d consomme %d \n" i v;
    Mutex.unlock m;
    Semaphore.Counting.release empty;
    Thread.yield()
  done

  let prods = Array.init nb_p (fun i -> Thread.create producer i)
  let cons = Array.init nb_c (fun i -> Thread.create consumer i)

let () =
  Array.iter Thread.join prods;
  Array.iter Thread.join cons
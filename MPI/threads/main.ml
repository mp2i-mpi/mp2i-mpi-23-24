let n = 20000

let acc = ref 0

let f k =
  for i = 1 to n do
    acc := !acc + 1
  done;
  Thread.exit ()

let t1 = Thread.create f "A"
let t2 = Thread.create f "B"
let () =
  Thread.join t1;
  Thread.join t2;
  Printf.printf ("\n%d !\n") !acc
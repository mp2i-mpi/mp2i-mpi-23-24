let m1 = Mutex.create ()
let m2 = Mutex.create ()

let f1 () =
  Mutex.lock m1;
  Thread.yield ();
  Mutex.lock m2;
  print_endline "f1";
  Mutex.unlock m2;
  Mutex.unlock m1

let f2 () =
  Mutex.lock m1;
  Thread.yield ();
  Mutex.lock m2;
  print_endline "f2";
  Mutex.unlock m2;
  Mutex.unlock m1

let t1 = Thread.create f1 ()
let t2 = Thread.create f2 ()
let () = Thread.join t1; Thread.join t2
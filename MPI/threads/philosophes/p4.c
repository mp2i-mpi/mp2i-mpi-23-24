#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


pthread_mutex_t *baguettes;

sem_t sem;

void *philo(void *arg) {
    int i = *((int*) arg);
    printf("Philosophe %d\n", i);
    while (true) {
        printf("Le philosophe %d pense\n", i);

        sem_wait(&sem);
        pthread_mutex_lock(&baguettes[i]);
        pthread_mutex_lock(&baguettes[(i + 1) % 5]);
        sem_post(&sem);

        printf("Le philosophe %d commence à manger\n", i);
        sched_yield();
        printf("Le philosophe %d mange\n", i);
        sched_yield();
        printf("Le philosophe %d finit de manger\n", i);

        pthread_mutex_unlock(&baguettes[(i + 1) % 5]);
        pthread_mutex_unlock(&baguettes[i]);
    }
}

int main() {
    sem_init(&sem, 0, 4);
    baguettes = malloc(5 * sizeof(pthread_mutex_t));
    for (int i = 0; i < 5; i++) {
        pthread_mutex_init(&baguettes[i], NULL);
    }
    pthread_t philos[5];
    for (int i = 0; i < 5; i++) {
        pthread_create(&philos[i], NULL, philo, &i);
    }
    for (int i = 0; i < 5; i++) {
        pthread_join(philos[i], NULL);
    }
    return 0;
}

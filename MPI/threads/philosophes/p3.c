#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

pthread_mutex_t *baguettes;

void *philo(void *arg) {
    int i = *((int*) arg);
    printf("Philosophe %d\n", i);
    while (true) {
        printf("Le philosophe %d pense\n", i);


        pthread_mutex_lock(&baguettes[i]);
        pthread_mutex_lock(&baguettes[(i + 1) % 5]);

        printf("Le philosophe %d commence à manger\n", i);
        printf("Le philosophe %d mange\n", i);
        printf("Le philosophe %d finit de manger\n", i);

        pthread_mutex_unlock(&baguettes[(i + 1) % 5]);
        pthread_mutex_unlock(&baguettes[i]);
    }
}

int main() {
    baguettes = malloc(5 * sizeof(pthread_mutex_t));
    for (int i = 0; i < 5; i++) {
        pthread_mutex_init(&baguettes[i], NULL);
    }
    pthread_t philos[5];
    for (int i = 0; i < 5; i++) {
        pthread_create(&philos[i], NULL, philo, &i);
    }
    for (int i = 0; i < 5; i++) {
        pthread_mutex_lock(&baguettes[i]);
        
    }
    for (int i = 0; i < 5; i++) {
        pthread_join(philos[i], NULL);
    }
    return 0;
}

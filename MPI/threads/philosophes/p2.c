#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

pthread_mutex_t m;

bool *baguettes;

void *philo(void *arg) {
    int i = *((int*) arg);
    printf("Philosophe %d\n", i);
    while (true) {
        printf("Le philosophe %d pense\n", i);

        while (true) {
            pthread_mutex_lock(&m);
            if (baguettes[i] && baguettes[(i + 1) % 5]) {
                baguettes[i] = false;
                baguettes[(i + 1) % 5] = false;
                pthread_mutex_unlock(&m);
                break;
            }
            pthread_mutex_unlock(&m);
        }

        printf("Le philosophe %d commence à manger\n", i);
        printf("Le philosophe %d mange\n", i);
        printf("Le philosophe %d finit de manger\n", i);

        baguettes[i] = true;
        baguettes[(i + 1) % 5] = true;
    }
}

int main() {
    pthread_mutex_init(&m, NULL);
    pthread_t philos[5];
    baguettes = malloc(5 * sizeof(bool));
    for (int i = 0; i < 5; i++) {
        baguettes[i] = true;
    }
    for (int i = 0; i < 5; i++) {
        pthread_create(&philos[i], NULL, philo, &i);
    }
    for (int i = 0; i < 5; i++) {
        pthread_join(philos[i], NULL);
    }
    return 0;
}

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

pthread_mutex_t m;

void *philo(void *arg) {
    int i = *((int*) arg);
    printf("Philosophe %d\n", i);
    while (true) {
        printf("Le philosophe %d pense\n", i);

        pthread_mutex_lock(&m);
        printf("Le philosophe %d commence à manger\n", i);
        printf("Le philosophe %d mange\n", i);
        printf("Le philosophe %d finit de manger\n", i);
        pthread_mutex_unlock(&m);
    }
}

int main() {
    pthread_mutex_init(&m, NULL);
    pthread_t philos[5];
    for (int i = 0; i < 5; i++) {
        pthread_create(&philos[i], NULL, philo, &i);
    }
    for (int i = 0; i < 5; i++) {
        pthread_join(philos[i], NULL);
    }
    return 0;
}

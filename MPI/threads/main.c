#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int x = 0;

bool want[2] = {false, false};
int turn = 0;

void lock(int i) {
    int other = 1 - i;
    want[i] = true;
    turn = other;
    while (want[other] && turn == other) {}
}

void unlock(int i) {
    want[i] = false;
}

// pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
// pthread_mutex_init(&m, NULL);

void *f(void *arg) {
    int *t_i = (int*) arg;
    for (int i = 0; i < 1000000-1000*(*t_i); i++) {
        // pthread_mutex_lock(&m);
        lock(*t_i);
        x++;
        unlock(*t_i);
        // pthread_mutex_unlock(&m);
    }
    pthread_exit(NULL);
}

int main() {
    pthread_t t1;
    pthread_t t2;
    int t1_i = 0;
    int t2_i = 1;
    pthread_create(&t1, NULL, f, &t1_i);
    pthread_create(&t2, NULL, f, &t2_i);
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    printf("x = %d\n", x);
    return 0;
}
#ifndef NUMEROTATION_H_INCLUDED
#define NUMEROTATION_H_INCLUDED

#include <stdbool.h>

typedef struct numerotation{
    int nb_num; // nombre de numéros possibles
    bool* t_num; // indique pour chaque numéro s'il est présent
} Numerotation;

void affiche_numrt(Numerotation* numrt);
void affiche_numrt_diz(Numerotation* numrt);

Numerotation* create_numrt_from_tab(int* tab, int lg_tab, int nb_num);
void delete_numrt(Numerotation* numrt);

int ind_from_num(int num, Numerotation* numrt);
int prochain_num(int num_0, Numerotation* numrt);
int num_from_ind(int ind, Numerotation* numrt);

#endif // NUMEROTATION_H_INCLUDED

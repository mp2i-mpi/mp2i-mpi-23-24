#ifndef COHERENCE_H_INCLUDED
#define COHERENCE_H_INCLUDED

#include <stdbool.h>

#include "numerotation.h"
#include "produits.h"

bool coherence_numrt_prdts(Numerotation* numrt, Produits* prdts);

#endif // COHERENCE_H_INCLUDED

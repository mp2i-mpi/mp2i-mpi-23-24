#include <assert.h>
#include <stdio.h>

#include "coherence.h"
#include "numerotation.h"
#include "produits.h"

int main(int argc, char* argv[]) {
    bool t_num[] = {
        true, false, false, true, false, true, false, false, false, false,
        true, false, true, false, true, true
    };
    Numerotation numrt = { 16, t_num }; // représente la numérotation 0, 3, 5, 10, 12, 14, 15

    int stocks1[] = { 4, 2 };
    int prix1[] = { 200, 350 };
    Produits prd1 = { 2, stocks1, prix1 };

    int stocks2[] = { 4, 2, 3, 5, 12, 1, 8 };
    int prix2[] = { 200, 350, 125, 430, 50, 150, 100 };
    Produits prd2 = { 7, stocks2, prix2 };

    printf("TEST coherence_numrt_prdts\n");
    assert(!coherence_numrt_prdts(&numrt, &prd1));
    assert(coherence_numrt_prdts(&numrt, &prd2));
    printf("OK\n\n");

    return 0;
}

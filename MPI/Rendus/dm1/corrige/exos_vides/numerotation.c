#include "numerotation.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

void affiche_numrt(Numerotation* numrt) {
    assert(false);
}

void affiche_numrt_diz(Numerotation* numrt) {
    assert(false);
}

Numerotation* create_numrt_from_tab(int* tab, int lg_tab, int nb_num) {
    assert(false);
}

void delete_numrt(Numerotation* numrt) {
    assert(false);
}

int ind_from_num(int num, Numerotation* numrt) {
    assert(false);
}

int prochain_num(int num_0, Numerotation* numrt) {
    assert(false);
}

int num_from_ind(int ind, Numerotation* numrt) {
    assert(false);
}

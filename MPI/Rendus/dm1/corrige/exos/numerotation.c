#include "numerotation.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

void affiche_numrt(Numerotation* numrt) {
    printf("numéros présents : ");
    for(int i = 0; i < numrt->nb_num; ++ i) {
        if (numrt->t_num[i]) {
            printf("%02d ", i);
        }
    }
    printf("\n");
}

void affiche_numrt_diz(Numerotation* numrt) {
    printf("numéros présents :");
    int cur_diz = -1;
    for(int i = 0; i < numrt->nb_num; ++i) {
        if (numrt->t_num[i]) {
            int diz = i / 10;
            if(diz > cur_diz) {
                cur_diz = diz;
                printf("\n");
            }
            printf("%02d ", i);
        }
    }
    printf("\n");
}

Numerotation* create_numrt_from_tab(int* tab, int lg_tab, int nb_num) {
    bool* t_num = (bool*)(malloc(nb_num * (sizeof(bool))));
    for (int i = 0; i < nb_num; ++i) {
        t_num[i] = 0;
    }
    for (int i = 0; i < lg_tab; ++i) {
        t_num[tab[i]] = true;
    }
    Numerotation* res = (Numerotation*)(malloc(sizeof(Numerotation)));
    res->nb_num = nb_num;
    res->t_num = t_num;
    return res;
}

void delete_numrt(Numerotation* numrt) {
    free(numrt->t_num);
    free(numrt);
}

int ind_from_num(int num, Numerotation* numrt) {
    assert(num < numrt->nb_num);

    if(!numrt->t_num[num]) return -1;

    int res = 0;
    for(int i = 0; i < num; ++i) {
        if (numrt->t_num[i]) {
            ++res;
        }
    }
    return res;
}

int prochain_num(int num_0, Numerotation* numrt) {
    for (int i = num_0; i < numrt->nb_num; ++i) {
        if (numrt->t_num[i]) return i;
    }
    return -1;
}

int num_from_ind(int ind, Numerotation* numrt) {
    int cur_num = -1;
    for(int i = 0; i <= ind; ++i) {
        cur_num = prochain_num(cur_num+1, numrt);
        if (cur_num == -1) {
            return -1;
        }
    }
    return cur_num;
}

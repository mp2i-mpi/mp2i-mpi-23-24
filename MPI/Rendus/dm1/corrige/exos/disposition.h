#ifndef DISPOSITION_H_INCLUDED
#define DISPOSITION_H_INCLUDED

#include "numerotation.h"

typedef struct disposition {
    int nb_rg; // nombre de rangées
    int* t_rg; // nombre de produits possibles sur chaque rangée
} Disposition;

void affiche_dispo_simple(Disposition* dispo);
void affiche_dispo_simple_num(Disposition* dispo, Numerotation* numrt);

#endif // DISPOSITION_H_INCLUDED
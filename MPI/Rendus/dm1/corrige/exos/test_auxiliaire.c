#include <stdio.h>

#include "auxiliaire.h"

int main(int argc, char* argv[]) {
    printf("TEST affiche_montant\n");
    affiche_montant(50);
    printf("\n");
    affiche_montant(500);
    printf("\n");
    affiche_montant(5);
    printf("\n");

    return 0;
}
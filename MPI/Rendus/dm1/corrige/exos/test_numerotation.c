#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

#include "numerotation.h"

int main(int argc, char* argv[]) {
    bool t_num[] = {
        true, false, false, true, false, true, false, false, false, false,
        true, false, true, false, true, true
    };
    Numerotation numrt = { 16, t_num }; // représente la numérotation 0, 3, 5, 10, 12, 14, 15

    printf("TEST affiche_numrt\n");
    affiche_numrt(&numrt);
    printf("\n");

    printf("TEST affiche_numrt_diz\n");
    affiche_numrt_diz(&numrt);
    printf("\n");

    printf("TEST create_numrt_from_tab\n");
    int tab[] = { 0, 3, 5, 10, 12, 14, 15 }; // même numérotation qu'avant mais en représentation tabulaire
    Numerotation* numrt_from_tab = create_numrt_from_tab(tab, 7, 16);
    assert(numrt_from_tab->nb_num == numrt.nb_num);
    for(int i = 0; i < numrt_from_tab->nb_num; ++i) {
        assert(numrt_from_tab->t_num[i] == numrt.t_num[i]);
    }
    delete_numrt(numrt_from_tab);
    printf("OK\n\n");

    printf("TEST ind_from_num\n");
    assert(ind_from_num(0, &numrt) == 0);
    assert(ind_from_num(3, &numrt) == 1);
    assert(ind_from_num(5, &numrt) == 2);
    assert(ind_from_num(10, &numrt) == 3);
    assert(ind_from_num(12, &numrt) == 4);
    assert(ind_from_num(14, &numrt) == 5);
    assert(ind_from_num(15, &numrt) == 6);

    assert(ind_from_num(2, &numrt) == -1);
    assert(ind_from_num(4, &numrt) == -1);
    assert(ind_from_num(11, &numrt) == -1);
    printf("OK\n\n");

    printf("TEST prochain_num\n");
    assert(prochain_num(0, &numrt) == 0);
    assert(prochain_num(1, &numrt) == 3);
    assert(prochain_num(2, &numrt) == 3);
    assert(prochain_num(3, &numrt) == 3);
    assert(prochain_num(4, &numrt) == 5);
    assert(prochain_num(5, &numrt) == 5);
    assert(prochain_num(11, &numrt) == 12);
    assert(prochain_num(15, &numrt) == 15);

    assert(prochain_num(16, &numrt) == -1);
    assert(prochain_num(18, &numrt) == -1);
    printf("OK\n\n");

    printf("TEST num_from_ind\n");
    assert(num_from_ind(0, &numrt) == 0);
    assert(num_from_ind(1, &numrt) == 3);
    assert(num_from_ind(2, &numrt) == 5);
    assert(num_from_ind(3, &numrt) == 10);
    assert(num_from_ind(4, &numrt) == 12);
    assert(num_from_ind(5, &numrt) == 14);
    assert(num_from_ind(6, &numrt) == 15);
    assert(num_from_ind(7, &numrt) == -1);
    assert(num_from_ind(8, &numrt) == -1);
    printf("OK\n\n");

    return 0;
}
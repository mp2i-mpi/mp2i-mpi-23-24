#include "auxiliaire.h"

#include <stdio.h>

void affiche_montant(int montant) {
    int centimes = montant % 100;
    int euros = montant / 100;
    printf("%d€%02d", euros, centimes);
}

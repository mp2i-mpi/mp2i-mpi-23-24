#include <stdio.h>

#include "produits.h"

int main(int argc, char* argv[]) {
    int stocks[] = { 4, 2 };
    int prix[] = { 200, 350 };
    Produits prd = { 2, stocks, prix };

    printf("TEST nb_produits\n");
    nb_produits(&prd);
    printf("\n");

    printf("\nTEST valeur_globale\n");
    valeur_globale(&prd);
    printf("\n");

    printf("\nTEST affiche_info_globale\n");
    affiche_info_globale(&prd);

    printf("\nTEST affiche_info_detaillee\n");
    affiche_info_detaillee(&prd);

    return 0;
}
#include "coherence.h"

bool coherence_numrt_prdts(Numerotation* numrt, Produits* prdts) {
    int num_presents = 0;
    for (int i = 0; i < numrt->nb_num; ++i) {
        if (numrt->t_num[i]) ++num_presents;
    }
    return num_presents == prdts->nb_p;
}

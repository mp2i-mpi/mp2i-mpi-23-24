#include "disposition.h"

#include <stdio.h>

void affiche_dispo_simple(Disposition* dispo) {
    printf("----------------------\n");
    for (int i = 0; i < dispo->nb_rg; ++i) {
        printf("rangée %d : %d produits\n", i+1, dispo->t_rg[i]);
    }
}

void affiche_dispo_simple_num(Disposition* dispo, Numerotation* numrt) {
    printf("----------------------\n");
    int num_courant = -1;
    for (int i = 0; i < dispo->nb_rg; ++i) {
        printf("rangée %d : ", i+1);
        for (int j = 0; j < dispo->t_rg[i]; ++j) {
            num_courant = prochain_num(num_courant+1, numrt);
            if (num_courant != -1) {
                printf("%d ", num_courant);
            }
        }
        printf("\n");
    }
}

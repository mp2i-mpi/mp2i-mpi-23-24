#ifndef PRODUITS_H_INCLUDED
#define PRODUITS_H_INCLUDED

typedef struct produits {
    int nb_p; // nombre de produits différents
    int* t_stock; // tableau de nb_p entiers indiquant le stock de chaque produit
    int* t_prix; // tableau de nb_p entiers indiquant le prix de chaque produit
} Produits;

void nb_produits(Produits* prd);
void valeur_globale(Produits* prd);
void affiche_info_globale(Produits* prd);
void affiche_info_detaillee(Produits* prd);

#endif // PRODUITS_H_INCLUDED

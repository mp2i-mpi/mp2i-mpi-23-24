#include "produits.h"

#include <stdio.h>

#include "auxiliaire.h"

void nb_produits(Produits* prd) {
    int res = 0;
    for (int i = 0; i < prd->nb_p; ++i) {
        res += prd->t_stock[i];
    }
    printf("%d", res);
}

void valeur_globale(Produits* prd) {
    int res = 0;
    for (int i = 0; i < prd->nb_p; ++i) {
        res += prd->t_stock[i] * prd->t_prix[i];
    }
    affiche_montant(res);
}

void affiche_info_globale(Produits* prd) {
    printf("Il y a ");
    nb_produits(prd);
    printf(" produits totalisant ");
    valeur_globale(prd);
    printf(".\n");
}

void affiche_info_detaillee(Produits* prd) {
    printf("Liste des produits\n"
            "====================\n"
            "prix | quantité\n"
            "--------------------\n"
    );
    for(int i = 0; i < prd->nb_p; ++i) {
        affiche_montant(prd->t_prix[i]);
        printf(" | %d\n", prd->t_stock[i]);
    }
    printf("--------------------\n");
}



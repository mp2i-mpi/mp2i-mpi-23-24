#include <assert.h>
#include <stdio.h>

#include "disposition.h"

int main(int argc, char* argv[]) {
    int t_rg[] = { 3, 3, 2 };
    Disposition dispo = { 3, t_rg };

    printf("TEST affiche_dispo_simple\n");
    affiche_dispo_simple(&dispo);
    printf("\n");

    printf("TEST affiche_dispo_simple_num\n");
    int tab[] = { 11, 12, 13, 21, 23, 25, 30, 32 };
    Numerotation* numrt = create_numrt_from_tab(tab, 8, 33);
    affiche_dispo_simple_num(&dispo, numrt);
    delete_numrt(numrt);
    printf("\n");

    return 0;
}

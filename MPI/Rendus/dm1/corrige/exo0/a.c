#include "a.h"

#include <stdio.h>

// version 1
void affiche_a(int n) {
    for (int i = 0; i < n; ++i) {
        printf("a");
    }
}

// version avec affiche_point

// void affiche_a(int n) {
//     affiche_point();
//     for (int i = 0; i < n; ++i) {
//         printf("a");
//     }
// }

// version avec espaces

// void affiche_a(int n) {
//     for (int i = 0; i < n; ++i) {
//         printf("a ");
//     }
// }

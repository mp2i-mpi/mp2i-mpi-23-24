//test.c

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "distributeur.c"

void main () {
    int t_stock[16] = {2, 0, 3, 1, 5, 3, 1, 4, 3, 6, 2, 0, 7, 1, 4, 7} ;
    int t_prix[16] = {210, 240, 110, 320, 70, 50, 220, 140, 320, 70, 270, 110, 250, 280, 200, 160} ;
    struct produits prd = {16, t_stock, t_prix} ;

    bool t_num [40] = {false, true, true, true, false, false , true, false, false, false,   
    false , true, false, true, false,true , false, false, false, false,
    false , true, false, true, true,true , true, false, false, false,
    false , true, false, true, true,false , false, false, true, false, } ;
    struct numerotation numrt = {40, t_num} ;

    int t_rg[4] = {4, 3, 5, 4} ;
    struct disposition dispo = {4 , t_rg} ;

    int t_pc[6] = {8, 13, 7, 18, 16, 23} ;
    struct caisse caisse_test = {t_pc} ;
    struct distributeur distrib = {prd, numrt, dispo, caisse_test} ;
    // affiche_dispo_num (dispo, numrt) ;
    // selection_produit (distrib) ;
    // affiche_info_detaillee (prd) ;
    distrib_final (distrib) ;
    
    // struct caisse moné = prend_monnaie() ;
    // affiche_caisse(moné) ;
    printf ("\n") ;
}
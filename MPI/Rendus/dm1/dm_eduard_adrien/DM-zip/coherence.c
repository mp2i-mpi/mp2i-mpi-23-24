//coherence.c 
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "structures.h"



bool coherence_numrt_dispo (struct disposition d, struct numerotation n) {
    if (d.nb_rg != n.nb_num) {
        return (false) ;
    }
    int i = 1 ;

    while (i < d.nb_rg) {
        int produits_rangee = 0 ;

        for (int j = 1 ; j < (n . nb_num) ; j++) {
            if (n.t_num [i*10+j]) {
                produits_rangee += 1 ;
            }
        }
        if (produits_rangee != (d.t_rg) [i-1]) {
            return (false) ;
        }

        i += 1 ;
    }
    return (true) ;
}



void main () {

};


bool coherence_numrt_prdts(Numerotation *numrt, struct produits *prd)
{
    int cont=0;
    for (int i=0; i<numrt->nb_num ; i++){
    if (numrt->t_num[i]==true){
        cont++; 
    }
  }
    return cont==prd->nb_p;
}
//disposition.c
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>



void affiche_dispo_simple (struct disposition d) {
    int i = 0 ;
    printf ("\n") ;
    printf ("----------------------") ;
    
    while (i < d.nb_rg) {
        printf("\n") ;
        printf (" rangée %d : %d produits", (i+1), (d.t_rg) [i]) ;
        i += 1 ;
    }
}

void affich_dispo_simple_num (struct disposition d, struct numerotation numrt) {
    int rangee = 1 ;
    int colonne = 1 ;
    printf("\n ----------------------") ;

    while (rangee <= d.nb_rg) {
        colonne = 1 ;
        printf("\n rangée %d :", rangee) ;
        rangee += 1 ;

        while (colonne < 10) {
            if (numrt.t_num [10*rangee + colonne]) {
                printf("%d ", (10*rangee + colonne)) ;
            }
            colonne += 1 ;
        }

        printf("\n") ;
    }
}

int pgcd (int a, int b) {
    if (b == 0) {
        return a ;
    }
    return pgcd(b , a%b) ;
}

int ppcm (int a , int b) {
    return (a * b / pgcd(a,b)) ;
}

int nbc_elem_dispo (struct disposition d) {
    int max = (d.t_rg)[0] ;
    if (d.nb_rg == 1) {
        return max ;
    }

    for (int j = 1 ; j < d.nb_rg ; j++) {
        max = ppcm (max, (d.t_rg)[j]) ;
    }
    return max ;
}

void affiche_trait (int n) {
    for (int i = 0 ; i < n ; i++) {
        printf("_") ;
    }
}

void affiche_case_vide (int n) {
    printf("[") ;
    affiche_trait (n - 2) ;
    printf ("]") ;
}

void affiche_ligne_vide (int nbc_elem, int nb_cases) {
    int n = (nbc_elem / nb_cases) ;
    for (int i = 0; i < nb_cases; i++) {
        affiche_case_vide (n) ;
    }
}

void affiche_dispo_vide (struct disposition d) {
    int nbc_elem = nbc_elem_dispo (d) ;
    printf ("\n") ;
    affiche_trait (nbc_elem) ;

    for (int i = 0 ; i < (d.nb_rg) ; i++) {
        printf ("\n") ;
        affiche_ligne_vide (nbc_elem, (d. t_rg)[i]) ;
    }
    printf ("\n") ;
    affiche_trait (nbc_elem) ;
}

void affiche_case_num (int n, int num) {
    printf ("[") ;
    
    for (int i = 0 ; i < (n - 2) ; i++) {
        printf (" ") ;
    }
    if (num < 10) { printf ("0");}
    printf("%d", num) ;

    for (int i = 0 ; i < (n - 2) ; i++) {
        printf (" ") ;
    }
    printf ("]") ;
}

int affiche_ligne_num (int nbc_elem, int nb_cases, struct numerotation numrt, int num_0) {
    printf ("\n") ;
    int i = num_0 ;
    int compteur = nb_cases ;

    while (compteur > 0) {
        if ((numrt . t_num) [i] ) {
            compteur -= 1 ;
            affiche_case_num (nbc_elem/nb_cases , i) ;
        }
        i += 1 ;
    }
    return i ;
}

void affiche_dispo_num (struct disposition d, struct numerotation numrt) {
    int nbc_elem = nbc_elem_dispo (d) ;
    printf ("\n") ;
    affiche_trait (2 * nbc_elem) ;

    for (int j= 0 ; j < d.nb_rg ; j++) {
        printf ("\n") ;
        int unite = affiche_ligne_num (nbc_elem, (d.t_rg)[j], numrt, j * 10) ; //il semble que l'on aie pas besoin que num_0 soit un numéro valide ?
                                                 // on ne s'occupe donc pas de trouver le num_0 suivant
    } 

    printf ("\n") ;
    affiche_trait (2 * nbc_elem) ;
}


//numerotation.c

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>



/* 1 */

void affiche_numrt(struct numerotation *numrt)
{
    printf("numéros présents : ");
    for (int i = 0; i < (numrt->nb_num) ; i++)
    {
        if ( (numrt->t_num) [i])
        {
            printf("%d ", i);
        }
    }
    printf("\n");
}

/* 2 */

void affiche_numrt_diz(struct numerotation *numrt)
{
    printf("numéros présents : ");
    for (int i = 0; i < (numrt->nb_num) /10; i++){
        bool vide=false;
        for (int j =i*10; j <i*10+10 ; j++){
            if ( (numrt -> t_num) [j]==true){
                vide=true;
                printf("%d ", j);
            }
        }
        if (vide==true){
            printf("\n");
        }
    }
}

/* 3 */

struct numerotation create_numrt_from_tab(int *tab, int lg_tab, int nb_num){
    bool t_num[nb_num];
    for (int j=0 ; j<nb_num; j++){
        t_num[j]=false;
    }
    for (int i=0 ; i<lg_tab; i++){
            t_num[tab[i]] == true;
    }
    struct numerotation manumerotation;
    manumerotation.nb_num = nb_num;
    manumerotation.t_num = t_num; 
    return manumerotation;
}

/* 4 */

int ind_from_num(int num, struct numerotation numrt)
{
    int cont=0;
    if ( (numrt.t_num) [num]==false){
            return -1;
        }
    for (int i=0; i<=num; i++){
        if (numrt.t_num[i]){
            cont++;
    }
    }
    return (cont - 1);
}

/* 5 */

int prochain_num(int num_0, struct numerotation *numrt)
{
    for (int i=num_0; i<numrt->nb_num;i++){
        if ( (numrt -> t_num) [i]==true){
            return numrt->t_num[i];
        }
    }
    return -1;
} 

/* 6 */

int num_from_ind(int ind, struct numerotation *numrt)
{   
    int cont=0;
    for (int i=0 ; i< (numrt -> nb_num) ; i++){
        if ( (numrt -> t_num) [i]){
            cont+= 1;
        }
        if (cont==ind+1){
            return i;
        }
    }
    return -1;
}



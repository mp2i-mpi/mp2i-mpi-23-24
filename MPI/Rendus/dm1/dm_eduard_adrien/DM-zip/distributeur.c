//distributeur.c
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "structures.h"

#include  "produits.c"
#include  "numerotation.c"
#include  "disposition.c"
#include "caisse.c"




int selection_produit (struct distributeur distrib) {
    affiche_dispo_num (distrib . dispo , distrib . numrt) ;
    printf("\n Saisissez un numéro de produit \n") ;
    int numero = 0 ;

    while (numero == 0) {    
        scanf("%d", &numero) ;
        if (!   (((distrib.numrt) . t_num) [numero])  ) {
                printf ("\n Numéro non valide. Réessayez. \n") ;
                numero = 0 ;
            }
        else {
            if (((distrib . prd) . t_stock) [ind_from_num (numero, distrib.numrt)]  == 0) {
                printf (" \nCe produit n’est plus en stock, désolé.\nSi vous souhaitez autre chose, tapez le numéro correspondant.\n") ;
                numero = 0 ;
            }
            else {
                return (ind_from_num (numero, distrib.numrt )) ;
            }
        }
    }


}

bool transaction (struct distributeur distrib) {


    struct caisse caisse_initiale = distrib.c ;
    int ind = selection_produit (distrib) ;
    int entier = 0 ;
    printf ("\nCe produit coûte ") ;
    affiche_montant (distrib.prd.t_prix[ind]) ;
    
    struct caisse monnaie_donnee = prend_monnaie() ; 
    struct caisse caisse_temporaire = caisse_initiale ; //sert à effectuer les opérations sur la caisse. Si l'opération est interrompue, on garde caisse_initiale, sinon on la remplace par caisse_temporaire.



    if (solde(monnaie_donnee) < (((distrib .prd). t_prix) [ind] )  ) {
        printf ("\n Montant insuffisant.") ;
        printf ("\n Si vous souhaitez recommencer l'opération, tapez 0. Tapez 1 sinon. \n") ;
        scanf ("%d", &entier) ;

        if (entier = 0) {
            rend_monnaie (monnaie_donnee, solde(monnaie_donnee)) ;
            return true ;
        }

        rend_monnaie(monnaie_donnee, solde(monnaie_donnee)) ;
        return false ;
    }

    ajoute_caisse(caisse_temporaire, monnaie_donnee) ;

    if (!   (caisse_suffit (caisse_temporaire, solde(monnaie_donnee)))   ) {
        printf("\n Le distributeur ne peut rendre la monnaie.") ;
        printf ("\n Si vous souhaitez recommencer l'opération, tapez 0. Tapez 1 sinon. \n") ;
        scanf ("%d", &entier) ;

        if (entier = 0) {
            rend_monnaie (monnaie_donnee, solde(monnaie_donnee)) ;
            return true ;
        }

        rend_monnaie(monnaie_donnee, solde(monnaie_donnee)) ;
        return false ;
    }
    

    rend_monnaie(caisse_temporaire, ( solde(monnaie_donnee) - ((distrib.prd).t_prix) [ind])) ;
    (distrib.prd).t_stock [ind] -= 1 ;
    
    caisse_initiale = caisse_temporaire ;
    printf ("\n Si vous souhaitez acheter d'autres articles, tapez 0. Tapez 1 sinon. \n") ;
    scanf ("%d", &entier) ;

    if (entier = 0) {
        return true ;
    }
    return false ;
}


void distrib_final( struct distributeur distrib) {
    if (transaction(distrib)) {distrib_final(distrib);}
}





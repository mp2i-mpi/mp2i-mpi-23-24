//structures.h
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>



 struct produits {
    int nb_p;
    int* t_stock;
    int* t_prix;
}  ;

struct numerotation {
    int nb_num ;
    bool* t_num ;
}  ;

struct disposition {
    int nb_rg ;
    int* t_rg ;
} ;

struct caisse {
    int* t_pc ;
} ;

struct distributeur {
    struct produits prd ;
    struct numerotation numrt ;
    struct disposition dispo ;
    struct caisse c;
} ;



void affiche_montant (int n) {
    int euros = n/100 ;
    int centimes = n-(100* euros );
    printf("%d€ %d", euros, centimes) ;
    if (centimes < 10) {
        printf ("0") ;
    }
}

// void main () {}



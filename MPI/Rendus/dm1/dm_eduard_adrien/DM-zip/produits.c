//produits.c
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>





int somme (int* l,int n) {
    int res = 0 ;
    int i = 0 ;
    while (i < n) {
        res = res+ l[i] ;
        i += 1 ;
    }
    return res ;
} 


void nb_produits (struct produits prd) {

    int res = somme (prd.t_stock, prd.nb_p) ;
    printf ( "\n ---------- \n   il y a %d produits en stock \n ----------", res) ;
} ;


void valeur_globale (struct produits prd) {
    int res = 0 ;
    int i = 0 ;
    int n = prd.nb_p ;

    while (i < n) {
        res = res+ (prd.t_stock)[i] * (prd.t_prix) [i] ; ;
        i += 1 ;
    }
    printf ( "\n ---------- \n le stock a une valeur de ") ;
    affiche_montant (res) ;
    printf ( "\n ----------") ;

} ;

void affiche_info_globale (struct produits prd) {

    int nb = somme (prd.t_stock, prd.nb_p) ;
    int res = 0 ;
    int i = 0 ;

    while (i < nb) {
        res = res+ (prd.t_stock)[i] * (prd.t_prix) [i] ; 
        i += 1 ;
    } ;

    printf ("\n Il y a %d produits en stock, de valeur totale %d", nb, res) ;
}



void affiche_info_detaillee (struct produits prd) {
    printf ( "\n ========== \n   prix | quantité \n ----------") ;
    int i = 0 ;

    while (i < prd.nb_p) {
        printf("\n") ;
        affiche_montant ((prd.t_prix) [i]);
        printf (" | ") ;
        printf ("%d", (prd.t_stock) [i]) ;
        i += 1 ;
    };
    
    printf ("\n ---------- ") ;
}



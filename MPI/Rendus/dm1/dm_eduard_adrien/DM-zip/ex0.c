// ex0.c

/* 1 */
void affiche_a(int n) {
    for (int i = 0; i < n; i++) {
        printf("a");
    }
}

/* 2 */

int main(int argc, int *argv[]) {
    affiche_a(argc);
    return 0;
}

/* 3 */

/* Message renvoyé après compilation : 

/usr/bin/ld: /usr/lib/gcc/x86_64-linux-gnu/11/../../../x86_64-linux-gnu/Scrt1.o: in function `_start':
(.text+0x1b): undefined reference to `main'
collect2: error: ld returned 1 exit status

Ceci est cohérent avec l'énoncé.
*/

/* 4 */

/* Un fichier a.o a bien été créé dans mon dossier. */

/* 5 */

/* Aucune erreur n'apparaît. */

/* 6 */

/* Message renvoyé après compilation :

a.c: In function ‘affiche_a’:
a.c:11:5: warning: implicit declaration of function ‘affiche_point’; did you mean ‘affiche_a’?  
-Wimplicit-function-declaration]
   11 |     affiche_point();
      |     ^~~~~~~~~~~~~
Ceci est bien cohérent avec l'énoncé.  
*/

/* 7 */

/* OK */

/* 8 */

/*
Message renvoyé après compilation :

/usr/bin/ld: /tmp/ccVeFXAe.o: in function `main':
test_a.c:(.text+0x19): undefined reference to `affiche_a'
collect2: error: ld returned 1 exit status

C'est bien cohérent. 
*/

/* 9 */

/* Un fichier test_a a bien été créé dans mon dossier. */

/* 10 */

void affiche_a(int n) {
    for (int i = 0; i < n; i++) {
        printf("a ");
    }
}

/* 11 */

/* Ce qui s'affiche est bien gcc -c a.c -o a.o et le ficier a.o a bien été créé dans 
mon dossier. */

/* 12 */

/* Ce qui est renvoyé est bien : gcc -c a.c -o a.o puis gcc test_a.c a.o -o test_a et
les fichiers a.o et test_a ont bien été créés. */

/* 13 */

/* Ce qui est renvoyé est bien : gcc test_a.c a.o -o test_a et
le fichier test_a a bien été créé. */

/* 14 */

/* Ce qui est renvoyé est bien gcc -c a.c -o a.o puis gcc test_a.c a.o -o test_a avec
la création du fichier test_a. */

/* 15 */

/* La commande make clean a bien supprimé les fichiers compilés. */



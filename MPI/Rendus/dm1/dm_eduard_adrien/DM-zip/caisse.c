//caisse.c

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>



const int nb_pc = 6 ;
const int val_pc[6] = {200, 100, 50, 20, 10, 5};



/* 1 */

int solde(struct caisse c) {
    int somme = 0;
    for (int i = 0; i < nb_pc; i++) {
        somme += c.t_pc[i] * val_pc[i];
    }
    return somme;
}

/* 2 */

void affiche_caisse(struct caisse c){
    printf("\npièces de 2€00 : %d", c.t_pc[0]);
    printf("\npièces de 1€00 : %d", c.t_pc[1]);
    printf("\npièces de 0€50 : %d", c.t_pc[2]);
    printf("\npièces de 0€20 : %d", c.t_pc[3]);
    printf("\npièces de 0€10 : %d", c.t_pc[4]);
    printf("\npièces de 0€05 : %d", c.t_pc[5]);
}

/* 3 */

void ajoute_caisse (struct caisse c1, struct caisse c2) {
    for (int i=0 ; i<nb_pc; i++){
        c1.t_pc[i]+=c2.t_pc[i];
    }
}

/* 4 */

// struct caisse prend_monnaie(){
//     // int vide[nb_pc]; 

//     struct caisse macaisse;;
//     for (int i=0 ; i <6; i++) {macaisse.t_pc[i] = 0;}
//     for (int i=0 ; i <6; i++) {printf("\n %d", macaisse.t_pc[i]);}
//     for (int i=0; i<6; i++){
//         int valeur;
//         printf("\n Combien de pièces de %d€%d%d voulez-vous insérer dans la machine ?", val_pc[i]/100, (val_pc[i]%100)/10, (val_pc[i]%100)%10);
//         scanf("%d", &valeur);
//         macaisse.t_pc[i]=valeur;
//     }
//     return macaisse;
// }

struct caisse prend_monnaie(){
    int vide[nb_pc];
    struct caisse macaisse ;
    macaisse.t_pc=vide;
    for (int i=0; i<nb_pc; i++){
        int valeur;
        printf("\nCombien de pièces de %d€%d%d voulez-vous insérer dans la machine ?", val_pc[i]/100, (val_pc[i]%100)/10, (val_pc[i]%100)%10);
        scanf("%d", & valeur);
        vide[i]=valeur;
    }
    return macaisse;
}

/* 5 */

bool caisse_suffit(struct caisse c, int m){
    int cont=m;
    for (int i=0; i<nb_pc; i++){
        int nb_pieces=cont/val_pc[i];
        if (nb_pieces>c.t_pc[i]){
            nb_pieces=c.t_pc[i];
        }
        cont-=nb_pieces*val_pc[i];
        if (cont==0){
            return true;
        }
    }
    return false;
}

/* 6 */





void rend_monnaie(struct caisse c, int m){
    int cont=m;
    for (int i=0; i<nb_pc; i++){
        int nb_pieces=cont/val_pc[i];
        if (nb_pieces>c.t_pc[i]){
            nb_pieces=c.t_pc[i];
        }
        cont-=nb_pieces*val_pc[i];
        c.t_pc[i] -= nb_pieces ;
        for (int j=0; j<nb_pieces; j++){
            printf("\n une pièce de %d€%d%d", val_pc[i]/100, (val_pc[i]%100)/10, (val_pc[i]%100)%10);
        }
    }
    printf ("\n") ;
}
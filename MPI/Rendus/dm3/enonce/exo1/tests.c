#include <assert.h>

#include "list.h"

void test_create() {
    list* l = list_create();
    assert(list_is_empty(l));
}

void test_push_front() {
    list* l = list_create();
    list_push_front(l, 1);
    list_push_front(l, 2);
    list_push_front(l, 3);
    int values[] = {3, 2, 1};
    assert(list_equals(l, values, 3));
}

void test_push_back() {
    list* l = list_create();
    list_push_back(l, 1);
    list_push_back(l, 2);
    list_push_back(l, 3);
    int values[] = {1, 2, 3};
    assert(list_equals(l, values, 3));
}

void test_pop_front() {
    list* l = list_create();
    list_push_back(l, 1);
    list_push_back(l, 2);
    list_push_back(l, 3);
    assert(list_pop_front(l) == 1);
    int values[] = {2, 3};
    assert(list_equals(l, values, 2));
}

void test_pop_back() {
    list* l = list_create();
    list_push_back(l, 1);
    list_push_back(l, 2);
    list_push_back(l, 3);
    assert(list_pop_back(l) == 3);
    int values[] = {1, 2};
    assert(list_equals(l, values, 2));
}

void test_size() {
    list* l = list_create();
    assert(list_size(l) == 0);
    list_push_back(l, 1);
    assert(list_size(l) == 1);
    list_push_back(l, 2);
    assert(list_size(l) == 2);
    list_push_back(l, 3);
    assert(list_size(l) == 3);
    list_pop_front(l);
    assert(list_size(l) == 2);
    list_pop_back(l);
    assert(list_size(l) == 1);
    list_pop_back(l);
    assert(list_size(l) == 0);
}

void test_get() {
    list* l = list_create();
    list_push_back(l, 1);
    list_push_back(l, 2);
    list_push_back(l, 3);
    assert(list_get(l, 0) == 1);
    assert(list_get(l, 1) == 2);
    assert(list_get(l, 2) == 3);
}

void test_clear() {
    list* l = list_create();
    list_push_back(l, 1);
    list_push_back(l, 2);
    list_push_back(l, 3);
    list_clear(l);
    assert(list_is_empty(l));
}

void test_insert() {
    list* l = list_create();
    list_insert(l, 1, 0);
    list_insert(l, 3, 1);
    list_insert(l, 2, 1);
    int values[] = {1, 2, 3};
    assert(list_equals(l, values, 3));
}

int main() {
    test_create();
    test_push_front();
    test_push_back();
    test_pop_front();
    test_pop_back();
    test_size();
    test_get();
    test_clear();
    test_insert();
    return 0;
}

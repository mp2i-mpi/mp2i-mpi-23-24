(*
  * Représente un graphe pondéré
  * Les sommets sont les entiers de 0 à n-1
  * Avec n la taille du tableau
  * On utilise des listes d'adjacence
  * L'arête (i, j) de poids w
  * est représentée par la paire (j, w)
  * dans la liste d'adjacence de i
*)
type wdigraph = (int * float) list array

(*
  * Crée un graphe pondéré de taille n
*)
let make_wdigraph n : wdigraph = Array.make n []

(*
  * Renvoie la taille du graphe
*)
let wdigraph_size (g: wdigraph) : int = Array.length g

(*
  * Renvoie vrai si le graphe a une arête (i, j)
*)
let has_edge (g: wdigraph) (i: int) (j: int) : bool =
  List.exists (fun (k, _) -> k = j) g.(i)

(*
  * Renvoie le poids de l'arête (i, j)
  * Renvoie +inf si l'arête n'existe pas
*)
let wdigraph_get_weight (g: wdigraph) (i: int) (j: int) : float =
  try
    List.assoc j g.(i)
  with Not_found -> infinity

(*
  * Ajoute une arête (i, j) de poids w
*)
let add_edge (g: wdigraph) (i: int) (j: int) (w: float) : unit =
  if not (has_edge g i j) then
    g.(i) <- (j, w) :: g.(i)

(*
  * Renvoie la liste des successeurs de i
*)
let wdigraph_succ (g: wdigraph) (i: int) : (int * float) list = g.(i)

(*
  * Renvoie la liste de toutes les arêtes du graphe
*)
let wdigraph_edges (g: wdigraph) : (int * int * float) list =
  let l = ref [] in
  for i = 0 to wdigraph_size g - 1 do
    let add_i_edge (j, w) = l := (i, j, w) :: !l in
    List.iter add_i_edge g.(i)
  done;
  !l

(*
  * Applique l'algorithme de Floydw-Warshall au graphe g
  * Renvoie une matrice des plus courtes distances
*)
let floyd_warshall (g: wdigraph) : float array array = failwith "TODO" 

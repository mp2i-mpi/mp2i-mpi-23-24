#ifndef STACK_H_INCLUDED
#define STACK_H_INCLUDED

#include <stdbool.h>

#include "list.h"

typedef list stack;

stack* stack_create();
bool stack_is_empty(stack* s);
void stack_destroy(stack* s);
void stack_push(stack* s, int value);
int stack_pop(stack* s);

#endif // STACK_H_INCLUDED

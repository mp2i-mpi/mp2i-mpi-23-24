#ifndef QUEUE_H_INCLUDED
#define QUEUE_H_INCLUDED

#include <stdbool.h>

#include "list.h"

typedef list queue;

queue* queue_create();
bool queue_is_empty(queue* s);
void queue_destroy(queue* s);
void queue_push(queue* s, int value);
int queue_pop(queue* s);

#endif // QUEUE_H_INCLUDED

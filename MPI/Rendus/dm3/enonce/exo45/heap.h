#ifndef HEAP_H_INCLUDED
#define HEAP_H_INCLUDED

#include <stdbool.h>

// Structure pour représenter un élément du tas
typedef struct HeapEntry {
    // Identifiant permettant d'associer l'élément à une donnée
    // (stockée par ailleurs ; par exemple, cela peut être
    // l'indice d'un tableau de données)
    int id;

    // Valeur "de priorité" qui sert pour la relation
    // d'ordre entre les éléments du tas
    int value;
} heap_entry;

// On interprète les tableaux comme des tas en considérant
// que les enfants d'un élément d'indice i
// sont les éléments d'indices 2*i+1 et 2*i+2

// Un tableau a donc une structure de tas si et seulement si
// Pour tout i < size/2, tab[i].value >= tab[2*i+1].value et tab[i].value >= tab[2*i+2].value

// Vérifie si le tableau a une structure de tas
bool is_heap(heap_entry *tab, int size);

// Ajoute un élément au tas
// On suppose que le tableau a une structure de tas jusqu'à l'indice heap_size (exclus)
// On suppose que le tableau a une capacité d'au moins heap_size+1
// Après l'appel, le tableau a une structure de tas jusqu'à l'indice heap_size+1 (exclus)
void heap_push(heap_entry *tab, int *heap_size, int id, int value);

// Supprime le plus grand élément du tas
// On suppose que le tableau a une structure de tas jusqu'à l'indice heap_size (exclus)
// On suppose que heap_size > 0
// Après l'appel, le tableau a une structure de tas jusqu'à l'indice heap_size-1 (exclus)
heap_entry* heap_pop(heap_entry *tab, int *heap_size);

// Transforme un tableau en tas
void make_heap(heap_entry *tab, int size);
// Une stratégie possible :
// On fait une boucle sur les éléments du tableau
// Qui a pour but de préservier l'invariant
// "le tableau est un tas jusqu'à l'indice i"
// Pour cela, au tour de boucle n°i on peut faire un appel à heap_push
// Avec l'élément d'indice i et heap_size = i, pour insérer cet élément
// Et ainsi agrandir le tas

// Implémente le tri par tas ; on pourra s'inspirer des fonctions précédentes
void heap_sort(int *tab, int size);

#endif // HEAP_H_INCLUDED

#ifndef PQUEUE_H_INCLUDED
#define PQUEUE_H_INCLUDED

// Cf heap_entry dans heap.h
typedef struct PQueueEntry {
    int id;

    // value est renommé priority
    // pour clarifier son rôle dans le contexte
    // d'une file de priorité
    // (mais son rôle ne change pas)
    int priority;
} pqueue_entry;

// Crée une pqueue_entry
pqueue_entry* pqueue_entry_create(int id, int priority);

// Libère la mémoire associée à la pqueue_entry
void pqueue_entry_destroy(pqueue_entry *pe);

// En pratique, la file de priorité va simplement être un tas
// dont la taille est dynamique, pour simplifier l'utilisation
typedef struct PQueue {
    int size;
    int capacity; // 0 <= size <= capacity
    pqueue_entry **tab;
} pqueue;

// Crée une file de priorité vide
pqueue* pqueue_create(int capacity);

// Libère la mémoire associée à la file de priorité
void pqueue_destroy(pqueue *pq);

// Vérifie si la file de priorité est vide
bool pqueue_is_empty(pqueue *pq);

// Ajoute un élément à la file de priorité
void pqueue_push(pqueue *pq, int id, int priority);

// Supprime l'élément de priorité maximale de la file de priorité
// La mémoire associée à l'élément n'est alors plus gérée par la file de priorité
// (elle doit être libérée par l'appelant)
pqueue_entry* pqueue_pop(pqueue *pq);

#endif // PQUEUE_H_INCLUDED
type uf

(*
  * create n crée une structure union-find
  * avec n éléments, numérotés de 0 à n-1
  * initialement, chaque élément est dans une classe à part 
*)
val create: int -> uf

(*
  * find uf x renvoie le représentant de la classe de x
  * dans la structure union-find uf
*)
val find: uf -> int -> int

(*
  * union uf x y fusionne les classes de x et y
  * dans la structure union-find uf
*)
val union: uf -> int -> int -> unit

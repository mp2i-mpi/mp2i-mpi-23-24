type uf = int array

(*
  * Pour les questions b et c :
  * type uf = {
  *   parent: int array;
  *   rank: int array;
  * }
*)

let create (n: int) : uf = failwith "TODO"

let find (uf: uf) (x: int) : int = failwith "TODO"

let union (uf: uf) (x: int) (y: int) : unit = failwith "TODO"

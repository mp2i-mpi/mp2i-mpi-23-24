#ifndef HASHTBL_H_INCLUDED
#define HASHTBL_H_INCLUDED

#include <stdbool.h>

typedef struct Hashtbl hashtbl;

// crée une table de hachage vide
hashtbl *hashtbl_create(int size);

// libère la mémoire associée à la table de hachage
void hashtbl_destroy(hashtbl *ht);

// renvoie false si la clé existe déjà (et ne fait rien)
bool hashtbl_insert(hashtbl *ht, const char *key, int value);

// renvoie false si la clé n'existe pas (et ne fait rien)
bool hashtbl_remove(hashtbl *ht, const char *key);

// renvoie true si la clé existe
bool hashtbl_contains(hashtbl *ht, const char *key);

// renvoie la valeur associée à la clé, ou une erreur si la clé n'existe pas
int hashtbl_get(hashtbl *ht, const char *key);

#endif // HASHTBL_H_INCLUDED

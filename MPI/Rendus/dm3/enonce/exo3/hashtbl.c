#include "hashtbl.h"

#include "list.h"

#include <assert.h>

typedef struct Hashtbl {
    list* buckets;
} hashtbl;

int hash(const char *str) {
    int h = 5381;
    int c;

    while ((c = *str++)) {
        h = ((h << 5) + h) + c; /* hash * 33 + c */
    }

    return h;
}

const int HASHTBL_SIZE = 100;

// crée une table de hachage vide
hashtbl *hashtbl_create(int size) {
    assert (false);
}

// libère la mémoire associée à la table de hachage
void hashtbl_destroy(hashtbl *ht) {
    assert (false);
}

// renvoie false si la clé existe déjà (et ne fait rien)
bool hashtbl_insert(hashtbl *ht, const char *key, int value) {
    assert (false);
}

// renvoie false si la clé n'existe pas (et ne fait rien)
bool hashtbl_remove(hashtbl *ht, const char *key) {
    assert (false);
}

// renvoie true si la clé existe
bool hashtbl_contains(hashtbl *ht, const char *key) {
    assert (false);
}

// renvoie la valeur associée à la clé, ou une erreur si la clé n'existe pas
int hashtbl_get(hashtbl *ht, const char *key) {
    assert (false);
}

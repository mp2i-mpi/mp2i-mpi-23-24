(* Exercice 2 *)

let x = 10
let x = 
  let y = 10 + x in
  let y = let x = y * x in y + x in
  y + x
let z = x + 100

(* Exercice 6 *)

type t = B | N | R

let permute = List.map (
  function
    | B -> N
    | N -> R
    | R -> B
  )

let rec compte_B_aux res = function
  | [] -> res
  | B::q -> compte_B_aux (res + 1) q
  | _::q -> compte_B_aux res q
let rec compte_B = compte_B_aux 0

let rec plus_grande_sequence_aux m cur = function
  | [] -> max m cur
  | B::q -> plus_grande_sequence_aux m (cur+1) q
  | _::q -> plus_grande_sequence_aux (max m cur) 0 q

let plus_grande_sequence = plus_grande_sequence_aux 0 0

(* Exercice 15 *)

let rec gcd a b =
  if b = 0 then a else gcd b (a mod b)

let test_gcd () =
  assert (gcd 8 2 = 2);
  assert (gcd 7 1 = 7);
  assert (gcd 3 0 = 3)

type frac = { num: int; den: int }

let rec simp f =
  let a, b = if f.den > 0 then f.num, f.den else -f.num, -f.den in
  let g = gcd (abs a) b in
  { num = a / g; den = b / g }

let frac a b = simp { num = a; den = b }

let add_frac f1 f2 =
  frac (f1.num * f2.den + f2.num * f1.den) (f1.den * f2.den)
let neg_frac f = frac (-f.num) f.den
let sub_frac f1 f2 = add_frac f1 (neg_frac f2)
let mul_frac f1 f2 = frac (f1.num * f2.num) (f1.den * f2.den)
let inv_frac f = if f.num = 0 then failwith "div by zero" else frac f.den f.num

let float_of_frac f = (float_of_int f.num) /. (float_of_int f.den)
let string_of_frac f = Printf.sprintf "%d/%d" f.num f.den

(* Exercice 16 *)

type num =
  | Int of int
  | Float of float
  | Frac of frac

let frac_of_int i = frac i 1

let to_float = function
  | Float f -> f
  | Frac f -> float_of_frac f
  | Int i -> float_of_int i

let exec_op op_i op_fr op_fl n1 n2 = match n1, n2 with
  | Float a, Float b -> Float (op_fl a b)
  | Int a, Int b -> Int (op_i a b)
  | Frac a, Frac b -> Frac (op_fr a b)
  | Float fl, b -> Float (op_fl fl (to_float b))
  | a, Float fl -> Float (op_fl (to_float a) fl)
  | Frac fr, Int i -> Frac (op_fr fr (frac_of_int i))
  | Int i, Frac fr -> Frac (op_fr (frac_of_int i) fr)

let add_num = exec_op (+) add_frac (+.)
let sub_num = exec_op (-) sub_frac (-.)
let add_num = exec_op (+) add_frac (+.)
let add_num = exec_op (+) add_frac (+.)
let add_num = exec_op (+) add_frac (+.)
let add_num = exec_op (+) add_frac (+.)
let add_num = exec_op (+) add_frac (+.)
  
let neg_num = exec_op (-) sub_frac (-.) (Int 0)


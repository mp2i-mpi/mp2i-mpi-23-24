#include <stdio.h>
#include <stdlib.h>

void swap(int* x, int* y) {
    int tmp = *x;
    *x = *y;
    *y = tmp;
}

void swap_array(int a[], int i, int j) {
    swap(a+i, a+j);
}

void knuth_shuffle(int a[], int n) {
    for (int i = 0; i < n; ++i) {
        int j = rand() % (i+1);
        swap_array(a, i, j);
    }
}

void two_way_sort(int a[], int n) {
    int i = 0;
    int j = n-1;
    while (i < j) {
        if (a[i] == 0) {
            ++i;
        } else if (a[j] == 1) {
            --j;
        } else {
            swap_array(a, i, j);
            ++i;
            --j;
        }
    }
}

// entrée : un tableau a de taille n contenant uniquement 0, 1, et 2
// sortie : le tableau est trié en place dans l'ordre croissant
void dutch_flag(int a[], int n) {
    int i = 0;
    int j = 0;
    int k = n-1;
    while (j <= k) {
        if (a[j] == 0) {
            swap_array(a, i, j);
            ++i;
            ++j;
        } else if (a[j] == 1) {
            ++j;
        } else {
            swap_array(a, j, k);
            --k;
        }
    }
}

void print_array(int a[], int n) {
    printf("[");
    for (int i = 0; i < n; ++i) {
        printf("%d", a[i]);
        if (i < n-1) {
            printf(", ");
        }
    }
    printf("]\n");
}

void insertion_sort(int a[], int n) {
    for (int i = 1; i < n; ++i) {
        int v = a[i];
        int j = i;
        while (j > 0 && a[j-1] > v) {
            a[j] = a[j-1];
            --j;
        }
        a[j] = v;
    }
}

int binary_search(int v, int a[], int n) {
    int i = 0;
    int j = n-1;
    while (i <= j) {
        int m = (i+j)/2;
        if (a[m] == v) {
            return m;
        } else if (a[m] < v) {
            i = m+1;
        } else {
            j = m-1;
        }
    }
    return -1;
}

void quickrec(int a[], int l, int r) {
    if (l >= r-1) {
        return;
    }
    int p = a[l];
    int low = l;
    int high = r;
    for (int i = l+1; i < high;) {
    // int i = l+1;
    // while (i < high) {
        if (a[i] < p) {
            // swap_array(a, i++, low++);
            swap_array(a, i, low);
            ++i;
            ++low;
        } else if (a[i] == p) {
            ++i;
        } else {
            // swap_array(a, i, --high);
            --high;
            swap_array(a, i, high);
        }
    }
    quickrec(a, l, low);
    quickrec(a, high, r);
}

void quick_sort(int a[], int n) {
    knuth_shuffle(a, n);
    quickrec(a, 0, n);
}

int naive_maximum_subarray(int a[], int n) {
    int max = 0;
    for (int i = 0; i < n; ++i) {
        int sum = 0;
        for (int j = i; j < n; ++j) {
            sum += a[j];
            if (sum > max) {
                max = sum;
            }
        }
    }
    return max;
}

int maximum_subarray(int a[], int n) {
    int max = 0;
    int max_here = 0;
    for (int i = 0; i < n; ++i) {
        max_here += a[i];
        if (max_here < 0) {
            max_here = 0;
        }
        if (max_here > max) {
            max = max_here;
        }
    }
    return max;
}

const int diff_a_A = 'a' - 'A';

void swap_case(FILE* input) {
    int c;
    while ((c = getc(input)) != EOF) {
        if ('a' <= c && c <= 'z') {
            c -= diff_a_A;
        } else if ('A' <= c && c <= 'Z') {
            c += diff_a_A;
        }
        putchar(c);
    }
}

void draw(int n) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            if ((i & j) == 0) {
                printf("*");
            } else {
                printf(" ");
            }
        }
        printf("\n");
    }
}

// int main() {
//     int a[] = {1, 2, 3, 4, 5};
//     knuth_shuffle(a, 5);
//     print_array(a, 5);

//     int b[] = {0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1};
//     two_way_sort(b, 16);
//     print_array(b, 16);

//     int c[] = {0, 1, 2, 0, 1, 2, 0, 1, 2};
//     dutch_flag(c, 9);
//     print_array(c, 9);

//     int d[] = {5, 4, 3, 2, 1};
//     insertion_sort(d, 5);
//     print_array(d, 5);

//     int e[] = {1, 2, 3, 4, 5};
//     printf("%d\n", binary_search(3, e, 5));
//     printf("%d\n", binary_search(6, e, 5));

//     int f[] = {5, 4, 3, 2, 1};
//     quick_sort(f, 5);
//     print_array(f, 5);

//     int g[] = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
//     printf("%d\n", naive_maximum_subarray(g, 9));
//     printf("%d\n", maximum_subarray(g, 9));

//     draw(128);
    
//     return 0;
// }

int main(int argc, char** argv) {
    printf("%s", argv[0]);
    FILE* input;
    if (argc < 2) {
        input = stdin;
    } else {
        printf("%s", argv[1]);
        input = fopen(argv[1], "r");
        if (input == NULL) {
            fprintf(stderr, "Erreur d'ouverture du fichier %s\n", argv[1]);
            exit(1);
        }
    }
    swap_case(input);
    if (argc >= 2) {
        fclose(input);
    }
}

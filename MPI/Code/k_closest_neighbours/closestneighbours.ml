type data = float array
type label = int

(*
  dim: dimension de l'espace de données
  get: fonction qui renvoie un flottant
  Renvoie un point de données de dimension dim construit en appellant get dim fois
*)
let load dim get =
  Array.init dim (fun _ -> get())

(*
  dim: dimension de l'espace de données
  get: fonction qui renvoie un flottant
  Renvoie une paire (point, label) en appellant get dim fois, puis une fois pour le label
*)
let load_sample dim get =
  let data = load dim get in
  let label = int_of_float (get()) in
  data, label

(*
  data: point de données
  put: fonction qui prend un flottant en paramètre
  Stocke data en appelant put pour chaque coordonnée
*)
let store data put =
  Array.iter put data

(*
  sample: paire (point, label)
  put: fonction qui prend un flottant en paramètre
  Stocke sample en appelant store pour le point, puis put pour le label
*)
let store_sample sample put =
  let data, label = sample in
  store data put;
  put (float_of_int label)

module PointDataset = Dataset.Make(struct
  type t = data * label
  let load = load_sample 2
  let store = store_sample
end)

let training_set: (data*label) array = PointDataset.load "train.txt"
let test_set: (data*label) array = PointDataset.load "test.txt"

let print_data data =
  Array.iter (Printf.printf "%f ")

let print_sample sample =
  let data, label = sample in
  Printf.printf "%f %f -> %d\n" data.(0) data.(1) label

let eucl_dist x y =
  let coord_contrib ci =
    let xi, yi = ci in
    xi**2. +. yi**2.
  in
  sqrt (Array.fold_left (+.) 0. (Array.map coord_contrib (Array.combine x y)))

(*
  dist: une distance entre deux points
  k: le nombre de voisins à renvoyer
  dataset: le dataset (points+labels) dans lequel chercher les voisins
  point: le point dont on cherche les voisins
*)
let k_closest_neighbours (dist: data->data->float) (k: int) (dataset: data array) (point: data) =
  let indices = Array.init (Array.length dataset) (fun x -> x) in
  let with_distances = Array.combine indices (Array.map (dist point) dataset) in
  Array.sort (fun x y -> (compare (snd x) (snd y))) with_distances;
  let sorted_indices = Array.map fst with_distances in
  (Array.sub sorted_indices 0 k)


(* Test k_closes_neighbours *)

let real_point x = [| x |]

let test_k_closest_neighbours() =
  let dist x y = abs_float (y.(0) -. x.(0)) in
  let k = 2 in
  let dataset = Array.map real_point [| 1.; 2.; 3.; 4.; 5.; 6.; 8.; 9.; 10. |] in
  let test_points = Array.map real_point [| 0.5; 1.3; 9.3; 10.5 |] in
  let results = [|
    [| 0; 1 |];
    [| 0; 1 |];
    [| 7; 8 |];
    [| 8; 7 |]
  |] in
  let test_one_sample point expected =
    let result = k_closest_neighbours dist k dataset point in
    Array.iter (Printf.printf "%d ") result;
    print_endline "";
    assert(result = expected) in
  Array.iter (fun (x, res) -> test_one_sample x res) (Array.combine test_points results)

(*
  Construit un tableau d'occurrences des classes dans un tableau de labels
  Les labels sont des entiers de 0 inclus à num_classes exclus
  resultat.(i) contient le nombre d'occurrences de la classe i
  Par exemple, si num_classes = 3 et labels = [|0; 1; 1; 2; 0; 1|], le tableau renvoyé est [|2; 3; 1|] 
*)
let rec count_labels labels num_classes =
  let res = Array.make num_classes 0 in
  Array.iter (fun x -> res.(x) <- res.(x) + 1) labels;
  res

let rec argmax arr =
  assert (Array.length arr > 0);
  let rec argmax_aux arr i res =
    if i = Array.length arr then
      res
    else
      let new_res = if arr.(i) > arr.(res) then i else res in
      argmax_aux arr (i+1) new_res
  in
  argmax_aux arr 0 0

let test_argmax() =
  assert (argmax [|1; 2; 3; 4; 5|] = 4);
  assert (argmax [|5; 4; 3; 2; 1|] = 0);
  assert (argmax [|1; 2; 5; 4; 3|] = 2)
  
(*
  Hyperparamètres : dist, k, num_classes
  Données : dataset
  Point à classifier : point
*)
let k_closest_neighbours_classify dist k num_classes dataset point =
  let data = Array.map fst dataset in
  let neighbours = k_closest_neighbours dist k data point in
  print_endline "Neighbours :";
  Array.iter (Printf.printf "%d ") neighbours;
  print_endline "==============";
  let labels = Array.map (fun index -> snd dataset.(index)) neighbours in
  let counts = count_labels labels num_classes in
  argmax counts


(* Tous les tests *)
let check_test test =
  let test_name, test_func = test in
  test_func();
  Printf.printf "%s OK\n" test_name

let all_tests() =
  List.iter check_test [
    ("argmax", test_argmax);
    ("k_closest_neighbours", test_k_closest_neighbours)
  ]

(* On fait d'abord tous nos tests unitaires *)
let () =
  all_tests()

(* Puis on exécute le programme *)
let () =
  print_endline "======== TRAIN ========";
  Array.iter (print_sample) training_set;
  print_endline "======== TEST  ========";
  Array.iter (print_sample) test_set


let () =
  let dist = eucl_dist in
  let k = 3 in
  let num_classes = 4 in
  let dataset = training_set in
  let prediction = Array.map (fun sample -> k_closest_neighbours_classify dist k num_classes dataset sample) (Array.map fst test_set) in
  let labels = Array.map snd test_set in
  let confusion = Array.combine prediction labels in
  Array.iter (fun x -> Printf.printf "Prédiction : %d (label correct : %d)\n" (fst x) (snd x)) confusion
  

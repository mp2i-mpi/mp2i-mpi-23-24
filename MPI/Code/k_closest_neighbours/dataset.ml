module type S = sig
  type t
  val load: (unit -> float) -> t
  val store: t -> (float -> unit) -> unit
end

module Make (M : S) = struct
  
  type t = M.t array
  let load filename =
    let ic = open_in filename in
    let scanner = Scanf.Scanning.from_channel ic in
    let n = Scanf.bscanf scanner " %d" (fun x -> x) in
    let data = Array.init n (fun _ -> M.load (fun () -> Scanf.bscanf scanner " %f" (fun x -> x))) in
    close_in ic;
    data

  let store data filename =
    let ic = open_out filename in
    Printf.fprintf ic "%d\n" (Array.length data);
    Array.iter (fun x -> M.store x (fun x -> Printf.fprintf ic "%f " x)) data
end


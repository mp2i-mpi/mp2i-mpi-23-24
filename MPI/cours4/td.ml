let are_attacking positions x1 x2 =
  let y1 = positions.(x1) in
  let y2 = positions.(x2) in
  x1 = x2 || y1 = y2 || abs (x1 - x2) = abs (y1 - y2)

let is_attacked_by_prec positions i =
  let rec aux j =
    if j < 0
      then false
    else
      are_attacking positions i j or aux (j-1)
    in aux (i - 1)

let n_reines n =
  let positions = Array.make n 0 in
  let rec try_random_pass_aux i =
    if i >= n
      then ()
  else
    let j = Random.int n in
    positions.(i) <- j;

    if are_attacking positions i j
      then try_random_pass_aux i
  else
    try_random_pass_aux (i + 1)
  in positions

let print_array = Array.iter (Printf.printf "%d ")

let main () =
  let () = Random.self_init() in
  print_array (n_reines 8)

let () = main()
